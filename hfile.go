package main

import (
	"context"
	"errors"
	"flag"
	"fmt"
	"hfile/middleware"
	"hfile/ratelimiter"
	"hfile/tmpfs"
	"hfile/tool"
	"net/http"
	"os"
	"os/signal"
	"path/filepath"
	"syscall"
	"time"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
)

var ServerIp string
var ServerPort, ServerTimeout uint
var BehindReverseProxy bool = true

func main() {
	//load config
	var argvcfgpat string
	flag.StringVar(&argvcfgpat, "config", "", "Config file path")
	flag.Parse()
	if !LoadConfig(argvcfgpat) {
		tool.Error("LoadConfig Fail! Well save Default Config and exit.")
		ConfigAsDefault = true
		LoadConfig("")
		return
	}

	sstPath, _ := filepath.Abs("eric/")
	t, _ := tmpfs.StaticFolderConvertCheck(tmpfs.StaticFolder{AbsPath: sstPath})
	tmpfs.StaticFolderConvert.Put("AH2TR", t)

	HfileContext, HfileCancel := context.WithCancel(context.Background())

	tmpfs.InitTmpfs()

	ratelimiter.InitRatelimiter(HfileContext)

	middleware.InitRouter(HfileContext)

	r := mux.NewRouter()

	//middleware
	if BehindReverseProxy {
		r.Use(handlers.ProxyHeaders)
	}

	//asset
	r.Handle("/asset/", middleware.StaticFolder(middleware.AssetFolderPath, "/asset/")).Methods("GET")
	r.Handle("/assets/", middleware.StaticFolder(middleware.AssetFolderPath, "/assets/")).Methods("GET")

	//test url
	r.HandleFunc("/hi", middleware.Hi)
	r.HandleFunc("/header", middleware.Header)
	r.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		switch r.Method {
		case http.MethodGet:
			middleware.StaticFd("index.html")(w, r)
		case http.MethodPost, http.MethodPut:
			middleware.UploadTmp(w, r)
		default:
		}
	})

	r.HandleFunc("/{firstpt}", func(w http.ResponseWriter, r *http.Request) {
		var ok bool
		var firstpt string
		var staticonroot []string = []string{"index.html", "robots.txt", "favicon.ico"}
		firstpt, ok = mux.Vars(r)["firstpt"]
		if !ok {
			firstpt = ""
		}
		//asset
		for _, p := range staticonroot {
			if p == firstpt {
				middleware.StaticFd(firstpt)(w, r)
				return
			}
		}

		// index on Get and upload on Post,Put
		switch r.Method {
		case http.MethodGet:
			if len(firstpt) == 0 {
				middleware.StaticFd("index.html")(w, r)
			} else {
				middleware.DownloadTmp(w, r)
			}
		case http.MethodPost, http.MethodPut:
			middleware.UploadTmp(w, r)
		default:
			hfi, endfunc := middleware.HfileContextStart(w, r, 0)
			hfi.HttpErr = tool.NewHttpErr405(r.Method, []string{http.MethodGet, http.MethodPost, http.MethodPut})
			endfunc()
		}
		return
	})

	//upload
	r.HandleFunc("/put/{firstpt}", middleware.UploadTmp)

	//download
	r.HandleFunc("/get/{firstpt}", middleware.DownloadTmp).Methods("GET")
	r.HandleFunc("/get/{firstpt}.zip", middleware.DownloadTmp).Methods("GET")
	r.HandleFunc("/get/{firstpt}/{token}", middleware.DownloadTmp).Methods("GET")

	//delete
	r.HandleFunc("/del/{folder}/{token}", middleware.Deletetmp)
	//qrcode
	r.HandleFunc("/qrc/{folder}", middleware.QrCodeTmp)
	r.HandleFunc("/qrc/({folder}).png", middleware.QrCodeTmp)
	//auth
	r.HandleFunc("/aut/csv", middleware.AuthCsv)
	r.HandleFunc("/aut/crt/{role:[a-z0-9]+}/{group:[0-9]+}", middleware.AuthCreate)
	r.HandleFunc("/aut/login", middleware.Login)
	//rateLimit
	r.HandleFunc("/rat/stat", middleware.RatelimiterStatus)

	r.NotFoundHandler = http.Handler(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		hfi, endfunc := middleware.HfileContextStart(w, r, 0)
		hfi.HttpErr = tool.NewHttpError(errors.New("Path not found"), 404, tool.DEBUG)
		endfunc()
	}))

	r.Walk(func(route *mux.Route, router *mux.Router, ancestors []*mux.Route) error {
		t, err := route.GetPathTemplate()
		if err != nil {
			fmt.Println(err)
			return err
		}

		tool.Info(t)
		return nil
	})

	/*
		//directUrl
		r.HandleFunc("/url/{folder:[0-9a-zA-Z]+}", downloadSst).Methods("GET")

		//staticfolder
		r.HandleFunc("/{folder:[0-9a-zA-Z]+}/{fd}", uploadStt).Methods("POST")
		r.HandleFunc("/{folder:[0-9a-zA-Z]+}/{fd}", downloadSst).Methods("GET")
	*/
	saddr := fmt.Sprintf("%s:%d", ServerIp, ServerPort)
	srv := &http.Server{
		Handler:      r,
		Addr:         saddr,
		WriteTimeout: 120 * time.Second,
		ReadTimeout:  120 * time.Second,
		IdleTimeout:  120 * time.Second,
	}

	chsignal := make(chan os.Signal, 1)
	shutctx, shutcancel := context.WithTimeout(HfileContext, 12*time.Second)
	defer shutcancel()
	signal.Notify(chsignal, os.Interrupt)
	signal.Notify(chsignal, syscall.SIGHUP)
	signal.Notify(chsignal, syscall.SIGTERM)

	go func() {
		tool.Info("server linsen on %s", saddr)
		if err := srv.ListenAndServe(); err != nil {
			tool.Error("Server listen fail %+v", err)
		}
	}()

	<-chsignal
	srv.Shutdown(shutctx)
	HfileCancel()
	time.Sleep(1 * time.Second)
	select {
	case <-HfileContext.Done():
	}
	tool.Info("server Shutdown")
}
