#!/usr/bin/env bash

declare hfilebindir="/usr/share/hfile"
declare hfilebuilddir="/home/hfile"
declare hfileconfigdir="/etc/hfile"

cd "${hfilebuilddir}"
if [[ -d "${hfilebuilddir}/hfile/" ]]; then
	rm -rf "${hfilebuilddir}/hfile/"
fi

git clone --depth 3 "https://gitlab.com/joe1231231218/hfile.git"
cd hfile
cp -f default_auth.csv "${hfileconfigdir}/deault_auth.csv"
cp -f DefaultConfig.ini "${hfileconfigdir}/DefaultConfig.ini"

if [[ ! -f "${hfileconfigdir}/auth.csv" ]]; then
	cp default_auth.csv "${hfileconfigdir}/auth.csv"
fi
if [[ ! -f "${hfilebindir}/auth.csv" ]]; then
	cp default_auth.csv "${hfilebindir}/auth.csv"
fi

# build hfile server
go clean -modcache
go mod download
go build

cd eric/test_assets
go mod download
go build
cd ../
cp -R -f test_assets "${hfilebindir}"
cd ../
cp -f hfile "${hfilebindir}"
if [ ! -f "${hfilebindir}/test_assets/_code" ]; then
	touch _code
	chmod 664 _code
fi

cd "${hfilebindir}"
chmod 775 "${hfilebindir}/hfile"
chmod 775 "${hfilebindir}/test_assets/hfile_client"
chmod 775 "${hfilebindir}/test_assets/hfile_test.bash"
./hfile > hfile.log &
sleep 3
cd test_assets
./hfile_test.bash 

exit

