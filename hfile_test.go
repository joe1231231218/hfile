package main

import (
	"testing"
)

func CheckPanic(t *testing.T) {
	if r := recover(); r == nil {
		t.Logf("Expect panic but not panic.\n")
	} else {
		t.Logf("Recover go next.\n")
	}
}

func Test_ConfigDefault(t *testing.T) {
	var r bool
	r = LoadConfig("")
	t.Log(r == false)
	r = LoadConfig(ConfigDefaultFilePath)
	t.Log(r)
}

func Test_ConfigLossKey(t *testing.T) {
	LoadConfig("test_assets/ConfigLossKey.ini")
	defer CheckPanic(t)
}

func Test_ConfigTypeErr(t *testing.T) {
	LoadConfig("test_assets/ConfigTypeErr.ini")
	defer CheckPanic(t)
}
