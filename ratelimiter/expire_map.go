package ratelimiter

import (
	"hfile/tool"
	"math"
	"sync"
	"time"

	singlylinkedlist "github.com/emirpasic/gods/lists/singlylinkedlist"
	"github.com/emirpasic/gods/maps/hashmap"
	redblacktree "github.com/emirpasic/gods/trees/redblacktree"
	godsutils "github.com/emirpasic/gods/utils"
)

type TimeSequence struct {
	Lock        sync.RWMutex
	LevelUnit   [3]int64
	_Log2Step   int32
	StepLength  int32
	TickCounter int32
}

type ExpireMap struct {
	Lock   sync.Mutex
	Limit  *hashmap.Map
	Expire *redblacktree.Tree
}

// config
var LConfExpireNodePre int = 32

func NewTimeSequence(steplength int) *TimeSequence {
	unixTs := time.Now().Unix()
	tsn := &TimeSequence{
		_Log2Step:   int32(math.Log2(float64(steplength + 1))),
		StepLength:  int32(steplength),
		TickCounter: 0,
	}
	tsn.LevelUnit[0] = unixTs
	tsn.LevelUnit[1] = unixTs
	tsn.LevelUnit[2] = unixTs
	return tsn
}

func (tsn *TimeSequence) CurrentLevel(level int) int64 {
	if level > len(tsn.LevelUnit) {
		tool.Error("CurrentLevel level too large.")
	}
	var ts int64
	tsn.Lock.RLock()
	ts = tsn.LevelUnit[level]
	tsn.Lock.RUnlock()
	return ts
}

func (tsn *TimeSequence) SynchronizeWatche() bool {
	var diff int64
	var tmSync bool = false
	unixTs := time.Now().Unix()
	tsn.Lock.Lock()
	diff = unixTs - tsn.LevelUnit[0]
	if diff > int64(tsn.StepLength) {
		tmSync = true
		tsn.LevelUnit[0] = unixTs
		if diff > int64(tsn.StepLength)*int64(tsn._Log2Step) {
			tsn.LevelUnit[1] = unixTs
			if diff > int64(tsn.StepLength)*int64(tsn.StepLength) {
				tsn.LevelUnit[2] = unixTs
			}
		}
	}
	tsn.Lock.Unlock()
	return tmSync
}

func NewExpireMap() *ExpireMap {
	return &ExpireMap{
		Limit:  hashmap.New(),
		Expire: redblacktree.NewWith(godsutils.Int64Comparator),
	}
}

func (emap *ExpireMap) DoExpire(tick int64) int64 {
	var cn int64 = 0
	var et int64

	emap.Lock.Lock()
	for int(cn) < LConfExpireNodePre {
		if emap.Expire.Size() > 0 {
			et = emap.Expire.Left().Key.(int64)
		} else {
			break
		}
		if et > tick {
			break
		}
		eval := emap.Expire.Left().Value.(*singlylinkedlist.List)
		iter := eval.Iterator()
		for iter.Next() {
			k := iter.Value()
			emap.Limit.Remove(k)
			cn += 1
		}
		emap.Expire.Remove(et)
	}
	emap.Lock.Unlock()
	return cn
}

func (emap *ExpireMap) Size() int {
	emap.Lock.Lock()
	size := emap.Limit.Size()
	emap.Lock.Unlock()
	return size
}

func (emap *ExpireMap) Get(key string) (interface{}, bool) {
	var found bool
	var itf interface{}
	emap.Lock.Lock()
	itf, found = emap.Limit.Get(key)
	emap.Lock.Unlock()
	return itf, found
}

func (emap *ExpireMap) PutCore(key string, content interface{}, expire int64, newOnly bool) bool {
	var sll *singlylinkedlist.List

	emap.Lock.Lock()
	_, ok := emap.Limit.Get(key)
	if !ok {
		isll, have := emap.Expire.Get(expire)
		if !have {
			sll = singlylinkedlist.New()
			emap.Expire.Put(expire, sll)
		} else {
			sll = isll.(*singlylinkedlist.List)
		}
		sll.Add(key)
	}
	if !(newOnly && ok) {
		emap.Limit.Put(key, content)
	}
	emap.Lock.Unlock()

	return !(newOnly && ok)
}

func (emap *ExpireMap) Put(key string, content interface{}, expire int64) bool {
	return emap.PutCore(key, content, expire, false)
}

func (emap *ExpireMap) New(key string, content interface{}, expire int64) bool {
	return emap.PutCore(key, content, expire, true)
}
