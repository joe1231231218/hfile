package ratelimiter

import (
	"context"
	"fmt"
	"hfile/tool"
	"strconv"
	"sync"
	"testing"
	"time"

	"github.com/redis/go-redis/v9"
)

/*


 */

/*
// if you want to do test, please open this block and full config value.

func LoadTestConfig() {
	Rlty = RLTY_LOCALDB
	DefaultBlockExpire = SECOND_PER_MINTUE * 5
	DefaultSlideWindowSize = 4
	DefaultLeakyBucketExpire = SECOND_PER_HOUR * 5
	RateLimiterRedisDb = 0
	RateLimiterRedisPoolSize = 32
	RateLimiterRedisAddrPort = ""
	RateLimiterRedisPassword = ""
}

*/

type ExpireMapNodeStruct struct {
	Goroutine int
	Sequence  int
	Method    string
}

func Test_ExpireMap(t *testing.T) {
	var exp *ExpireMap
	var expnew string = "ExpNew"
	var expput string = "ExpPut"
	exp = NewExpireMap()

	exp.New(expnew, expnew, 1)
	itf, ok := exp.Get(expnew)
	if !ok {
		t.Errorf("Expect ExpireMap::Get sus.")
	}
	s := itf.(string)
	if s != expnew {
		t.Errorf("Expect ExpireMap::Get and ExpireMap::New as same pointer.")
	}
	exp.Put(expnew, expput, time.Now().Unix()+1)
	itf, ok = exp.Get(expnew)
	if !ok {
		t.Errorf("Expect ExpireMap::Get sus.")
	}
	s = itf.(string)
	if s != expput {
		t.Errorf("Expect ExpireMap::Get and ExpireMap::Put(%s) as same pointer.", s)
	}
	if exp.Size() != 1 {
		t.Errorf("Expect Size as 1.")
	}
	time.Sleep(1 * time.Second)
	exp.DoExpire(time.Now().Unix())
	if exp.Size() != 0 {
		t.Errorf("Expect Expired.")
	}

	var wg sync.WaitGroup

	var parallelfunc = func(goroutine int) {
		defer wg.Done()
		var emns *ExpireMapNodeStruct

		for i := 0; i < 100; i += 1 {
			emns = &ExpireMapNodeStruct{
				Goroutine: goroutine,
				Sequence:  i,
				Method:    "New",
			}
			s := fmt.Sprintf("%d::%d", goroutine, i)
			if !exp.New(s, emns, time.Now().Unix()+int64(i)) {
				t.Errorf("Expect key not exist.")
			}
		}
		exp.DoExpire(time.Now().Unix())
		for i := 0; i < 100; i += 1 {
			s := fmt.Sprintf("%d::%d", goroutine, i)
			itf, ok := exp.Get(s)
			if ok {
				emns = itf.(*ExpireMapNodeStruct)
				if emns.Goroutine != goroutine || emns.Sequence != i || emns.Method != "New" {
					t.Errorf("UnExpect Value in %+v goroutine %d Sequence %d Method %s.", emns, goroutine, i, "New")
				}
			}
		}
		for i := 50; i < 60; i += 1 {
			emns = &ExpireMapNodeStruct{
				Goroutine: goroutine,
				Sequence:  1,
				Method:    "Put",
			}
			s := fmt.Sprintf("%d::%d", goroutine, i)
			if !exp.Put(s, emns, time.Now().Unix()+int64(i)) {
				t.Errorf("Expect Put sus.")
			}
		}
	}

	for i := 0; i < 6; i += 1 {
		wg.Add(1)
		go parallelfunc(i)
	}
	wg.Wait()

	var ts *TimeSequence = NewTimeSequence(2)
	ts.SynchronizeWatche()
	t.Logf("TimeSequence Lv0 %d Lv1 %d Lv2 %d.", ts.CurrentLevel(0), ts.CurrentLevel(1), ts.CurrentLevel(2))
	time.Sleep(5 * time.Second)
	ts.SynchronizeWatche()
	t.Logf("TimeSequence Lv0 %d Lv1 %d Lv2 %d.", ts.CurrentLevel(0), ts.CurrentLevel(1), ts.CurrentLevel(2))
}

func Test_Localdb(t *testing.T) {
	var wg sync.WaitGroup
	var ts *TimeSequence = NewTimeSequence(1)
	var blt *BlockLimiter = NewBlockLimiter(ts)
	var lbl *LeakyBucketLimiter = NewLeakyBucketLimiter(ts)
	var swl *SlideWindowLimiter = NewSlideWindowLimiter(ts, 1)
	var dostress bool = true
	var blockstr string = "Idt"

	prd, ok := blt.IsBlock(blockstr)
	if ok {
		t.Errorf("Expect BlockLimiter::IsBlock(%s) UnBlock.", blockstr)
	}
	t.Log(ok, prd)

	blt.Block(blockstr)
	prd, ok = blt.IsBlock(blockstr)
	blt.DoExpire()
	if !ok {
		t.Errorf("Expect BlockLimiter::IsBlock(%s) Blocked.", blockstr)
	}
	t.Log(ok, prd)

	for i := 0; i < 25; i += 1 {
		blt.Block(blockstr)
	}
	blt.DoExpire()
	prd, ok = blt.IsBlock(blockstr)
	t.Log(ok, prd)

	if dostress {
		DefaultBlockExpire = 1
		for i := 0; i < 100; i += 1 {
			go func(goroutineindx int) {
				wg.Add(1)
				defer wg.Done()
				var iok bool
				var iprd int64
				for i := 0; i < 10000; i += 1 {
					iprd, iok = blt.IsBlock(strconv.Itoa(i))
					if (goroutineindx*i)%3 == 0 {
						if blt.Block(strconv.Itoa(i)) < iprd {
							t.Errorf("Expect Block extend Expire i %d iprd %d isblock %v.", i, iprd, iok)
						}
						iprd, iok = blt.IsBlock(strconv.Itoa(i))
						if !iok {
							t.Error("Expect Block let IsBlock return ok.")
						}
					}
				}
			}(i)
		}
		wg.Wait()
		DefaultBlockExpire = SECOND_PER_MINTUE * 5
	}

	var water int64
	var allow bool
	if water = lbl.Risk(blockstr, 1); water != 0 {
		t.Errorf("Expect water(%d) shell be 0.", water)
	}
	if water, allow = lbl.Allow(blockstr, 2, 1, 100); !allow {
		t.Errorf("Expect allow.")
	}
	if water = lbl.Risk(blockstr, 1); water != 2 {
		t.Errorf("Expect water(%d) shell be 2.", water)
	}
	time.Sleep(2 * time.Second)
	ts.SynchronizeWatche()
	lbl.Risk(blockstr, 1)
	if water, allow = lbl.Allow(blockstr, 1000, 1, 100); allow {
		t.Errorf("Expect not allow.")
	}

	var total int64
	if swl.Risk(blockstr) != 0 {
		t.Errorf("Expect Total shell be 0.")
	}
	if total, allow = swl.Allow(blockstr, 2, 100); !allow {
		t.Errorf("Expect allow.")
	}
	time.Sleep(5 * time.Second)
	ts.SynchronizeWatche()
	if swl.Risk(blockstr) != 0 {
		t.Errorf("Expect total slide to 0.")
	}
	if total, allow = swl.Allow(blockstr, 1000, 100); allow {
		t.Errorf("Expect not allow.")
	}
	_ = total
}

func Test_Redisdb(t *testing.T) {
	var ok bool
	var wg sync.WaitGroup
	var err error
	var blt *RBlockLimiter
	var lbl *RLeakyBucketLimiter
	var swl *RSlideWindowLimiter
	var rpool *redis.Client
	var dostress bool = true
	var blockstr string = "Idt"
	var leakystr string = "Lea"
	var slidestr string = "Sli"
	LoadTestConfig()
	rpool = redis.NewClient(&redis.Options{
		DB:          RateLimiterRedisDb,
		Addr:        RateLimiterRedisAddrPort,
		PoolSize:    RateLimiterRedisPoolSize,
		Password:    RateLimiterRedisPassword,
		ReadTimeout: 5 * time.Second,
	})
	rpool.FlushDBAsync(context.Background())

	blt = NewRBlockLimiter(rpool)
	ctx, canael := context.WithTimeout(context.Background(), time.Second*10)
	prd, ok, _ := blt.IsBlock(ctx, blockstr)
	if ok {
		t.Errorf("Expect BlockLimiter::IsBlock(%s) UnBlock.", blockstr)
	}
	t.Log(ok, prd)

	blt.Block(ctx, blockstr)
	prd, ok, _ = blt.IsBlock(ctx, blockstr)
	if !ok {
		t.Errorf("Expect BlockLimiter::IsBlock(%s) Blocked.", blockstr)
	}
	t.Log(ok, prd)

	for i := 0; i < 25; i += 1 {
		blt.Block(ctx, blockstr)
	}
	prd, ok, _ = blt.IsBlock(ctx, blockstr)
	t.Log(ok, prd)
	canael()
	if dostress {
		DefaultBlockExpire = 8
		for i := 0; i < 10; i += 1 {
			go func(goroutineindx int) {
				wg.Add(1)
				defer wg.Done()
				var iok bool
				var iprd, jprd int64
				for i := 0; i < 100; i += 1 {
					ctx, canael := context.WithTimeout(context.Background(), time.Second*3)
					idt := fmt.Sprintf("192.168.%d.%d", i/256, i%256)
					iprd, iok, _ = blt.IsBlock(ctx, idt)
					if (goroutineindx*i)%3 == 0 {
						jprd, _ = blt.Block(ctx, idt)
						if jprd < iprd {
							t.Error("Expect Block extend Expire.")
						}
						iprd, iok, _ = blt.IsBlock(ctx, idt)
						if !iok {
							t.Error("Expect Block let IsBlock return ok.")
						}
					}
					canael()
				}

			}(i)
		}
		wg.Wait()
		DefaultBlockExpire = SECOND_PER_MINTUE * 5
	}
	var water int64
	ctx, canael = context.WithTimeout(context.Background(), time.Second*10)
	lbl = NewRLeakyBucketLimiter(rpool)
	water, err = lbl.Risk(ctx, leakystr, 0)
	if err != nil || water != 0 {
		t.Error(err, water)
	}
	water, ok, err = lbl.Allow(ctx, leakystr, 24, 0, 500)
	if err != nil {
		t.Error(err)
	}
	if !ok {
		t.Errorf("Expect allow.")
	}
	if water != 24 {
		t.Errorf("Expect water 24, but water is %d.", water)
	}
	time.Sleep(1 * time.Second)
	water, err = lbl.Risk(ctx, leakystr, 1)
	if water >= 24 {
		t.Errorf("Expect water(%d) Leak.", water)
	}
	t.Log("Water", water)
	canael()
	if dostress {
		for i := 0; i < 10; i += 1 {
			go func(goroutineindx int) {
				wg.Add(1)
				defer wg.Done()
				var ok bool
				var idt string
				var iwaterf, iwaters int64
				for i := 0; i < 1000; i += 1 {
					ictx, icanel := context.WithTimeout(context.Background(), time.Second*3)
					idt = fmt.Sprintf("192.168.%d.%d", i/256, i%256)

					iwaterf, _ = lbl.Risk(ictx, idt, 1)
					iwaters, ok, _ = lbl.Allow(ictx, idt, 20, 1, 200)
					if ok {
						if iwaterf > iwaters {
							t.Errorf("Expect water inc %d %d", iwaterf, iwaters)
						}
					}
					if (i*goroutineindx)%10 == 0 {
						iwaterf, ok, _ = lbl.Allow(ictx, idt, 250, 1, 200)
						if iwaterf != 200 {
							t.Errorf("Expect water(%d) set to burst", iwaterf)
						}
					}
					icanel()
				}
			}(i)
		}
		wg.Wait()
	}
	var total int64
	swl = NewRSlideWindowLimiter(rpool, 10)
	ctx, canael = context.WithTimeout(context.Background(), time.Second*10)
	total, _ = swl.Risk(ctx, slidestr)
	if total != 0 {
		t.Errorf("Expect total(%d) set to 0.", total)
	}
	total, ok, _ = swl.Allow(ctx, slidestr, 12, 50)
	if total != 12 {
		t.Errorf("Expect total(%d) set to 12.", total)
	}
	total, _ = swl.Risk(ctx, slidestr)
	if total != 12 {
		t.Errorf("Expect total(%d) keep 12.", total)
	}
	canael()
	slidestr += "o"
	swl = NewRSlideWindowLimiter(rpool, 1)
	ctx, canael = context.WithTimeout(context.Background(), time.Second*12)
	total, ok, _ = swl.Allow(ctx, slidestr, 12, 50)
	time.Sleep(1 * time.Second)
	if total != 12 {
		t.Errorf("Expect total(%d) set to 12.", total)
	}
	total, ok, _ = swl.Allow(ctx, slidestr, 12, 50)
	if total != 24 {
		t.Errorf("Expect total(%d) set to 24.", total)
	}
	time.Sleep(2 * time.Second)
	swl.Allow(ctx, slidestr, 0, 50)
	time.Sleep(3 * time.Second)
	total, _ = swl.Risk(ctx, slidestr)
	if total != 0 {
		t.Errorf("Expect total(%d) set to 0.", total)
	}
	canael()

	if dostress {
		for i := 0; i < 12; i += 1 {
			go func(goroutineindx int) {
				wg.Add(1)
				defer wg.Done()
				var idt string
				for j := 0; j < 12; j += 1 {
					ictx, icanel := context.WithTimeout(context.Background(), time.Second*3)
					idt = fmt.Sprintf("Slide%d", goroutineindx)
					swl.Allow(ictx, idt, j, 300)
					time.Sleep(1 * time.Second)
					icanel()
				}
			}(i)
		}
		time.Sleep(1 * time.Second)
		wg.Wait()
	}
	fmt.Print("endff")
	rpool.Close()
}

func Test_RateLimiter(t *testing.T) {
	var ok bool
	var err error
	var rlany, rluser *tool.UserInfo
	var period int64
	var rllst []*tool.UserInfo

	ctx, cancel := context.WithCancel(context.TODO())

	Rlty = RLTY_LOCALDB
	InitRatelimiter(ctx)

	rlany = tool.NewUserInfoBase("any")

	if period, ok, err = IsBlock(ctx, rlany.Role); ok {
		t.Log("Shell not IsBlock.")
	}
	if period, ok, err = Allow(ctx, rlany.Role, 5, rlany); !ok {
		t.Log("Shell Allow.")
	}
	if period, ok, err = Allow(ctx, rlany.Role, 1500, rlany); ok {
		t.Log("Shell not Allow.")
	}
	if period, ok, err = IsBlock(ctx, rlany.Role); !ok {
		t.Log("Shell Block.")
	}
	t.Logf("Block %s period %d.", rlany.Role, period)

	AllowTen := func(i int) string {
		testIp := fmt.Sprintf("1.1.2.%d", i)
		if period, ok, err = Allow(ctx, testIp, 5, rlany); !ok {
			t.Log("Shell Allow.")
		}
		return testIp
	}

	for i := 0; i < 256; i++ {
		AllowTen(i)
		AllowTen(i)
	}
	rllst = make([]*tool.UserInfo, 0)
	for i := 0; i < 100; i++ {
		rluser = tool.NewUserInfoBase(fmt.Sprintf("1.1.3.%d", i))
		rllst = append(rllst, rluser)
		if period, ok, err = IsBlock(ctx, rluser.Role); ok {
			t.Log("Shell not IsBlock.")
		}
		if period, ok, err = Allow(ctx, rluser.Role, 5, rluser); !ok {
			t.Log("Shell Allow.")
		}
		if period, ok, err = Allow(ctx, rluser.Role, 1500, rluser); ok {
			t.Log("Shell not Allow.")
		}
		if period, ok, err = IsBlock(ctx, rluser.Role); !ok {
			t.Log("Shell Block.")
		}
	}

	for i := 0; i < 255; i++ {
		if i < 100 {
			rluser = rllst[i]
		} else {
			rluser = tool.NewUserInfoBase(fmt.Sprintf("1.1.4.%d", i))
		}
		period, ok, err = IsBlock(ctx, rluser.Role)
		if !ok {
			if i < 100 {
				t.Log("Shell Block.")
			}
		} else if 100 < i {
			t.Log("Shell Not Block.")
		}
	}
	_ = err
	cancel()
}
