package ratelimiter

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"hfile/tool"
	"time"

	"github.com/redis/go-redis/v9"
)

const (
	RbkeyBlock       string = "B"
	RbkeyLeakyBucket string = "L"
	RbkeySlideWindow string = "S"
)

type RBlockLimiter struct {
	Pool          *redis.Client
	LimiterName   string
	DefaultExpire int64
}

func NewRBlockLimiter(pool *redis.Client) *RBlockLimiter {
	return &RBlockLimiter{
		Pool:          pool,
		LimiterName:   "",
		DefaultExpire: int64(DefaultBlockExpire),
	}
}

type RLeakyBucketLimiter struct {
	Pool          *redis.Client
	LimiterName   string
	DefaultExpire int64
}

func NewRLeakyBucketLimiter(pool *redis.Client) *RLeakyBucketLimiter {
	return &RLeakyBucketLimiter{
		Pool:          pool,
		LimiterName:   "",
		DefaultExpire: int64(DefaultLeakyBucketExpire),
	}
}

type RSlideWindowLimiter struct {
	Pool          *redis.Client
	BeforeSize    int
	SlidePeriod   int64
	LimiterName   string
	DefaultExpire int64
}

func NewRSlideWindowLimiter(pool *redis.Client, slideperiod int64) *RSlideWindowLimiter {
	var beforesize int = DefaultSlideWindowSize
	return &RSlideWindowLimiter{
		Pool:          pool,
		BeforeSize:    beforesize,
		SlidePeriod:   slideperiod,
		LimiterName:   "",
		DefaultExpire: slideperiod*(int64(beforesize)+1) + 100,
	}

}

func RbKeyGenerate(limiter string, limiteridt string, idt string) string {
	var rbkey string = ""
	if len(limiteridt) != 0 {
		rbkey = fmt.Sprintf("%s:", limiteridt)
	}
	rbkey = fmt.Sprintf("%s:%s%s", limiter, rbkey, idt)
	return rbkey
}

func RedisGet(ctx context.Context, pool *redis.Client, rbkey string, strtype string, jsonUnmarshal func([]byte) error) (bool, error) {
	var ok bool
	var err error
	var jsonstr string

	ok = true
	jsonstr, err = pool.Get(ctx, rbkey).Result()
	//fmt.Printf("RedisGet(%s) %s\n", rbkey, jsonstr)
	if err != nil {
		if errors.Is(err, redis.Nil) {
			ok = false
		} else {
			tool.Error("RBlockLimiter::_Get Redis Get %s %s", rbkey, err.Error())
			return false, err
		}
	}
	if ok {
		err = jsonUnmarshal([]byte(jsonstr))
		if err != nil {
			ok = false
			tool.Warn("RedisGet(rbkey %s) Json decode to %s %s", rbkey, strtype, err.Error())
		}
	}

	return ok, nil
}

func RedisSet(ctx context.Context, pool *redis.Client, rbkey string, ltt int64, strtype string, jsonMarshal func() ([]byte, error)) error {
	var err error
	var buff []byte

	buff, err = jsonMarshal()
	if err != nil {
		err = errors.New(fmt.Sprintf("RedisSet Json encode to %s %s", strtype, err.Error()))
		tool.Error(err.Error())
		return err
	}
	//fmt.Printf("RedisSet(%s) %s\n", rbkey, string(buff))
	err = pool.Set(ctx, rbkey, string(buff), time.Duration(ltt)*time.Second).Err()
	if err != nil {
		err = errors.New(fmt.Sprintf("RedisSet Redis Set %s %s", rbkey, err.Error()))
		tool.Error(err.Error())
		return err
	}
	return nil
}

func (blt *RBlockLimiter) _Get(ctx context.Context, rbkey string) (*ContentBlock, bool, error) {
	var ok bool
	var err error
	var node *ContentBlock
	ok, err = RedisGet(ctx, blt.Pool, rbkey, "ContentBlock", func(buff []byte) error {
		return json.Unmarshal(buff, &node)
	})

	return node, ok, err
}

func (blt *RBlockLimiter) Block(ctx context.Context, idt string) (int64, error) {
	var ok bool
	var ts int64
	var err error
	var node *ContentBlock
	var rbkey string

	ts = time.Now().Unix()
	rbkey = RbKeyGenerate(RbkeyBlock, blt.LimiterName, idt)
	node, ok, err = blt._Get(ctx, rbkey)
	if err != nil {
		return 0, err
	}
	if !ok {
		fmt.Printf("RBlockLimiter::Block(%s) not ok.\n", idt)
		node = NewContentBlock(idt, ts, blt.DefaultExpire)
	} else {
		node.Block(ts, blt.DefaultExpire)
	}
	err = RedisSet(ctx, blt.Pool, rbkey, tool.MoreThen0Int64(node.Expire-ts)+2, "ContentBlock", func() ([]byte, error) {
		return json.Marshal(&node)
	})
	if err != nil {
		return 0, err
	}
	return node.Expire, nil
}

func (blt *RBlockLimiter) IsBlock(ctx context.Context, idt string) (int64, bool, error) {
	var ok bool
	var err error
	var node *ContentBlock
	node, ok, err = blt._Get(ctx, RbKeyGenerate(RbkeyBlock, blt.LimiterName, idt))
	if err != nil {
		return 0, false, err
	}
	if !ok {
		return 0, false, nil
	}
	if node.Expire <= time.Now().Unix() {
		return 0, false, nil
	}
	return node.Expire, true, nil
}

func (lbl *RLeakyBucketLimiter) Allow(ctx context.Context, idt string, risk int, leakrate int, burst int) (int64, bool, error) {
	var ok, allow bool
	var ts, currentwater int64
	var err error
	var node *ContentLeakyBucket
	var rbkey string

	rbkey = RbKeyGenerate(RbkeyLeakyBucket, lbl.LimiterName, idt)
	ok, err = RedisGet(ctx, lbl.Pool, rbkey, "ContentLeakyBucket", func(buff []byte) error {
		return json.Unmarshal(buff, &node)
	})
	if err != nil {
		return 0, false, err
	}
	ts = time.Now().Unix()
	if !ok {
		node = NewContentLeakyBucket(ts)
	}
	currentwater, allow = node.LeakyWater(ts, risk, leakrate, burst)
	err = RedisSet(ctx, lbl.Pool, rbkey, lbl.DefaultExpire, "ContentLeakyBucket", func() ([]byte, error) {
		return json.Marshal(&node)
	})
	if err != nil {
		return 0, false, err
	}
	return currentwater, allow, nil
}

func (lbl *RLeakyBucketLimiter) Risk(ctx context.Context, idt string, leakrate int) (int64, error) {
	var ok bool
	var err error
	var node *ContentLeakyBucket
	ok, err = RedisGet(ctx, lbl.Pool, RbKeyGenerate(RbkeyLeakyBucket, lbl.LimiterName, idt), "ContentLeakyBucket", func(buff []byte) error {
		return json.Unmarshal(buff, &node)
	})
	if err != nil {
		return 0, err
	}
	if !ok {
		return 0, nil
	}
	return node.CurrentWater(time.Now().Unix(), 0, leakrate), nil
}

func (swl *RSlideWindowLimiter) Allow(ctx context.Context, idt string, risk int, full int64) (int64, bool, error) {
	var ok, allow bool
	var ts, total int64
	var err error
	var node *ContentSlideWindow
	var rbkey string

	ts = time.Now().Unix()
	rbkey = RbKeyGenerate(RbkeySlideWindow, swl.LimiterName, idt)
	ok, err = RedisGet(ctx, swl.Pool, rbkey, "ContentSlideWindow", func(buff []byte) error {
		return json.Unmarshal(buff, &node)
	})
	if err != nil {
		return 0, false, err
	}
	if !ok {
		node = NewContentSlideWindow(ts, swl.BeforeSize)
	}
	total = node.SlideTotal(ts, swl.SlidePeriod, risk)
	if total < full {
		allow = true
	} else {
		allow = false
	}
	err = RedisSet(ctx, swl.Pool, rbkey, swl.DefaultExpire, "ContentSlideWindow", func() ([]byte, error) {
		return json.Marshal(&node)
	})
	if err != nil {
		return 0, false, err
	}
	return total, allow, nil
}

func (swl *RSlideWindowLimiter) Risk(ctx context.Context, idt string) (int64, error) {
	var ok bool
	var err error
	var node *ContentSlideWindow
	ok, err = RedisGet(ctx, swl.Pool, RbKeyGenerate(RbkeySlideWindow, swl.LimiterName, idt), "ContentSlideWindow", func(buff []byte) error {
		return json.Unmarshal(buff, &node)
	})
	if err != nil {
		return 0, err
	}
	if !ok {
		return 0, nil
	}
	return node.SlideTotal(time.Now().Unix(), swl.SlidePeriod, 0), nil
}
