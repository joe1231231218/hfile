package ratelimiter

import (
	"context"
	"hfile/tool"
	"time"

	"github.com/redis/go-redis/v9"
)

const (
	RLTY_LOCALDB = 0
	RLTY_REDIS   = 1
)

const (
	SECOND_PER_MINTUE = 60
	SECOND_PER_HOUR   = 60 * 60
	SECOND_PER_DAY    = 60 * 60 * 24
)

// config
var Rlty int = RLTY_LOCALDB
var DefaultBlockExpire int = SECOND_PER_MINTUE * 5
var DefaultSlideWindowSize int = 4
var DefaultLeakyBucketExpire int = SECOND_PER_HOUR * 5
var RateLimiterRedisDb int = 0
var RateLimiterRedisPoolSize int = 32
var RateLimiterRedisAddrPort string
var RateLimiterRedisPassword string

type TreeLimit struct {
	Block    *BlockLimiter
	Level0   *LeakyBucketLimiter
	Level1   *SlideWindowLimiter
	Level2   *SlideWindowLimiter
	TspCache *TimeSequence
}

func NewTreeLimit(ctx context.Context) *TreeLimit {
	var step int = 1
	var tspcache *TimeSequence = NewTimeSequence(step)
	r := &TreeLimit{
		Block:    NewBlockLimiter(tspcache),
		Level0:   NewLeakyBucketLimiter(tspcache),
		Level1:   NewSlideWindowLimiter(tspcache, SECOND_PER_DAY),
		Level2:   NewSlideWindowLimiter(tspcache, SECOND_PER_DAY*30),
		TspCache: tspcache,
	}

	var doexpire = func(ictx context.Context) {
		for {
			select {
			case <-time.After(time.Duration(step) * time.Second):
			case <-ictx.Done():
				tool.Info("TreeLimit DoExpire Goroutine Canceled.")
				return
			}
			r.TspCache.SynchronizeWatche()
			r.Block.DoExpire()
			r.Level0.DoExpire()
			r.Level1.DoExpire()
			r.Level2.DoExpire()
		}
	}
	go doexpire(ctx)
	return r
}

type RedisLimit struct {
	Block     *RBlockLimiter
	Level0    *RLeakyBucketLimiter
	Level1    *RSlideWindowLimiter
	Level2    *RSlideWindowLimiter
	RedisPool *redis.Client
}

func NewRedisLimit(ctx context.Context) *RedisLimit {
	var rpool *redis.Client
	rpool = redis.NewClient(&redis.Options{
		DB:          RateLimiterRedisDb,
		Addr:        RateLimiterRedisAddrPort,
		PoolSize:    RateLimiterRedisPoolSize,
		Password:    RateLimiterRedisPassword,
		ReadTimeout: 5 * time.Second,
	})
	r := &RedisLimit{
		Block:     NewRBlockLimiter(rpool),
		Level0:    NewRLeakyBucketLimiter(rpool),
		Level1:    NewRSlideWindowLimiter(rpool, SECOND_PER_DAY),
		Level2:    NewRSlideWindowLimiter(rpool, SECOND_PER_DAY*30),
		RedisPool: rpool,
	}

	var contextCancel = func(ctx context.Context) {
		select {
		case <-ctx.Done():
			r.RedisPool.Close()
		}
		tool.Info("RedisLimit ContextCancel Goroutine Canceled.")
	}
	go contextCancel(ctx)
	return r
}

var Localdb *TreeLimit
var Redisdb *RedisLimit

type ContentBlock struct {
	Key     string
	Expire  int64
	Reason  uint8
	Counter int64
}

func NewContentBlock(key string, ts int64, defaultexpire int64) *ContentBlock {
	return &ContentBlock{
		Key:     key,
		Expire:  ts + defaultexpire,
		Counter: 1,
	}
}

type ContentLeakyBucket struct {
	Water     int64
	LastOutTp int64
}

func NewContentLeakyBucket(lastouttp int64) *ContentLeakyBucket {
	return &ContentLeakyBucket{
		Water:     0,
		LastOutTp: lastouttp,
	}
}

type ContentSlideWindow struct {
	Before      []int64
	LastSlideTp int64
}

func NewContentSlideWindow(lastouttp int64, beforesize int) *ContentSlideWindow {
	return &ContentSlideWindow{
		Before:      make([]int64, beforesize),
		LastSlideTp: lastouttp,
	}
}

func (ctk *ContentBlock) Block(ts int64, defaultexpire int64) *ContentBlock {
	if ctk.Expire < ts {
		ctk.Counter = 1
	} else {
		ctk.Counter = ctk.Counter + 1
	}
	ctk.Expire = ts + ctk.Counter*defaultexpire
	return ctk
}

func (clb *ContentLeakyBucket) CurrentWater(ts int64, risk int, leakrate int) int64 {
	var diffts int64
	var currwater int64

	diffts = ts - clb.LastOutTp
	if diffts > (1 << 45) {
		diffts = (1 << 45)
	}
	currwater = (clb.Water + int64(risk)) - tool.MoreThen0Int64(diffts)*int64(leakrate)
	return tool.MoreThen0Int64(currwater)
}

func (clb *ContentLeakyBucket) LeakyWater(ts int64, risk int, leakrate int, burst int) (currwater int64, allow bool) {
	currwater = clb.CurrentWater(ts, risk, leakrate)
	if currwater > int64(burst) {
		clb.Water = int64(burst)
		allow = false
	} else {
		clb.Water = currwater
		allow = true
	}
	clb.LastOutTp = ts
	currwater = clb.Water
	return
}

func (csw *ContentSlideWindow) SlideTotal(ts int64, slideperiod int64, risk int) int64 {
	var beforesize int
	var total, movestep, i int64

	total = 0
	beforesize = len(csw.Before)
	movestep = tool.MoreThen0Int64(ts-csw.LastSlideTp) / slideperiod
	if movestep == 0 {
		for i = 0; i < int64(beforesize); i += 1 {
			total += csw.Before[i]
		}
	} else if movestep < int64(beforesize) {
		for i = 0; i < movestep && i+movestep < int64(beforesize); i = i + 1 {
			if i+movestep < int64(beforesize) {
				total = total + csw.Before[i]
				csw.Before[movestep+i] = csw.Before[i]
			}
		}
		for i = 0; i < movestep; i += 1 {
			csw.Before[i] = 0
		}
	} else {
		for i = 0; i < int64(beforesize); i = i + 1 {
			csw.Before[i] = 0
		}
	}
	total = total + int64(risk)
	csw.LastSlideTp = ts
	csw.Before[0] = csw.Before[0] + int64(risk)
	return total
}

func IsBlock(ctx context.Context, idt string) (int64, bool, error) {
	var err error
	var period int64 = 0
	var isblock bool = true
	if Rlty == RLTY_LOCALDB {
		period, isblock = Localdb.Block.IsBlock(idt)
	} else if Rlty == RLTY_REDIS {
		period, isblock, err = Redisdb.Block.IsBlock(ctx, idt)
		if err != nil {
			return period, false, nil
		}
	}
	return period, isblock, nil
}

func Risk(ctx context.Context, idt string, leakrate int) (int64, int64, int64, error) {
	var err error
	var lv0, lv1, lv2 int64
	if Rlty == RLTY_LOCALDB {
		lv0 = Localdb.Level0.Risk(idt, leakrate)
		lv1 = Localdb.Level1.Risk(idt)
	} else if Rlty == RLTY_REDIS {
		lv0, err = Redisdb.Level0.Risk(ctx, idt, leakrate)
		if err != nil {
			return lv0, lv1, lv2, err
		}
		lv1, err = Redisdb.Level1.Risk(ctx, idt)
		if err != nil {
			return lv0, lv1, lv2, err
		}
	}

	return lv0, lv1, lv2, nil
}

func Allow(ctx context.Context, idt string, risk int, role *tool.UserInfo) (int64, bool, error) {
	var ok bool
	var err error
	var total int64

	var blockidt = func() (int64, bool, error) {
		var err error
		var period int64
		err = nil
		if Rlty == RLTY_LOCALDB {
			period = Localdb.Block.Block(idt)
		} else if Rlty == RLTY_REDIS {
			period, err = Redisdb.Block.Block(ctx, idt)
		}
		return period, false, err
	}

	if Rlty == RLTY_LOCALDB {
		total, ok = Localdb.Level0.Allow(idt, risk, role.LeakRate, role.Burst)
		if !ok {
			return blockidt()
		}
		total, ok = Localdb.Level1.Allow(idt, risk, role.Lv1Full)
		if !ok {
			return blockidt()
		}
		total, ok = Localdb.Level2.Allow(idt, risk, role.Lv2Full)
		if !ok {
			return blockidt()
		}
		_ = total
	} else if Rlty == RLTY_REDIS {
		total, ok, err = Redisdb.Level0.Allow(ctx, idt, risk, role.LeakRate, role.Burst)
		if err != nil {
			return 0, false, err
		}
		if !ok {
			return blockidt()
		}
		total, ok, err = Redisdb.Level1.Allow(ctx, idt, risk, role.Lv1Full)
		if err != nil {
			return 0, false, err
		}
		if !ok {
			return blockidt()
		}
		total, ok, err = Redisdb.Level2.Allow(ctx, idt, risk, role.Lv2Full)
		if err != nil {
			return 0, false, err
		}
		if !ok {
			return blockidt()
		}
	}
	return 0, true, nil
}

func InitRatelimiter(ctx context.Context) {
	if Rlty == RLTY_LOCALDB {
		Localdb = NewTreeLimit(ctx)
	} else if Rlty == RLTY_REDIS {
		Redisdb = NewRedisLimit(ctx)
	}
}
