package ratelimiter

import (
	"hfile/tool"
	"sync"

	singlylinkedlist "github.com/emirpasic/gods/lists/singlylinkedlist"
	"github.com/emirpasic/gods/maps/hashmap"
	redblacktree "github.com/emirpasic/gods/trees/redblacktree"
	godsutils "github.com/emirpasic/gods/utils"
)

type LeakyBucketLimiter struct {
	m             *ExpireMap
	TspCache      *TimeSequence
	DefaultExpire int64
}

func NewLeakyBucketLimiter(tspcache *TimeSequence) *LeakyBucketLimiter {
	return &LeakyBucketLimiter{
		m:             NewExpireMap(),
		TspCache:      tspcache,
		DefaultExpire: int64(DefaultLeakyBucketExpire),
	}
}

type BlockLimiter struct {
	Lock          sync.Mutex
	Limit         *hashmap.Map
	Expire        *redblacktree.Tree
	TspCache      *TimeSequence
	DefaultExpire int64
}

func NewBlockLimiter(tspcache *TimeSequence) *BlockLimiter {
	return &BlockLimiter{
		Limit:         hashmap.New(),
		Expire:        redblacktree.NewWith(godsutils.Int64Comparator),
		TspCache:      tspcache,
		DefaultExpire: int64(DefaultBlockExpire),
	}
}

type SlideWindowLimiter struct {
	m             *ExpireMap
	TspCache      *TimeSequence
	BeforeSize    int
	SlidePeriod   int64
	DefaultExpire int64
}

func NewSlideWindowLimiter(tspcache *TimeSequence, slideperiod int64) *SlideWindowLimiter {
	var beforesize int = DefaultSlideWindowSize
	return &SlideWindowLimiter{
		m:             NewExpireMap(),
		TspCache:      tspcache,
		BeforeSize:    beforesize,
		SlidePeriod:   slideperiod,
		DefaultExpire: slideperiod * (int64(beforesize) + 1),
	}
}

func (lbl *LeakyBucketLimiter) _GetContent(idt string) *ContentLeakyBucket {
	var node *ContentLeakyBucket

	itf, ok := lbl.m.Get(idt)
	if !ok {
		node = NewContentLeakyBucket(lbl.TspCache.CurrentLevel(0))
	} else {
		node = itf.(*ContentLeakyBucket)
	}

	return node
}

func (lbl *LeakyBucketLimiter) Allow(idt string, risk int, leakrate int, burst int) (int64, bool) {
	var ts int64
	var node *ContentLeakyBucket
	var allow bool
	var currwater int64

	if risk < 0 || leakrate < 0 || burst < 0 {
		tool.Error("LeakyBucketLimiter::Allow risk,leakrate,burst should large then 0")
		return 0, false
	}
	ts = lbl.TspCache.CurrentLevel(0)
	node = lbl._GetContent(idt)
	currwater, allow = node.LeakyWater(ts, risk, leakrate, burst)
	lbl.m.Put(idt, node, ts+lbl.DefaultExpire)
	return currwater, allow
}

func (lbl *LeakyBucketLimiter) Risk(idt string, leakrate int) int64 {
	currwater := lbl._GetContent(idt).CurrentWater(lbl.TspCache.CurrentLevel(0), 0, leakrate)
	return currwater
}

func (lbl *LeakyBucketLimiter) DoExpire() int64 {
	return lbl.m.DoExpire(lbl.TspCache.CurrentLevel(1))
}

func (swl *SlideWindowLimiter) _Get(idt string) *ContentSlideWindow {
	var node *ContentSlideWindow

	itf, ok := swl.m.Get(idt)
	if !ok {
		node = NewContentSlideWindow(swl.TspCache.CurrentLevel(0), swl.BeforeSize)
	} else {
		node = itf.(*ContentSlideWindow)
	}
	return node
}

func (swl *SlideWindowLimiter) Allow(idt string, risk int, full int64) (int64, bool) {
	var ts, total int64
	var node *ContentSlideWindow

	ts = swl.TspCache.CurrentLevel(0)
	node = swl._Get(idt)
	total = node.SlideTotal(ts, swl.SlidePeriod, risk)
	swl.m.Put(idt, node, ts+swl.DefaultExpire)
	if total > full {
		return total, false
	}
	return total, true
}

func (swl *SlideWindowLimiter) Risk(idt string) int64 {
	var total int64
	var tempnode ContentSlideWindow
	tempnode = *(swl._Get(idt))
	total = tempnode.SlideTotal(swl.TspCache.CurrentLevel(0), swl.SlidePeriod, 0)
	return total
}

func (swl *SlideWindowLimiter) DoExpire() int64 {
	return swl.m.DoExpire(swl.TspCache.CurrentLevel(1))
}

func (blt *BlockLimiter) IsBlock(idt string) (int64, bool) {
	ift, ok := blt._Get(idt)
	if !ok {
		return 0, false
	}
	node := ift.(*ContentBlock)
	if blt.TspCache.CurrentLevel(0) > node.Expire {
		return 0, false
	}
	return node.Expire, true
}

func (blt *BlockLimiter) Block(idt string) int64 {
	var ts int64
	var node *ContentBlock

	ts = blt.TspCache.CurrentLevel(0)
	ift, ok := blt._Get(idt)
	if !ok {
		node = NewContentBlock(idt, ts, blt.DefaultExpire)
		blt._Put(node.Key, node, node.Expire)
	} else {
		node = ift.(*ContentBlock)
		node.Block(ts, blt.DefaultExpire)
	}
	return node.Expire
}

func (blt *BlockLimiter) DoExpire() int64 {
	return blt._DoExpire(blt.TspCache.CurrentLevel(1))
}

func (blt *BlockLimiter) _DoExpire(tick int64) int64 {
	var cn int64 = 0
	var et int64
	var itflst []interface{} = make([]interface{}, 0)

	blt.Lock.Lock()
	for int(cn) < LConfExpireNodePre {
		if blt.Expire.Size() > 0 {
			et = blt.Expire.Left().Key.(int64)
		} else {
			break
		}
		if et > tick {
			break
		}
		eval := blt.Expire.Left().Value.(*singlylinkedlist.List)
		iter := eval.Iterator()
		for iter.Next() {
			o, ok := blt.Limit.Get(iter.Value())
			if ok {
				itflst = append(itflst, o)
			}

			k := iter.Value()
			blt.Limit.Remove(k)
			cn += 1

		}
		blt.Expire.Remove(et)
	}
	blt.Lock.Unlock()

	for _, o := range itflst {
		cbs := o.(*ContentBlock)
		if cbs.Expire > tick {
			blt._Put(cbs.Key, cbs, cbs.Expire)
			cn -= 1
		}
	}

	return cn
}

func (blt *BlockLimiter) _Get(key string) (interface{}, bool) {
	var found bool
	var itf interface{}
	blt.Lock.Lock()
	itf, found = blt.Limit.Get(key)
	blt.Lock.Unlock()
	return itf, found
}

func (blt *BlockLimiter) _PutCore(key string, content interface{}, expire int64, newOnly bool) bool {
	var sll *singlylinkedlist.List

	blt.Lock.Lock()
	_, ok := blt.Limit.Get(key)
	if !ok {
		isll, have := blt.Expire.Get(expire)
		if !have {
			sll = singlylinkedlist.New()
			blt.Expire.Put(expire, sll)
		} else {
			sll = isll.(*singlylinkedlist.List)
		}
		sll.Add(key)
	}
	if !(newOnly && ok) {
		blt.Limit.Put(key, content)
	}
	blt.Lock.Unlock()

	return !(newOnly && ok)
}

func (blt *BlockLimiter) _Put(key string, content interface{}, expire int64) bool {
	return blt._PutCore(key, content, expire, false)
}
