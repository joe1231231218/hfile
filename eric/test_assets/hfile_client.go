package main

import (
	"bufio"
	"context"
	"encoding/base64"
	"errors"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"strings"

	"github.com/rs/xid"
)

type ReaderStruct struct {
	Reset      func()
	Readerfunc func([]byte) (int, error)
}

func (r ReaderStruct) Read(b []byte) (int, error) {
	return r.Readerfunc(b)
}

type Session struct {
	Id        string
	LogMd     *os.File
	Auther    func(*http.Request) *http.Request
	Client    http.Client
	ServHost  string
	LogStdout bool
	TmpUpList [][]string
}

func NewSession() *Session {
	var s *Session
	s = &Session{
		LogStdout: false,
	}
	return s.New()
}

func (s *Session) New() *Session {
	var err error
	s.Id = xid.New().String()
	s.Client = http.Client{}
	s.ServHost = os.Getenv("servhost")
	s.LogMd, err = os.OpenFile(s.Id+".md", os.O_CREATE|os.O_TRUNC|os.O_RDWR, 664)
	s.TmpUpList = make([][]string, 0)
	if err != nil {
		fmt.Printf("Can not open %s %v\n", s.Id+".md", err)
	}
	s.LogLn("#%s\n", s.Id)
	return s
}

func (s *Session) End() {
	s.LogMd.Close()
}

func (s *Session) LogLn(format string, v ...interface{}) {
	var str string
	format = strings.Trim(format, "\n\r")
	str = fmt.Sprintf(format+"\n", v...)
	s.LogMd.WriteString(str)
	if s.LogStdout {
		fmt.Printf("%s", str)
	}
}

func (s *Session) Error(format string, v ...interface{}) {
	var str string
	str = fmt.Sprintf("ERROR: "+format, v...)
	s.LogMd.WriteString(str + "\n")
	fmt.Println(str)
}

/*
func (s *Session) CooKieAuth(user string, passwd string) func(*http.Request) *http.Request {

}
*/
func (s *Session) EmptyAuth() func(*http.Request) *http.Request {
	var empty = func(r *http.Request) *http.Request {
		return r
	}
	return empty
}

func (s *Session) BaseAuth(user string, passwd string) func(*http.Request) *http.Request {
	var base = func(r *http.Request) *http.Request {
		var auth string
		s.LogLn("- BaseAuth %s %s", user, passwd)
		auth = user + ":" + passwd
		auth = base64.StdEncoding.EncodeToString([]byte(auth))
		r.Header.Add("Authorization", "Basic "+auth)
		return r
	}
	return base
}

func (s *Session) FileReader(name string) func() (bool, *ReaderStruct) {
	var rfunc = func() (bool, *ReaderStruct) {
		var fd *os.File
		var err error
		var rstruct *ReaderStruct
		s.LogLn("FileReader %s", name)
		fd, err = os.OpenFile(name, os.O_RDONLY, 664)
		if err != nil {
			s.Error("Can not Open %s %v", name, err)
			_, rstruct = s.BuffReader(0, 0)()
			return false, rstruct
		}

		rstruct = &ReaderStruct{
			Readerfunc: func(b []byte) (int, error) {
				n, err := fd.Read(b)
				if errors.Is(err, io.EOF) {
					fd.Close()
				}
				return n, err
			},
			Reset: func() {
				fd.Close()
				fd, err = os.OpenFile(name, os.O_RDONLY, 664)
				if err != nil {
					s.Error("Can not Open %s %v", name, err)
					_, rstruct = s.BuffReader(0, 0)()
				}
			},
		}
		return true, rstruct
	}
	return rfunc
}

func (s *Session) BuffReader(buffType uint, repeat uint) func() (bool, *ReaderStruct) {
	var rfunc = func() (bool, *ReaderStruct) {
		var cn uint
		var buff []byte
		var readerstruct *ReaderStruct
		switch buffType {
		case 0:
			buff = make([]byte, 0)
		case 1:
			buff = make([]byte, 256)
			var i uint8
			for pos, _ := range buff {
				buff[pos] = i
				i += 1
			}
		default:
			buff = make([]byte, 0)
		}
		cn = 0
		readerstruct = &ReaderStruct{
			Readerfunc: func(b []byte) (int, error) {
				if cn < repeat {
					b = buff
					return len(buff), nil
				}
				cn += 1
				return 0, io.EOF
			},
			Reset: func() {
				cn = 0
			},
		}
		return true, readerstruct
	}
	return rfunc
}

func (s *Session) AdminAuther(r *http.Request) *http.Request {
	return s.BaseAuth("admin", "0000")(r)
}

func (s *Session) TmpUp(body func() (bool, *ReaderStruct)) bool {
	var ok bool
	var err error
	var req *http.Request
	var resp *http.Response
	var rstru *ReaderStruct
	var dlurl, delurl string
	var uprlt []string
	ctx, cancel := context.WithTimeout(context.Background(), 25)
	defer cancel()
	s.LogLn("### TmpUp\n")
	ok, rstru = body()
	if !ok {
		return false
	}
	req, err = http.NewRequestWithContext(ctx, "POST", s.ServHost+"/put", rstru)
	if err != nil {
		s.Error("NewRequestWithContext %v", err)
		return false
	}
	req.Header.Set("Content-Type", "application/octet-stream")
	req = s.Auther(req)
	resp, err = s.Client.Do(req)
	if err != nil {
		s.Error("Client::Do %v", err)
		return false
	}
	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		s.Error("ReadAllBody %v", err)
		return false
	}
	if resp.StatusCode == 200 {
		dlurl = strings.Trim(string(b), " \n\r")
		delurl = strings.TrimSpace(resp.Header.Get("X-Url-Delete"))
		uprlt = []string{dlurl, delurl}
		s.TmpUpList = append(s.TmpUpList, uprlt)
	}

	return true
}

func (s *Session) CrtRole() bool {
	var err error
	var url string
	var req *http.Request
	var resp *http.Response
	ctx, cancel := context.WithTimeout(context.Background(), 25)
	defer cancel()
	s.LogLn("### Create Role(%s)", s.Id)
	url = fmt.Sprintf("%s/aut/crt/%s/%d", s.ServHost, s.Id, 3)
	req, err = http.NewRequestWithContext(ctx, "GET", url, nil)
	if err != nil {
		s.Error("NewRequestWithContext %v", err)
		return false
	}
	req = s.AdminAuther(req)
	resp, err = s.Client.Do(req)
	if err != nil {
		s.Error("Client::Do %v", err)
		return false
	}
	if resp.StatusCode == 200 {
		s.Auther = s.BaseAuth(s.Id, "0000")
	}
	return true
}

func (s *Session) User() {
	s.Auther = s.EmptyAuth()
	s.TmpUp(s.FileReader("9740"))
}

func CurlI(parserCurli string, XDeleteUrl bool) int {
	const (
		iinit int = iota
		i100
		iheader
		ibody
	)
	var fd *os.File
	var err error
	var body string
	var readStatus int = iinit
	var currentLine int = 0
	var statCodeFd *os.File
	var doXDeleteUrl bool = false
	var readEmptyLine bool
	//	fmt.Printf("Run Curl-i %s\n", parserCurli)
	fd, err = os.OpenFile(parserCurli, os.O_RDWR, 664)
	if err != nil {
		fmt.Printf("curl-i open file %v", err)
		return 1
	}
	defer fd.Close()
	statCodeFd, err = os.OpenFile("_code", os.O_CREATE|os.O_TRUNC|os.O_WRONLY, os.FileMode(0664))
	if err != nil {
		fmt.Printf("curl-i open _code %v", err)
		return 1
	}
	defer statCodeFd.Close()
	sc := bufio.NewScanner(fd)
	for sc.Scan() {
		if sc.Err() != nil {
			fmt.Printf("curl-i file %v\n", err)
			return 1
		}
		s := sc.Text()
		if len(s) == 0 {
			readEmptyLine = true
		} else {
			readEmptyLine = false
		}
		switch readStatus {
		case iinit:
			ls := strings.Split(s, " ")
			if len(ls) >= 3 {
				//fmt.Println(ls)
				statCodeFd.Seek(0, io.SeekStart)
				statCodeFd.Truncate(0)
				fmt.Fprintf(statCodeFd, "%s", strings.TrimSpace(ls[1]))
			} else {
				fmt.Printf("Can not get Status code in first line\n")
				return 1
			}
			if strings.TrimSpace(ls[1]) == "100" {
				readStatus = i100
			} else {
				readStatus = iheader
			}
		case i100:
			if readEmptyLine {
				readStatus = iinit
			}
		case iheader:
			if readEmptyLine {
				readStatus = ibody
			} else if XDeleteUrl {
				if strings.Contains(s, "X-Url-Delete:") {
					XDeleteUrl = false
					doXDeleteUrl = true
					delfd, err := os.OpenFile("_del", os.O_CREATE|os.O_TRUNC|os.O_WRONLY, os.FileMode(0664))
					if err != nil {
						fmt.Printf("Can not open del %v", err)
						return 1
					}
					defer delfd.Close()
					s = strings.Replace(s, "X-Url-Delete:", "", 1)
					s = strings.TrimSpace(s)
					fmt.Fprint(delfd, s)
				}
			}
		case ibody:
			body += s + "\n"
		default:
			fmt.Printf("UnExpect readStatus %d", readStatus)
			return 1

		}
		currentLine += 1
	}
	if !(readStatus == ibody) {
		fmt.Printf("UnExpect readStatus End parser %d", readStatus)
		return 1
	}
	fd.Seek(0, io.SeekStart)
	fd.Truncate(0)
	fmt.Fprintf(fd, "%s", strings.Trim(body, " \n\r"))
	if doXDeleteUrl && XDeleteUrl {
		os.Remove("_del")
	}
	//fmt.Printf("End Curl-i %s\n", parserCurli)
	return 0
}

func main() {
	var exitCode int
	var userStr string
	var XDeleteUrl bool
	var parserCurli string

	exitCode = 0
	flag.StringVar(&userStr, "user", "", "")
	flag.StringVar(&parserCurli, "curl-i", "", "Parser curl -i generated file, save status code to _code, save body to original file.")
	flag.BoolVar(&XDeleteUrl, "X-Del", false, "save X-Url-Delete header to del if header exist.")
	flag.Parse()

	if len(parserCurli) > 0 {
		exitCode = CurlI(parserCurli, XDeleteUrl)
		os.Exit(exitCode)
		return
	}
	if len(userStr) != 0 {
		var sess *Session
		sess = NewSession()
		sess.User()
	}

}
