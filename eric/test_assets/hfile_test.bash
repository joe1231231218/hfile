PS4='${LINENO}: '

declare servhost=127.0.0.1:8080
declare tmpresult="_tmp"
declare delobjurl=""
declare saveumask=""
declare adminpasswd="admin:0000"
declare hfileclient="hfile_client"
declare downloadobjurl=""
declare downloadsmallurl=""

if [[ "${OSTYPE}" == "msys" ]] ;then
	hfileclient="${hfileclient}.exe"
fi

saveumask=$(umask)
umask 002

function CurlIByTmp(){
	"./${hfileclient}" -curl-i ${tmpresult} -X-Del
	if [ $? -eq 0 ] 
	then
		if [ $(cat _code) != "200" ]
		then
			echo "Status code $(cat _code)"
			return 2
		fi
		return 0
	fi
	return 1
}

function DownloadOneDelete(){
	CurlIByTmp
	if [ $? -eq 0 ] 
	then
		downloadobjurl=$(cat  ${tmpresult})
		curl --no-progress-meter -o _tmp "${downloadobjurl}"
		if [ -f _del ]
		then
			delobjurl=$(cat _del)
			curl --no-progress-meter -i -o _tmp "${delobjurl}"
			CurlIByTmp
			return 0
		fi
	fi
	return 1
}

echo index
curl --no-progress-meter -o _tmp "${servhost}/"
curl --no-progress-meter -o _tmp "${servhost}/index.html"

echo Hi
curl --no-progress-meter "${servhost}/hi"

echo header
curl --no-progress-meter "${servhost}/header"

echo "method put small on root with filename"
curl --no-progress-meter -i --upload-file 9740 "${servhost}" > _tmp
CurlIByTmp
if [ $? -eq 0 ] 
then
	downloadsmallurl=$(cat _tmp)
	echo "download small url ${downloadsmallurl}"
fi

echo "method put BbZh.pdf on root with filename"
curl --no-progress-meter -i --upload-file BbZh.pdf "${servhost}" > _tmp
CurlIByTmp
if [ $? -eq 0 ] 
then
	downloadobjurl=$(cat _tmp)
fi

if [ "$downloadsmallurl" != "" ]
then
	echo "download small ${downloadsmallurl}"
	curl --no-progress-meter -i -o _tmp "${downloadsmallurl}" 
fi

if [ "$downloadobjurl" != "" ]
then
	echo "download BbZh.pdf"
	curl --no-progress-meter -o __tmp "${downloadobjurl}"
	cmp -s __tmp BbZh.pdf
	if [ $? -ne 0 ]
	then
		echo "BbZh.pdf not match"
	fi
fi

echo "method post small"
curl --no-progress-meter -i --data-urlencode file@9740 "${servhost}" > _tmp
DownloadOneDelete


echo "method post BbZh.pdf"
curl --no-progress-meter -i --data-urlencode BbZh@BbZh.pdf "${servhost}" > _tmp 
DownloadOneDelete

echo "multipart form post"
curl --no-progress-meter -i -F file=@BbZh.pdf "${servhost}" > _tmp
DownloadOneDelete

echo "method put and post small without filename"
curl --no-progress-meter -i -X "PUT" --data-urlencode file@9740 "${servhost}" > _tmp
CurlIByTmp
curl --no-progress-meter -i -X "POST" --data-urlencode file@9740 "${servhost}" > _tmp
CurlIByTmp
curl --no-progress-meter -i -X "PUT" -H "Content-Type: application/octet-stream" --data-binary @9740 "${servhost}" > _tmp
CurlIByTmp
curl --no-progress-meter -i -X "POST" -H "Content-Type: application/octet-stream" --data-binary @9740 "${servhost}" > _tmp
CurlIByTmp

curl --no-progress-meter -i -X "PUT" --data-urlencode file@9740 "${servhost}/put" > _tmp
CurlIByTmp
curl --no-progress-meter -i -X "POST" --data-urlencode file@9740 "${servhost}/put" > _tmp
CurlIByTmp
curl --no-progress-meter -i -X "PUT" -H "Content-Type: application/octet-stream" --data-binary @9740 "${servhost}/put" > _tmp
CurlIByTmp
curl --no-progress-meter -i -X "POST" -H "Content-Type: application/octet-stream" --data-binary @9740 "${servhost}/put" > _tmp
CurlIByTmp

curl --no-progress-meter -i -X "PUT" --data-urlencode file@9740 "${servhost}/put/9740" > _tmp
CurlIByTmp
curl --no-progress-meter -i -X "POST" --data-urlencode file@9740 "${servhost}/put/9740" > _tmp
CurlIByTmp
curl --no-progress-meter -i -X "PUT" -H "Content-Type: application/octet-stream" --data-binary @9740 "${servhost}/put/9740" > _tmp
CurlIByTmp
curl --no-progress-meter -i -X "POST" -H "Content-Type: application/octet-stream" --data-binary @9740 "${servhost}/put/9740" > _tmp
CurlIByTmp


echo "Rename"
curl --no-progress-meter -i -X "POST" -T 9740 -H "X-Rename: Rename" "${servhost}" > _tmp
CurlIByTmp
if [ $? -eq 0 ]
then
	echo "download $(cat _tmp)"
	curl --no-progress-meter -i $(cat _tmp) > _tmp
	CurlIByTmp
fi

echo "Acl"
curl --no-progress-meter -i -X "POST" -T 9740 -H "X-Acl: admin" "${servhost}" > _tmp
CurlIByTmp
curl --no-progress-meter -i -X "POST" -T 9740 -H "X-Acl: admin" "${servhost}" > _tmp
DownloadOneDelete

echo "Max-download"
curl --no-progress-meter -i -X "POST" -T 9740 -H "X-Remain: 1" "${servhost}" > _tmp
CurlIByTmp
if [ $? -eq 0 ]
then
	cp -f _tmp _url
	curl --no-progress-meter -i -o _tmp $(cat _url)
	CurlIByTmp
	if [ $? -eq 0 ]
	then
		curl --no-progress-meter -i -o _tmp $(cat _url)
		"./${hfileclient}" -curl-i ${tmpresult}
		if [ $(cat _code) != "500" ]; then
			echo "Expect expire."
		fi
	fi
fi

echo "X-Expire"
curl --no-progress-meter -i -X "POST" -T 9740 -H "X-Expire: 10" "${servhost}" > _tmp
CurlIByTmp

echo "Qrcode AbCdE"
curl --no-progress-meter -o AbCdE.png "${servhost}/qrc/AbCdE"
curl --no-progress-meter -o _tmp "${servhost}/qrc/AbCdE.png"
if cmp -s _tmp AbCdE.png
then
	echo "/qrc/AbCdE /qrc/AbCdE.png not match"
fi

echo "Path not exist"
curl --no-progress-meter -i -o _tmp "${servhost}/path/not/exist"
"./${hfileclient}" -curl-i _tmp
if [ $(cat _code) != "404" ]
then
	echo "Expect ${servhost}/path/not/exist return 404"
fi
curl --no-progress-meter -i -o _tmp "${servhost}/qrc/fffff/exist"
"./${hfileclient}" -curl-i _tmp
if [ $(cat _code) != "404" ]
then
	echo "Expect ${servhost}/qrc/fffff/exist return 404"
fi

echo "Method not allow"
curl --no-progress-meter -i "${servhost}/put/fffff" > _tmp
"./${hfileclient}" -curl-i _tmp
if [ $(cat _code) != "405" ]
then
	echo "Expect return 405"
fi

echo "BaseAuth"
curl --no-progress-meter -u ${adminpasswd} -X "POST" -T 9740 "${servhost}/put" > _tmp
CurlIByTmp

echo "AuthCsv"
curl --no-progress-meter -u ${adminpasswd} -o auth.csv "${servhost}/aut/csv"
curl --no-progress-meter -i -u ${adminpasswd} -X "POST" -T auth.csv "${servhost}/aut/csv" > _tmp
CurlIByTmp
curl --no-progress-meter -u ${adminpasswd} -o auth_t.csv "${servhost}/aut/csv"
cmp -s auth.csv auth_t.csv
if [ $? -ne 0 ]
then
	echo "Expect same AuthCsv"
fi

echo "RatelimiterStatus"
curl --no-progress-meter "${servhost}/rat/stat"
curl --no-progress-meter -u ${adminpasswd} "${servhost}/rat/stat"

umask ${saveumask}