package tmpfs

import (
	"context"
	"errors"
	"hfile/tool"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"

	"github.com/emirpasic/gods/sets/treeset"
)

// Todo move Dir_tmp to TempfileStoreDirectory
var Dir_tmp string

func TempfileInternalOpen(fpath string, flag int, emethod string) (*os.File, *TmpFsError) {
	f, err := os.OpenFile(fpath, flag, 664)
	if err != nil {
		etype := "Unknow"
		if errors.Is(err, os.ErrNotExist) {
			etype = "NotExistPath"
		} else if errors.Is(err, os.ErrExist) {
			etype = "LinkExistPath"
		}
		ierr := _NewTmpFsError(emethod, etype, tool.INFO, err)
		ierr.Path = fpath
		return f, ierr
	}
	return f, nil
}

func _TempfileRead(ctxc context.Context, ctx context.Context, folder string, dryrun bool) (*os.File, *tool.NodeInfo, *TmpFsError) {
	var err error
	var ierr *TmpFsError
	var ninfo *tool.NodeInfo
	var rinfo io.Reader
	var meta, fd *os.File
	var fileinfo os.FileInfo
	var metapath, nodepath string

	var openfunc = func(fpath string, flag int) (*os.File, *TmpFsError) {
		return TempfileInternalOpen(fpath, flag, "TempFileRead")
	}
	metapath = filepath.Join(Dir_tmp, folder, "metadata")

	meta, ierr = openfunc(metapath, os.O_RDONLY)
	if ierr != nil {
		return fd, ninfo, ierr
	}
	ninfo, rinfo, ierr = UpdateMataJsonWhenRead(meta.Read, metapath, "TempFileRead")
	meta.Close()
	if ierr != nil {
		return fd, ninfo, ierr
	}

	if !dryrun {
		meta, ierr = openfunc(metapath, os.O_WRONLY|os.O_TRUNC)
		if ierr != nil {
			return fd, ninfo, ierr
		}
		_, err := io.Copy(meta, rinfo)
		meta.Close()
		if err != nil {
			return fd, ninfo, _NewTmpFsError("TempFileRead", "UnknowWriteErr", tool.WARN, err)
		}
	}

	nodepath = filepath.Join(Dir_tmp, folder, "node")
	fileinfo, err = os.Stat(nodepath)
	if err != nil {
		var errmsg string = "Unknow"
		if errors.Is(err, os.ErrNotExist) {
			errmsg = "NotExistPath"
		}
		ierr = _NewTmpFsError("TempFileRead", errmsg, tool.DEBUG, err)
		return fd, ninfo, ierr
	}
	ninfo.Size = fileinfo.Size()
	fd, ierr = openfunc(nodepath, os.O_RDONLY)
	if ierr != nil {
		return fd, ninfo, ierr
	}

	return fd, ninfo, nil
}

func _TempfileLink(ctxc context.Context, ctx context.Context, inf *tool.NodeInfo, folder string) (*os.File, *TmpFsError) {
	var fd, meta *os.File
	var ierr *TmpFsError
	var openfunc = func(fpath string, flag int) (*os.File, *TmpFsError) {
		return TempfileInternalOpen(fpath, flag, "TempFileLink")
	}
	folderp := filepath.Join(Dir_tmp, folder)

	_, err := os.Stat(folderp)
	if !errors.Is(err, os.ErrNotExist) {
		var msg string
		var etype string
		if err != nil {
			msg = err.Error()
			etype = "Unknow"
		} else {
			msg = "LinkExistPath"
			etype = "LinkExistPath"
		}
		ierr = _NewTmpFsError("TempFileLink", etype, tool.WARN, errors.New(msg))
		ierr.Folder = folderp
		return fd, ierr
	}
	tool.TmpfsChangeFsLock()
	err = os.Mkdir(folderp, 664)
	tool.TmpfsChangeFsUnlock()
	if err != nil {
		ierr := _NewTmpFsError("TempFileLink", "Unknow", tool.WARN, err)
		ierr.Folder = folderp
		return fd, ierr
	}
	metapath := filepath.Join(folderp, "metadata")
	meta, ierr = openfunc(metapath, os.O_WRONLY|os.O_CREATE)
	if ierr != nil {
		return fd, ierr
	}
	defer meta.Close()
	ierr = CreateMataJsonWhenLink(meta.Write, inf, "TempFileLink")
	if ierr != nil {
		ierr.Path = metapath
		return fd, ierr
	}

	fd, ierr = openfunc(filepath.Join(folderp, "node"), os.O_WRONLY|os.O_CREATE)
	if ierr != nil {
		return fd, ierr
	}
	return fd, nil
}

func _TempfileUnlink(folder string) *TmpFsError {
	var ierr *TmpFsError
	folderp := filepath.Join(Dir_tmp, folder)

	inf, err := os.Stat(folderp)
	if err != nil {
		etype := "Unknow"
		if errors.Is(err, os.ErrNotExist) {
			etype = "NotExistPath"
		}
		ierr := _NewTmpFsError("TempFileUnlink", etype, tool.WARN, err)
		ierr.Folder = folderp
		return ierr
	}
	if !inf.IsDir() {
		ierr = _NewTmpFsError("TempFileUnlink", "NotDir", tool.WARN, errors.New(""))
		ierr.Folder = folderp
		return ierr
	}

	tool.TmpfsChangeFsLock()
	err = os.RemoveAll(folderp)
	tool.TmpfsChangeFsUnlock()
	if err != nil {
		ierr := _NewTmpFsError("TempFileUnlink", "Unknow", tool.WARN, err)
		ierr.Folder = folderp
		return ierr
	}
	return nil

}

func _TempfileScan(ctx context.Context, rmExpire bool) (*treeset.Set, *TmpFsError) {
	var ierr *TmpFsError
	used := treeset.NewWithStringComparator()
	infls, err := ioutil.ReadDir(Dir_tmp)
	if err != nil {
		ierr = _NewTmpFsError("TempFileScan", "Unknow", tool.ERROR, err)
		return used, ierr
	}
	for _, fl := range infls {
		nm := fl.Name()
		if len(nm) == PrefixLen+SuffixLen && PrefixSymbolCheck(nm[0:PrefixLen]) {
			used.Add(nm[0:PrefixLen])
		}
		if rmExpire {
			fd, _, err := _TempfileRead(ctx, ctx, nm, true)
			fd.Close()
			if err != nil {
				if err.StructDamage {
					ierr = _TempfileUnlink(nm)
					if ierr != nil {
						tool.Logf(tool.TMPFSSCANRM, "folder %s Unlink %v", nm, ierr)
					}
					tool.Logf(tool.TMPFSSCANRM, "folder %s rm cause %s", nm, err.Error())
				}
			}
		}
		select {
		case <-ctx.Done():
			return used, _NewTmpFsError("TempFileScan", "ContextCancel", tool.ERROR, tool.ContextTimeOutErr)
		default:
		}
	}
	return used, nil
}
