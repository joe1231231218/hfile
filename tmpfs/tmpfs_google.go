package tmpfs

import (
	"context"
	"errors"
	"fmt"
	"hfile/tool"
	"io"
	"time"

	"cloud.google.com/go/storage"
	"github.com/googleapis/gax-go/v2"
	"google.golang.org/api/googleapi"
	"google.golang.org/api/iterator"
	"google.golang.org/api/option"

	"github.com/emirpasic/gods/sets/treeset"
)

var GoogleStorageBucket string
var ServiceAccountCredentialJSON string

type GoogleStorageIo struct {
	R      *storage.Reader
	W      *storage.Writer
	Client *storage.Client
}

func (gsi *GoogleStorageIo) Read(p []byte) (int, error) {
	if gsi.R != nil {
		return gsi.R.Read(p)
	}
	return 0, _NewTmpFsError("GoogleStorage", "MethodNotImplemented", tool.ERROR, errors.New("GoogleStorageIoMethodNotImplemented"))
}

func (gsi *GoogleStorageIo) Write(p []byte) (int, error) {
	if gsi.W != nil {
		return gsi.W.Write(p)
	}
	return 0, _NewTmpFsError("GoogleStorage", "MethodNotImplemented", tool.ERROR, errors.New("GoogleStorageIoMethodNotImplemented"))
}

func (gsi *GoogleStorageIo) Close() error {
	var err error
	if gsi.R != nil {
		err = gsi.R.Close()
		if err != nil {
			_NewTmpFsError("GoogleStorage", "UnknowReadErr", tool.ERROR, err)
		}
	}
	if gsi.W != nil {
		err = gsi.W.Close()
		if err != nil {
			_NewTmpFsError("GoogleStorage", "UnknowWriteErr", tool.ERROR, err)
		}
	}
	gsi.Client.Close()
	return nil
}

func NewGoogleStorageIo(r *storage.Reader, w *storage.Writer, client *storage.Client) *GoogleStorageIo {
	return &GoogleStorageIo{
		R:      r,
		W:      w,
		Client: client,
	}
}

func GoogleInternalErrorTransfer(err error) *TmpFsError {
	var ok bool
	var etype string
	var terr *TmpFsError
	var gerr *googleapi.Error
	var statcode int
	etype = "Unknow"
	statcode = 0
	if ok = errors.As(err, &gerr); ok {
		if gerr.Code == 429 {
			statcode = 429
			etype = "TooManyRequests"
		}
	}
	terr = _NewTmpFsError("GoogleStorage", etype, tool.ERROR, err)
	if statcode != 0 {
		terr.StatCode = int16(statcode)
	}
	return terr
}

func GoogleInternalCreate(ctx context.Context, obj *storage.ObjectHandle) (*storage.Writer, *TmpFsError) {
	var err error
	var terr *TmpFsError
	var attr *storage.ObjectAttrs
	var writer *storage.Writer
	var isexist bool

	isexist = true
	attr, err = obj.Attrs(ctx)
	if err != nil {
		if errors.Is(err, storage.ErrObjectNotExist) {
			isexist = false
		} else {
			terr = GoogleInternalErrorTransfer(err)
		}
	} else {
		terr = _NewTmpFsError("GoogleStorage", "LinkExistPath", tool.WARN, errors.New("GoogleStorage::GoogleInternalCreate LinkExistPath"))
	}
	if isexist {
		return nil, terr
	}
	_ = attr
	writer = obj.NewWriter(ctx)
	return writer, nil
}

func GoogleInternalDownload(ctx context.Context, obj *storage.ObjectHandle) (*storage.Reader, *TmpFsError) {
	var err error
	var terr *TmpFsError
	var reader *storage.Reader
	reader, err = obj.NewReader(ctx)
	if err != nil {
		if errors.Is(err, storage.ErrObjectNotExist) {
			terr = _NewTmpFsError("GoogleStorage", "NotExistPath", tool.WARN, err)
		} else {
			terr = _NewTmpFsError("GoogleStorage", "Unknow", tool.ERROR, err)
		}
		return nil, terr
	}
	return reader, nil
}

// if error, storage.Client close by callee
func GoogleStorageProject(ctx context.Context) (*storage.Client, *storage.BucketHandle, *TmpFsError) {
	var err error
	var terr *TmpFsError
	var client *storage.Client
	var bucket *storage.BucketHandle

	client, err = storage.NewClient(ctx, option.WithCredentialsJSON([]byte(ServiceAccountCredentialJSON)))
	client.SetRetry(
		storage.WithBackoff(
			gax.Backoff{
				Max: time.Second * 20,
			}),
		storage.WithPolicy(storage.RetryIdempotent),
	)
	if err != nil {
		terr = _NewTmpFsError("GoogleStorage", "AccessAuthorizateErr", tool.ERROR, err)
		if client != nil {
			client.Close()
		}
		return nil, nil, terr
	}
	bucket = client.Bucket(GoogleStorageBucket)
	if _, err = bucket.Attrs(ctx); err != nil {
		if errors.Is(err, storage.ErrBucketNotExist) {
			terr = _NewTmpFsError("GoogleStorage", "BucketAccess", tool.ERROR, err)
		} else {
			terr = _NewTmpFsError("GoogleStorage", "Unknow", tool.ERROR, err)
		}
		client.Close()
		return nil, nil, terr
	}
	return client, bucket, nil
}

func _TempGoogleRead(ctxc context.Context, ctx context.Context, folder string, dryrun bool) (*GoogleStorageIo, *tool.NodeInfo, *TmpFsError) {
	var fd *storage.Reader
	var err error
	var terr *TmpFsError
	var rinfo io.Reader
	var ninfo *tool.NodeInfo
	var attr *storage.ObjectAttrs
	var client *storage.Client
	var bucket *storage.BucketHandle
	var writer *storage.Writer
	var metakey, nodekey string

	metakey, nodekey = StorjS3FolderChild(folder)
	client, bucket, terr = GoogleStorageProject(ctxc)
	if terr != nil {
		return nil, ninfo, terr
	}

	var cleanUpAterClientReady = func(terr *TmpFsError) (*GoogleStorageIo, *tool.NodeInfo, *TmpFsError) {
		client.Close()
		return nil, ninfo, terr
	}
	fd, terr = GoogleInternalDownload(ctx, bucket.Object(metakey))
	if terr != nil {
		if terr.Etype == TmpFsErrorTypeFind("NotExistPath") {
			terr.StatCode = 404
		}
		return cleanUpAterClientReady(terr)
	}
	ninfo, rinfo, terr = UpdateMataJsonWhenRead(fd.Read, metakey, "GoogleStorage")
	fd.Close()
	if terr != nil {
		return cleanUpAterClientReady(terr)
	}
	if !dryrun {
		writer = bucket.Object(metakey).NewWriter(ctx)
		_, err = io.Copy(writer, rinfo)
		writer.Close()
		if err != nil {
			return cleanUpAterClientReady(_NewTmpFsError("GoogleStorage", "UnknowWriteErr", tool.ERROR, err))
		}
	}
	fd, terr = GoogleInternalDownload(ctxc, bucket.Object(nodekey))
	if terr != nil {
		return cleanUpAterClientReady(terr)
	}
	attr, err = bucket.Object(nodekey).Attrs(ctx)
	if err != nil {
		terr = GoogleInternalErrorTransfer(err)
		return cleanUpAterClientReady(terr)
	}
	ninfo.Size = attr.Size
	return NewGoogleStorageIo(fd, nil, client), ninfo, nil
}

func _TempGoogleLink(ctxc context.Context, ctx context.Context, inf *tool.NodeInfo, folder string) (*GoogleStorageIo, *TmpFsError) {
	var fd *GoogleStorageIo
	var err error
	var terr *TmpFsError
	var metakey, nodekey string
	var writer *storage.Writer
	var client *storage.Client
	var bucket *storage.BucketHandle
	metakey, nodekey = StorjS3FolderChild(folder)

	client, bucket, terr = GoogleStorageProject(ctxc)
	if terr != nil {
		return nil, terr
	}

	var cleanUpAterClientReady = func(terr *TmpFsError) (*GoogleStorageIo, *TmpFsError) {
		client.Close()
		return nil, terr
	}
	writer, terr = GoogleInternalCreate(ctx, bucket.Object(metakey))
	if terr != nil {
		return cleanUpAterClientReady(terr)
	}
	terr = CreateMataJsonWhenLink(writer.Write, inf, "GoogleStorage")
	err = writer.Close()
	if terr != nil {
		return cleanUpAterClientReady(terr)
	}
	if err != nil {
		return cleanUpAterClientReady(_NewTmpFsError("GoogleStorage", "UnknowWriteErr", tool.ERROR, err))
	}
	writer, terr = GoogleInternalCreate(ctxc, bucket.Object(nodekey))
	if terr != nil {
		return cleanUpAterClientReady(terr)
	}
	fd = NewGoogleStorageIo(nil, writer, client)
	return fd, nil
}

func _TempGoogleUnlink(ctx context.Context, folder string) *TmpFsError {
	var terr *TmpFsError
	var client *storage.Client
	var bucket *storage.BucketHandle
	var metakey, nodekey string
	metakey, nodekey = StorjS3FolderChild(folder)

	client, bucket, terr = GoogleStorageProject(ctx)
	if terr != nil {
		fmt.Println(terr)
	}
	defer client.Close()
	bucket.Object(metakey).Delete(ctx)
	bucket.Object(nodekey).Delete(ctx)
	return nil
}

func _TempGoogleScan(ctx context.Context, rmExpire bool) (*treeset.Set, *TmpFsError) {
	var err error
	var used *treeset.Set = treeset.NewWithStringComparator()
	var terr *TmpFsError
	var attr *storage.ObjectAttrs
	var iter *storage.ObjectIterator
	var client *storage.Client
	var bucket *storage.BucketHandle
	client, bucket, terr = GoogleStorageProject(ctx)
	if terr != nil {
		return used, terr
	}
	defer client.Close()
	iter = bucket.Objects(ctx, &storage.Query{Prefix: ""})
	for {
		attr, err = iter.Next()
		if err != nil {
			if errors.Is(err, iterator.Done) {
				break
			}
			return used, _NewTmpFsError("GoogleStorage", "Unknow", tool.ERROR, err)
		}
		//fmt.Print(attr.Name)
		ok, k := StorjS3Key2UsedPrefix(attr.Name)
		if ok {
			used.Add(k)
		}
	}
	return used, nil
}
