package tmpfs

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"hfile/tool"
	"io"
	"strings"
	"time"

	"github.com/emirpasic/gods/sets/treeset"
	godsutils "github.com/emirpasic/gods/utils"
)

var _SymbolSet *treeset.Set = treeset.NewWith(godsutils.Int32Comparator)

func _MapIntStrFind(sp string, tarmap map[int]string) (int, bool) {
	for i, s := range tarmap {
		if strings.ToLower(sp) == strings.ToLower(s) {
			return i, true
		}
	}
	return 0, false
}

func TmpFsErrorTypeFind(errorType string) int {
	var i int
	var ok bool
	if i, ok = _MapIntStrFind(errorType, TmpFsEtype); !ok {
		i = TmpFsErrorStrNotMatchCode
		tool.Error("Etype(%s) not find in TmpFsEtype", errorType)
	}
	return i
}

func LfsrFibonacci6Bit(b uint8) uint8 {
	//taps: 6 5
	var lfsr uint16
	lfsr = uint16(b)
	h := (lfsr >> 2) ^ (lfsr>>3)&1
	lfsr = (lfsr)>>1 | (h << 6)
	return uint8(lfsr & 0x3f)
}

func LfsrFibonacci8Bit(b uint8) uint8 {
	//taps: 8 6 5 4
	var lfsr uint16
	lfsr = uint16(b)
	h := (lfsr ^ (lfsr >> 2) ^ (lfsr >> 3) ^ (lfsr >> 4)) & 1
	lfsr = (lfsr)>>1 | (h << 8)
	return uint8(lfsr & 0xff)
}

func LfsrFibonacci24Bit(b uint32) uint32 {
	//taps: 24 23 22 17
	var lfsr uint32
	lfsr = b
	h := (lfsr ^ (lfsr >> 1) ^ (lfsr >> 2) ^ (lfsr >> 6)) & 1
	lfsr = (lfsr)>>1 | (h << 24)
	return lfsr & 0x0ffffff
}

func StorjS3FolderChild(folder string) (string, string) {
	metadate := fmt.Sprintf("%s/%s", folder, "metadata")
	node := fmt.Sprintf("%s/%s", folder, "node")
	return metadate, node
}

func _SymbolCheck(s string) bool {
	var ci int32
	for _, c := range s {
		ci = int32(c)
		if !_SymbolSet.Contains(ci) {
			return false
		}
	}
	return true
}

func FolderSymbolCheckRetTmpfsError(folder string, errmethod string) *TmpFsError {
	var terr *TmpFsError = nil

	if len(folder) != PrefixLen+SuffixLen {
		terr = _NewTmpFsError(errmethod, "ErrPathFormat", tool.DEBUG, errors.New(fmt.Sprintf("The length(%d) of the folder is incorrect.", PrefixLen+SuffixLen)))
		terr.StatCode = 404
		return terr
	}
	if !_SymbolCheck(folder) {
		terr = _NewTmpFsError(errmethod, "ErrPathFormat", tool.DEBUG, errors.New(fmt.Sprintf("Contains unspecified characters(%s)", SYMBOLS)))
		terr.StatCode = 404
		return terr
	}
	return nil
}

func PrefixSymbolCheck(prefix string) bool {
	return _SymbolCheck(prefix)
}

func StorjS3Key2UsedPrefix(key string) (bool, string) {
	k := strings.TrimSpace(key)
	kl := strings.Split(k, "/")
	//fmt.Printf("\n%q %s %d %d\n", kl, k, len(kl), strings.Count(kl[0], ""))
	if len(kl) > 0 && strings.Count(kl[0], "") == (PrefixLen+SuffixLen+1) {
		k = kl[0][0:PrefixLen]
		if PrefixSymbolCheck(k) {
			return true, k
		}
	}

	return false, ""
}

func TmpFsErrorNil2ErrorNil(terr *TmpFsError) error {
	if terr == nil {
		return nil
	}
	return terr
}

func UpdateMataJsonWhenRead(readerfunc func([]byte) (int, error), filepath string, emethod string) (*tool.NodeInfo, io.Reader, *TmpFsError) {
	var err error
	var buff []byte
	var ierr *TmpFsError
	var ninfo tool.NodeInfo

	buff, err = tool.ReadNByte(readerfunc, MaxMetadataLen)
	if err != nil {
		ierr = _NewTmpFsError(emethod, "UnknowReadErr", tool.WARN, err)
		return &ninfo, nil, ierr
	}
	if len(buff) >= MaxMetadataLen {
		ierr = _NewTmpFsError(emethod, "MetadataTooLarge", tool.WARN, errors.New(""))
		ierr.StructDamage = true
		return &ninfo, nil, ierr
	}

	err = json.Unmarshal(buff, &ninfo)
	if err != nil {
		ierr = _NewTmpFsError(emethod, "ErrJsonDecode", tool.WARN, err)
		ierr.StructDamage = true
		ierr.Path = filepath
		return &ninfo, nil, ierr
	}
	if ninfo.Remain > 0 {
		ninfo.Remain = ninfo.Remain - 1
	} else {
		ierr = _NewTmpFsError(emethod, "RemainExhaust", tool.INFO, errors.New(""))
		ierr.StructDamage = true
		ierr.Path = filepath
		return &ninfo, nil, ierr
	}

	if ninfo.Birth+ninfo.Expire < time.Now().Unix() {
		s := fmt.Sprintf("Node Expire at %d.", ninfo.Birth+ninfo.Expire)
		ierr = _NewTmpFsError(emethod, "NodeExpire", tool.INFO, errors.New(s))
		ierr.StructDamage = true
		return &ninfo, nil, ierr
	}

	if ninfo.Expire == 0 && AllowNodePermanent == false {
		ierr = _NewTmpFsError(emethod, "ExpireSetPermanent", tool.INFO, errors.New("Expire Set 0, but ExpireSetPermanent not set."))
		ierr.StructDamage = true
		return &ninfo, nil, ierr
	}

	buff, err = json.Marshal(ninfo)
	if err != nil {
		ierr = _NewTmpFsError(emethod, "ErrJsonEncode", tool.WARN, err)
		ierr.StructDamage = true
		ierr.Path = filepath
		return &ninfo, nil, ierr
	}
	r := bytes.NewReader(buff)

	return &ninfo, r, nil
}

func CreateMataJsonWhenLink(writerfunc func([]byte) (int, error), info *tool.NodeInfo, emethod string) *TmpFsError {
	var ierr *TmpFsError
	var wsize int64
	var wstruct tool.WriterStruct
	buff, err := json.Marshal(info)
	if err != nil {
		ierr = _NewTmpFsError(emethod, "ErrJsonEncode", tool.WARN, err)
		ierr.StructDamage = true
		return ierr
	}
	buffsize := len(buff)
	if buffsize >= MaxMetadataLen {
		ierr = _NewTmpFsError(emethod, "MetadataTooLarge", tool.WARN, errors.New(""))
		ierr.StructDamage = true
		return ierr
	}

	r := bytes.NewReader(buff)
	wstruct.WriterFunc = writerfunc
	wsize, err = io.Copy(wstruct, r)
	if err != nil {
		ierr = _NewTmpFsError(emethod, "UnknowWriteErr", tool.WARN, err)
		return ierr
	}
	if wsize != int64(buffsize) {
		ierr = _NewTmpFsError(emethod, "UnknowWriteErr", tool.WARN, errors.New("Do not write enough bytes."))
		return ierr
	}
	return nil
}
