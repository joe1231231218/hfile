package tmpfs

import (
	"context"
	"errors"
	"fmt"
	"hfile/tool"
	"math/rand"
	"os"
	"strconv"
	"sync"
	"time"

	"github.com/emirpasic/gods/lists/singlylinkedlist"
	"github.com/emirpasic/gods/maps/treemap"
	"github.com/emirpasic/gods/sets/treeset"
)

var ChangeLock sync.Mutex
var FolderRwLockSet tool.RwLockSet

var Prefix string
var UsedPrefix *treeset.Set = treeset.NewWithStringComparator()
var UnUsedsuffix *singlylinkedlist.List = singlylinkedlist.New()
var StaticFolderConvert *treemap.Map = treemap.NewWithStringComparator()

const (
	Undecided = iota
	Tempfile
	Storj
	S3
	GoogleStorage
	Sftp
	Ftp
)

// config
var Flty int = GoogleStorage
var PrefixLen, SuffixLen int = 2, 3
var MaxMetadataLen int = 1024
var AllowNodePermanent bool = false

const (
	//SYMBOLS characters used for short-urls
	//SYMBOLS = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
	//SYMBOLS = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	SYMBOLS = "23456789abdefghijrtABCDEFGHJKLMNOPQRSTUVWXYZ"
)

const (
	TmpFsErrorStrNotMatchCode int = 23
)

var TmpFsMethod = map[int]string{
	0:  "Undefine",
	1:  "TempFileRead",
	2:  "TempFileLink",
	3:  "TempFileUnlink",
	4:  "TempFileScan",
	5:  "SttRead",
	6:  "SttLink",
	7:  "Storj",
	8:  "Minio",
	9:  "GoogleStorage",
	10: "NewTmpFsError",
	11: "ExternalLibrary",
	12: "Sftp",
}

var TmpFsEtype = map[int]string{
	0:  "NotExistPath",
	1:  "LinkExistPath",
	2:  "ErrJsonDecode",
	3:  "ErrJsonEncode",
	4:  "Unknow",
	5:  "FltyNotImplemented",
	6:  "NotDir",
	7:  "PathNotAbsolute",
	8:  "ErrPathFormat",
	9:  "MethodNotImplemented",
	10: "BucketAccess",

	12:                        "RemainExhaust",
	13:                        "MetadataTooLarge",
	14:                        "UnknowReadErr",
	15:                        "UnknowWriteErr",
	16:                        "AccessAuthorizateErr",
	17:                        "TooManyRequests",
	18:                        "NodeExpire",
	19:                        "ExpireSetPermanent",
	20:                        "ContextCancel",
	21:                        "FolderRwLockErr",
	22:                        "TryReadLockFail",
	TmpFsErrorStrNotMatchCode: "TmpFsErrorStrNotMatch",
}

// if TryReadLockFail in Tmpfs::Read mean folder in link or delete , just consider as folder not exist

type TmpFsError struct {
	Msg          string
	Path         string
	Method       int
	Etype        int
	Folder       string
	StatCode     int16
	LogLevel     int16
	StructDamage bool
}

func (err TmpFsError) Error() string {
	//return fmt.Sprintf("(%+v)", err)
	return fmt.Sprintf("%s %s %s.\n", TmpFsMethod[err.Method], TmpFsEtype[err.Etype], err.Msg)
}

func NewTmpFsError(method int, etype int, loglevel int, msg string) TmpFsError {
	return TmpFsError{
		Msg:      msg,
		Path:     "",
		Method:   method,
		Etype:    etype,
		Folder:   "",
		StatCode: 500,
		LogLevel: int16(loglevel),
	}
}

func _NewTmpFsError(method string, etype string, loglevel int, err error) *TmpFsError {
	var terr *TmpFsError
	var methodok bool
	var methodindx, etypeindx int

	methodindx, methodok = _MapIntStrFind(method, TmpFsMethod)
	etypeindx = TmpFsErrorTypeFind(etype)
	if (!methodok) || (etypeindx == TmpFsErrorStrNotMatchCode) {
		var errstr string
		if !methodok {
			errstr = fmt.Sprintf("Method(%s) not find in TmpFsMethod Etype:%s", method, etype)
		} else {
			errstr = fmt.Sprintf("Method:%s Etype(%s) not find in TmpFsEtype", method, etype)
		}
		errstr = fmt.Sprintf("%s OriginalErr:%s", errstr, err.Error())
		terr = &TmpFsError{
			Msg:          errstr,
			Path:         "",
			Method:       9,
			Etype:        TmpFsErrorStrNotMatchCode,
			Folder:       "",
			StatCode:     500,
			LogLevel:     tool.ERROR,
			StructDamage: false,
		}
	} else {
		terr = &TmpFsError{
			Msg:          err.Error(),
			Path:         "",
			Method:       methodindx,
			Etype:        etypeindx,
			Folder:       "",
			StatCode:     500,
			LogLevel:     int16(loglevel),
			StructDamage: false,
		}
	}
	tool.EtLogf(2, terr.LogLevel, "%v", terr)
	return terr
}

type StaticFolder struct {
	AbsPath string
}

type TmpfsFd interface {
	Read([]byte) (int, error)
	Write([]byte) (int, error)
	Close() error
	//PickUpTheSlack() error
}

type TmpfsFdWrap struct {
	Fd TmpfsFd

	RLock  func()
	Closed bool
}

func NewTmpfsFdWrap(fd TmpfsFd, rlock func()) *TmpfsFdWrap {
	return &TmpfsFdWrap{
		Fd:     fd,
		RLock:  rlock,
		Closed: false,
	}
}

func (t *TmpfsFdWrap) Close() error {
	if !t.Closed {
		t.RLock()
		t.Closed = true
		return t.Fd.Close()
	}
	return nil
}

func (t *TmpfsFdWrap) Read(b []byte) (int, error) {
	return t.Fd.Read(b)
}

func (t *TmpfsFdWrap) Write(b []byte) (int, error) {
	return t.Fd.Write(b)
}

func InitTmpfs() {
	InitTmpfsSftp()
	InitTmpfsStorj()

	FolderRwLockSet = tool.NewLocalRwLockSet()
	for _, c := range SYMBOLS {
		_SymbolSet.Add(c)
	}
	// RmExpireRoutine()
}

func PermutationsSet(slt string, r int, wv func(string)) { //len(slt) and r shell large then 1
	var cp []byte
	var stopf bool = false
	var sltl int = len(slt)
	var stat []int = make([]int, r, r)
	/*var set *treeset.Set = treeset.NewWith(func (ac,bc interface{}) int {
		var a []string = ac.([]string)
		var b []string = bc.([]string)
		if len(a) != len(b){
			logger.Panicf("len(ac) != len(bc) can not compare.\n")
		}

		for i:=0;i<len(a);i=i+1{
			if(a>b){
				return 1
			}else if a<b{
				return -1
			}
		}
		return 0
	})*/
	for i := 0; i < r; i = i + 1 {
		stat[i] = 0
	}

	for !stopf {
		cp = make([]byte, r, r)
		for i, c := range stat {
			cp[i] = slt[c]
		}
		wv(string(cp))

		stat[r-1] = stat[r-1] + 1
		for p := r - 1; p > 0; p = p - 1 {
			if stat[p] < sltl {
				break
			}
			stat[p] = 0
			stat[p-1] = stat[p-1] + 1
		}
		if stat[0] >= sltl {
			stopf = true
		}
	}
	return
}

func LoadLfsrStatus() *TmpFsError {
	var fd *os.File
	var err error
	var buff [128]byte
	var prefix string
	var index64 []uint8
	var nextlfsr = func(start int) {
		var i int
		var tmp uint8
		for i = 0; i < len(index64); i += 1 {
			tmp = index64[(start+i)%len(index64)]
			tmp = LfsrFibonacci6Bit(tmp)
			if tmp == 0 {
				tmp = 1
				tool.Warn("tmpfs::LfsrFibonacci6Bit return 0.")
			}
			if tmp != 1 {
				index64[(start+i)%len(index64)] = tmp
				return
			}
		}
		panic(fmt.Sprintf("Lfsr(%v) status damage.", index64))
	}
	// Load status from file

	fd, err = os.OpenFile("lfsrstatus.txt", os.O_RDWR|os.O_CREATE, 664)
	defer fd.Close()
	_, err = fd.Read(buff[:])
	if err != nil {

	}
	strlst := tool.String2NodeInfoAcl(string(buff[:]))
	for _, uistr := range strlst {
		i, e := strconv.Atoi(uistr)
		if e != nil {

		}
		index64 = append(index64, uint8(i))
	}
	if len(index64) == 0 {
		for i := 0; i < PrefixLen; i += 1 {
			index64 = append(index64, 1)
		}
		tool.Info("tmpfs::LoadLfsrStatus generate init seed status.")
	}
RESTART:
	for {
		prefix = ""
		for indx, _ := range index64 {
			if indx < len(SYMBOLS) {
				prefix += string(SYMBOLS[indx])
			} else {
				nextlfsr(indx)
				continue RESTART
			}
		}
	}
	s := ""
	sep := ""
	for _, i := range index64 {
		s = sep + strconv.Itoa(int(i))
		sep = ","
	}
	nextlfsr(0)
	fd.Truncate(0)
	fd.WriteString(s)
	return nil
}

func UnUsedPrefixQuick() string {

	return ""
}

func UnUsedPrefixSlow(ctx context.Context) string {
	used, _ := Scan(ctx, false)
	unuse := singlylinkedlist.New()
	psls := func(v string) {
		_, ok := StaticFolderConvert.Get(v)
		if !ok && !used.Contains(v) {
			unuse.Append(v)
		}
	}
	PermutationsSet(SYMBOLS, PrefixLen, psls)
	prefix, _ := unuse.Get(rand.Intn(unuse.Size()))
	return prefix.(string)
}

func UnUsedFolder(ctx context.Context) string {
	adls := func(v string) {
		UnUsedsuffix.Append(v)
	}
	if UnUsedsuffix.Size() < 25 {
		Prefix = UnUsedPrefixSlow(ctx)
		ChangeLock.Lock()
		PermutationsSet(SYMBOLS, SuffixLen, adls)
		ChangeLock.Unlock()
	}
	r := rand.Intn(UnUsedsuffix.Size())
	ChangeLock.Lock()
	suff, _ := UnUsedsuffix.Get(r)
	UnUsedsuffix.Remove(r)
	ChangeLock.Unlock()
	return Prefix + suff.(string)
}

func Read(ctxc context.Context, ctx context.Context, folder string, dryrun bool) (TmpfsFd, *tool.NodeInfo, *TmpFsError) {
	var ok bool
	var fd TmpfsFd
	var fdw *TmpfsFdWrap
	var err error
	var inf *tool.NodeInfo
	var terr *TmpFsError
	if terr = FolderSymbolCheckRetTmpfsError(folder, "TempFileRead"); terr != nil {
		return fd, inf, terr
	}

	ok, err = FolderRwLockSet.TryRLock(ctx, folder)
	if err != nil {
		terr = _NewTmpFsError("TempFileRead", "FolderRwLockErr", tool.ERROR, err)
		return fd, inf, terr
	}
	if !ok {
		FolderRwLockSet.RUnlock(ctx, folder)
		terr = _NewTmpFsError("TempFileRead", "TryReadLockFail", tool.DEBUG, errors.New("Mean folder in link or delete, just consider as folder not exist"))
		terr.StatCode = 403
		return fd, inf, terr
	}

	switch Flty {
	case Tempfile:
		fd, inf, terr = _TempfileRead(ctxc, ctx, folder, dryrun)
	case Storj:
		fd, inf, terr = _TempStorjRead(ctxc, ctx, folder, dryrun)
	case S3:
		fd, inf, terr = _TempS3Read(ctxc, ctx, folder, dryrun)
	case GoogleStorage:
		fd, inf, terr = _TempGoogleRead(ctxc, ctx, folder, dryrun)
	default:
		terr = _NewTmpFsError("TempFileRead", "FltyNotImplemented", tool.ERROR, errors.New(""))
	}
	fdw = NewTmpfsFdWrap(fd, func() {
		FolderRwLockSet.RUnlock(ctxc, folder)
	})
	if dryrun {
		fdw.Close()
	}
	return fdw, inf, terr
}
func Link(ctxc context.Context, ctx context.Context, inf *tool.NodeInfo) (TmpfsFd, string, *TmpFsError) {
	var fd TmpfsFd
	var fdw *TmpfsFdWrap
	var err error
	var terr *TmpFsError
	var folder string = UnUsedFolder(ctx)

	err = FolderRwLockSet.WLockBlock(ctx, folder)
	if err != nil {
		terr = _NewTmpFsError("TempFileLink", "FolderRwLockErr", tool.ERROR, err)
		return fd, folder, terr
	}
	inf.Folder = folder
	switch Flty {
	case Tempfile:
		fd, terr = _TempfileLink(ctxc, ctx, inf, folder)
	case Storj:
		fd, terr = _TempStorjLink(ctxc, ctx, inf, folder)
	case S3:
		fd, terr = _TempS3Link(ctxc, ctx, inf, folder)
	case GoogleStorage:
		fd, terr = _TempGoogleLink(ctxc, ctx, inf, folder)
	case Sftp:
		fd, terr = _TempSftpLink(ctxc, ctx, inf, folder)
	default:
		terr = _NewTmpFsError("TempFileLink", "FltyNotImplemented", tool.ERROR, errors.New(""))
	}
	fdw = NewTmpfsFdWrap(fd, func() {
		FolderRwLockSet.WUnlock(ctxc, folder)
	})

	return fdw, folder, terr
}

func UnLink(ctx context.Context, folder string) *TmpFsError {
	var err error
	var terr *TmpFsError

	if terr = FolderSymbolCheckRetTmpfsError(folder, "TempFileUnlink"); terr != nil {
		return terr
	}
	err = FolderRwLockSet.WLockBlock(ctx, folder)
	defer FolderRwLockSet.WUnlock(ctx, folder)
	if err != nil {
		terr = _NewTmpFsError("TempFileUnlink", "FolderRwLockErr", tool.ERROR, err)
		return terr
	}

	switch Flty {
	case Tempfile:
		terr = _TempfileUnlink(folder)
	case Storj:
		terr = _TempStorjUnlink(ctx, folder)
	case S3:
		terr = _TempS3Unlink(ctx, folder)
	case GoogleStorage:
		terr = _TempGoogleUnlink(ctx, folder)
	default:
		terr = _NewTmpFsError("TempFileUnlink", "FltyNotImplemented", tool.ERROR, errors.New(""))
	}

	return terr
}

// If err, used well return zero item set.
func Scan(ctx context.Context, rmExpire bool) (*treeset.Set, *TmpFsError) {
	var terr *TmpFsError
	var used *treeset.Set
	switch Flty {
	case Tempfile:
		used, terr = _TempfileScan(ctx, rmExpire)
	case Storj:
		used, terr = _TempStorjScan(ctx, rmExpire)
	case S3:
		used, terr = _TempS3Scan(ctx, rmExpire)
	case GoogleStorage:
		used, terr = _TempGoogleScan(ctx, rmExpire)
	case Sftp:
		used, terr = _TempGoogleScan(ctx, rmExpire)
	default:
		terr = _NewTmpFsError("TempFileScan", "FltyNotImplemented", tool.ERROR, errors.New(""))
	}
	return used, terr
}

func RmExpireRoutine(ctxb context.Context) {
	var ierr *TmpFsError
	for {
		ctx, _ := context.WithTimeout(ctxb, time.Minute*20)
		_, ierr = Scan(ctx, true)
		if ierr != nil {

		}
		select {
		case <-ctxb.Done():
			tool.Info("RmExpireRoutine Goroutine Canceled.")
			return
		case <-ctx.Done():
		}
	}
}
