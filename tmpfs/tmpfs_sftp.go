package tmpfs

import (
	"context"
	"fmt"
	"hfile/tool"
	"os"
	"time"

	"github.com/emirpasic/gods/sets/treeset"
	"github.com/melbahja/goph"
	"github.com/pkg/sftp"
	"golang.org/x/crypto/ssh"
)

var SftpPool *SftpPoolStruct
var GophConfig *goph.Config

// config
var SftpUser string
var SftpAddr string
var SftpPort uint
var SftpTimeout uint = 12
var SftpPassword string
var SftpStoreDirectory string

type SftpPoolStruct struct {
}

func NewSftpPoolStruct() *SftpPoolStruct {
	var p SftpPoolStruct
	p = SftpPoolStruct{}
	return &p
}

func (p *SftpPoolStruct) Conn() (*sftp.Client, func()) {
	var err error
	var pwd []byte
	var sftpc *sftp.Client
	var client *goph.Client
	client, err = goph.NewConn(GophConfig)
	pwd, err = client.Run("pwd")
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(string(pwd))
	sftpc, err = client.NewSftp(sftp.UseConcurrentReads(false), sftp.UseConcurrentWrites(false))
	var disConn = func() {
		sftpc.Close()
		client.Close()
	}
	return sftpc, disConn
}

func InitTmpfsSftp() {
	if len(SftpStoreDirectory) == 0 || SftpStoreDirectory[0:1] != "/" {
		s := fmt.Sprintf("SftpStoreDirectory(%s) MUST be Absolute path", SftpStoreDirectory)
		tool.Error(s)
		panic(s)
	}
	GophConfig = &goph.Config{
		User:     SftpUser,
		Addr:     SftpAddr,
		Port:     SftpPort,
		Auth:     goph.Password(SftpPassword),
		Timeout:  time.Duration(SftpTimeout) * time.Second,
		Callback: ssh.InsecureIgnoreHostKey(),
	}
	SftpPool = NewSftpPoolStruct()
}

func _TempSftpRead(ctxc context.Context, ctx context.Context, folder string, dryrun bool) (*os.File, *tool.NodeInfo, *TmpFsError) {
	var fd *os.File
	var ninfo *tool.NodeInfo

	return fd, ninfo, nil
}

func _TempSftpLink(ctxc context.Context, ctx context.Context, inf *tool.NodeInfo, folder string) (*sftp.File, *TmpFsError) {
	var fd *sftp.File
	var err error
	var ierr *TmpFsError
	var sftpc *sftp.Client
	var disconn func()
	var folderp string
	var metapath, nodepath string
	sftpc, disconn = SftpPool.Conn()
	folderp = sftpc.Join(SftpStoreDirectory, folder)
	err = sftpc.Mkdir(folderp)
	if err != nil {
		fmt.Println(err)
	}
	metapath = sftpc.Join(folderp, "metadata")
	fd, err = sftpc.Create(metapath)
	if err != nil {
		fmt.Println(err)
	}
	ierr = CreateMataJsonWhenLink(fd.Write, inf, "Sftp")
	if ierr != nil {
		fmt.Println(ierr)
	}
	fd.Close()
	nodepath = sftpc.Join(folderp, "node")
	fd, err = sftpc.Create(nodepath)
	if err != nil {
		fmt.Println(err)
	}
	_ = disconn
	return fd, nil
}

func _TempSftpUnlink(folder string) *TmpFsError {
	return nil

}

func _TempSftpScan(ctx context.Context, rmExpire bool) (*treeset.Set, *TmpFsError) {
	used := treeset.NewWithStringComparator()

	return used, nil
}
