package tmpfs

import (
	"context"
	"errors"
	"fmt"
	"hfile/tool"
	"io"
	"time"

	"github.com/emirpasic/gods/sets/treeset"
	"storj.io/uplink"
)

const (
	eTypeNotExistPath    uint = 0x0001
	eTypeErrPathFormat   uint = 0x0002
	eTypeTooManyRequests uint = 0x0004
)

var StorjBucketName string
var StorjAccessGrant string
var StorjRootPassphrase string

var StorjAccess *uplink.Access

type StorjIoWrite struct {
	Upload  *uplink.Upload
	Project *uplink.Project
}

func (siw *StorjIoWrite) Read(p []byte) (int, error) {
	return 0, _NewTmpFsError("Storj", "MethodNotImplemented", tool.ERROR, errors.New("StorjIoWriteNotImplementedReadMethod"))
}

func (siw *StorjIoWrite) Write(p []byte) (int, error) {
	n, err := siw.Upload.Write(p)
	if err != nil {
		if errors.Is(err, io.EOF) {
			tool.Error("StorjIoRead::Read raise EOF Error.")
		}
		return 0, _NewTmpFsError("Storj", "UnknowWriteErr", tool.WARN, err)
	}
	return n, err
}

func (sfl *StorjIoWrite) Close() error {
	sfl.Upload.Commit()
	sfl.Project.Close()
	return nil
}

func NewStorjIoWrite(upload *uplink.Upload, project *uplink.Project) *StorjIoWrite {
	return &StorjIoWrite{
		Upload:  upload,
		Project: project,
	}
}

type StorjIoRead struct {
	Download *uplink.Download
	Project  *uplink.Project
}

func (sir *StorjIoRead) Read(p []byte) (int, error) {
	n, err := sir.Download.Read(p)
	if err != nil {
		if errors.Is(err, io.EOF) {
			tool.Error("StorjIoRead::Read raise EOF Error.")
			return n, err
		}
		return 0, _NewTmpFsError("Storj", "UnknowReadErr", tool.WARN, err)
	}
	return n, nil
}

func (sir *StorjIoRead) Write(p []byte) (int, error) {
	return 0, _NewTmpFsError("Storj", "MethodNotImplemented", tool.ERROR, errors.New("StorjIoReradNotImplementedWriteMethod"))
}

func (sir *StorjIoRead) Close() error {
	sir.Download.Close()
	sir.Project.Close()
	return nil
}

func NewStorjIoRead(download *uplink.Download, project *uplink.Project) *StorjIoRead {
	return &StorjIoRead{
		Download: download,
		Project:  project,
	}
}

func InitTmpfsStorj() {
	StorjAccess = nil
}

func StorjInternalErrorTransfer(err error, checkflag uint) string {
	var etype string
	if err != nil {
		etype = "Unknow"
		if errors.Is(err, uplink.ErrObjectNotFound) && (checkflag&eTypeNotExistPath) != 0 {
			etype = "NotExistPath"
		} else if errors.Is(err, uplink.ErrObjectKeyInvalid) && (checkflag&eTypeErrPathFormat) != 0 {
			etype = "ErrPathFormat"
		} else if errors.Is(err, uplink.ErrTooManyRequests) || errors.Is(err, uplink.ErrBandwidthLimitExceeded) && (checkflag&eTypeTooManyRequests) != 0 {
			etype = "TooManyRequests"
		}
	}
	return etype
}

func StorjInternalObjExist(ctx context.Context, key string, project *uplink.Project) (bool, *TmpFsError) {
	var err error
	var terr *TmpFsError
	var etype string
	//var obinfo *uplink.Object
	_, err = project.StatObject(ctx, StorjBucketName, key)
	if err != nil {
		if errors.Is(err, uplink.ErrObjectNotFound) {
			return false, nil
		} else {
			etype = StorjInternalErrorTransfer(err, eTypeErrPathFormat|eTypeTooManyRequests)
			terr = _NewTmpFsError("Storj", etype, tool.INFO, err)
			terr.Path = key
			return false, terr
		}
	}
	return true, nil
}

func StorjInternalUpload(ctx context.Context, key string, info *tool.NodeInfo, project *uplink.Project, isexist bool) (*uplink.Upload, *TmpFsError) {
	var err error
	var terr *TmpFsError
	var load *uplink.Upload
	var etype string
	var uploadoption uplink.UploadOptions
	found, ecterr := StorjInternalObjExist(ctx, key, project)
	if ecterr != nil {
		return nil, ecterr
	}
	terr = nil
	if isexist && found == false {
		terr = _NewTmpFsError("Storj", "NotExistPath", tool.WARN, errors.New(""))
	} else if isexist == false && found {
		terr = _NewTmpFsError("Storj", "LinkExistPath", tool.WARN, errors.New(""))
	}
	if terr != nil {
		terr.Path = key
		return nil, terr
	}

	if info.Expire == 0 {
		if AllowNodePermanent == false {
			return nil, _NewTmpFsError("storj", "ExpireSetPermanent", tool.INFO, errors.New("Expire Set 0, but ExpireSetPermanent not set."))
		}
		uploadoption.Expires = time.Unix(0, 0)
	} else {
		uploadoption.Expires = time.Unix(info.Birth+info.Expire, 0)
	}
	load, err = project.UploadObject(ctx, StorjBucketName, key, &uploadoption)
	if err != nil {
		etype = StorjInternalErrorTransfer(err, eTypeErrPathFormat|eTypeTooManyRequests)
		return nil, _NewTmpFsError("Storj", etype, tool.WARN, err)
	}
	return load, nil
}

func StorjProject(ctx context.Context) (*uplink.Project, *TmpFsError) {
	var err error
	var terr *TmpFsError
	var project *uplink.Project

	var parseaccess = func() *TmpFsError {
		var err error
		// Parse the Access Grant.
		StorjAccess, err = uplink.ParseAccess(StorjAccessGrant)
		if err != nil {
			StorjAccess = nil
			se := fmt.Sprintf("Could not parse access grant: %+v", err)
			return _NewTmpFsError("Storj", "AccessAuthorizateErr", tool.INFO, errors.New(se))
		}
		return nil
	}
	if StorjAccess == nil {
		terr = parseaccess()
		if terr != nil {
			return nil, terr
		}
	}
	// Open up the Project we will be working with.
	project, err = uplink.OpenProject(ctx, StorjAccess)
	if err != nil {
		terr = parseaccess()
		if terr != nil {
			return nil, terr
		}
		project, err = uplink.OpenProject(ctx, StorjAccess)
		if err != nil {
			se := fmt.Sprintf("Could not open project: %+v", err)
			return nil, _NewTmpFsError("Storj", "AccessAuthorizateErr", tool.ERROR, errors.New(se))
		}
	}

	// Ensure the desired Bucket within the Project is created.
	_, err = project.EnsureBucket(ctx, StorjBucketName)
	if err != nil {
		se := fmt.Sprintf("Could not ensure bucket: %+v", err)
		return nil, _NewTmpFsError("Storj", "BucketAccess", tool.ERROR, errors.New(se))
	}
	//fmt.Print("TempStorjBefore open bucket")
	return project, nil
}

func _TempStorjRead(ctxc context.Context, ctx context.Context, folder string, dryrun bool) (*StorjIoRead, *tool.NodeInfo, *TmpFsError) {
	var fd *StorjIoRead
	var ninfo *tool.NodeInfo
	var rinfo io.Reader
	var upload *uplink.Upload
	var download *uplink.Download
	metakey, nodekey := StorjS3FolderChild(folder)

	project, terr := StorjProject(ctx)
	if terr != nil {
		return fd, ninfo, terr
	}
	var downloadfunc = func(ctx context.Context, key string) (*uplink.Download, *TmpFsError) {
		var etype string
		download, err := project.DownloadObject(ctx, StorjBucketName, key, nil)
		if err != nil {
			etype = StorjInternalErrorTransfer(err, eTypeNotExistPath|eTypeErrPathFormat|eTypeTooManyRequests)
			terr = _NewTmpFsError("Storj", etype, tool.INFO, err)
			terr.Path = key
			terr.Folder = folder
			return nil, terr
		}
		i := download.Info()
		_ = i.System.ContentLength
		return download, nil
	}

	download, terr = downloadfunc(ctx, metakey)
	if terr != nil {
		return fd, ninfo, terr
	}
	ninfo, rinfo, terr = UpdateMataJsonWhenRead(download.Read, metakey, "Storj")
	download.Close()
	if terr != nil {
		return fd, ninfo, terr
	}
	if !dryrun {
		upload, terr = StorjInternalUpload(ctx, metakey, ninfo, project, true)
		_, err := io.Copy(upload, rinfo)
		if err != nil {
			upload.Abort()
			terr = _NewTmpFsError("Storj", "UnknowWriteErr", tool.WARN, err)
			terr.Path = metakey
			return fd, ninfo, terr
		}
		upload.Commit()
	}

	download, terr = downloadfunc(ctxc, nodekey)
	if terr != nil {
		return fd, ninfo, terr
	}
	ninfo.Size = download.Info().System.ContentLength
	fd = NewStorjIoRead(download, project)

	return fd, ninfo, nil
}

func _TempStorjLink(ctxc context.Context, ctx context.Context, inf *tool.NodeInfo, folder string) (*StorjIoWrite, *TmpFsError) {
	var upload *uplink.Upload
	metakey, nodekey := StorjS3FolderChild(folder)
	project, ierr := StorjProject(ctx)
	if ierr != nil {
		tool.Info("_TempStorjLink")
		return nil, ierr
	}

	upload, ierr = StorjInternalUpload(ctx, metakey, inf, project, false)
	if ierr != nil {
		return nil, ierr
	}
	ierr = CreateMataJsonWhenLink(upload.Write, inf, "Storj")
	if ierr != nil {
		upload.Abort()
		return nil, ierr
	}
	upload.Commit()

	upload, ierr = StorjInternalUpload(ctxc, nodekey, inf, project, false)
	if ierr != nil {
		return nil, ierr
	}
	nodefile := NewStorjIoWrite(upload, project)
	return nodefile, nil
}

func _TempStorjUnlink(ctx context.Context, folder string) *TmpFsError {
	metakey, nodekey := StorjS3FolderChild(folder)
	project, ierr := StorjProject(ctx)
	defer project.Close()
	if ierr != nil {
		return ierr
	}
	delfunc := func(keyname string) error {
		_, err := project.DeleteObject(ctx, StorjBucketName, keyname)

		if err != nil {
			tool.Info("Delete %s key %+v.", keyname, err)
			StorjInternalErrorTransfer(err, eTypeNotExistPath|eTypeErrPathFormat|eTypeTooManyRequests)
			// If need raice TmpfsError
			return err
		}
		return nil
	}
	_ = delfunc(metakey)
	_ = delfunc(nodekey)
	_ = delfunc(folder)
	return nil
}

func _TempStorjScan(ctx context.Context, rmExpire bool) (*treeset.Set, *TmpFsError) {
	var used *treeset.Set = treeset.NewWithStringComparator()
	project, ierr := StorjProject(ctx)
	defer project.Close()
	if ierr != nil {
		return nil, ierr
	}

	objiter := project.ListObjects(ctx, StorjBucketName, nil)
	for objiter.Next() {
		if err := objiter.Err(); err != nil {

			etype := StorjInternalErrorTransfer(err, eTypeNotExistPath|eTypeErrPathFormat|eTypeTooManyRequests)
			return nil, _NewTmpFsError("Storj", etype, tool.INFO, err)
		}

		ok, k := StorjS3Key2UsedPrefix(objiter.Item().Key)
		if ok {
			used.Add(k)
		}
	}
	return used, nil
}
