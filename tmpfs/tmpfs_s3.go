package tmpfs

import (
	"bytes"
	"context"
	"encoding/xml"
	"errors"
	"fmt"
	"hfile/tool"
	"io"
	"io/ioutil"
	"os"
	"strconv"

	"github.com/emirpasic/gods/sets/treeset"
	"github.com/minio/minio-go/v7"
	"github.com/minio/minio-go/v7/pkg/credentials"
)

const S3EndPoint string = "s3.amazonaws.com"

var S3Region string
var S3BucketName string
var S3SccessKey string
var S3SecretAccessKey string

var S3AutoMakeBucket bool = false

// Ctx is for W, W will use the coroutine,
// if Ctx is canceled, it will cause W to fail or block
type S3Io struct {
	R        *minio.Object
	W        *io.PipeWriter
	S3Client *minio.Client
	Ctx      context.Context
	Cancel   context.CancelFunc
}

func (s3i *S3Io) Read(p []byte) (int, error) {
	if s3i.R == nil {
		return 0, _NewTmpFsError("Minio", "MethodNotImplemented", tool.ERROR, errors.New(""))
	}
	return s3i.R.Read(p)
}

func (s3i *S3Io) Write(p []byte) (int, error) {
	if s3i.W == nil {
		return 0, _NewTmpFsError("Minio", "MethodNotImplemented", tool.ERROR, errors.New(""))
	}
	select {
	case <-s3i.Ctx.Done():
		tool.Info("S3Io::Write ContextCancel.")
		s3i.Close()
		return 0, _NewTmpFsError("Minio", "ContextCancel", tool.WARN, errors.New(""))
	default:
	}
	return s3i.W.Write(p)
}

func (s3i *S3Io) Close() error {
	if s3i.R != nil {
		s3i.R.Close()
		s3i.R = nil
	}
	if s3i.W != nil {
		s3i.W.Close()
		s3i.W = nil
	}
	return nil
}

func (s3i *S3Io) PickUpTheSlack() error {
	s3i.Close()
	s3i.Cancel() // Can lead context canceled
	return nil
}

func NewS3IoRerader(reader *minio.Object, s3client *minio.Client) *S3Io {
	return &S3Io{
		R:        reader,
		S3Client: s3client,
	}
}

func S3InternalGetObject(ctx context.Context, s3client *minio.Client, key string) (*minio.Object, int64, *TmpFsError) {
	var ob *minio.Object
	var err error
	var size int64
	var terr *TmpFsError
	var etype string
	var obinfo minio.ObjectInfo

	ob, err = s3client.GetObject(ctx, S3BucketName, key, minio.GetObjectOptions{})
	if err != nil {
		terr = _NewTmpFsError("Minio", "Unknow", tool.INFO, err)
		terr.Path = key
		return nil, 0, terr
	}

	//Need to read object to get error err.Error() The specified key does not exist.
	obinfo, err = ob.Stat()
	if err != nil {
		etype = "UnknowReadErr"
		if err.Error() == "The specified key does not exist." {
			etype = "NotExistPath"
		}
		terr = _NewTmpFsError("Minio", etype, tool.INFO, err)
		terr.Path = key
		return nil, 0, terr
	}
	size = obinfo.Size
	//fmt.Printf("Obinfo:%+v", obinfo)
	return ob, size, nil
}

func S3InternalObjectExist(ctx context.Context, s3client *minio.Client, key string) (bool, *TmpFsError) {
	_, err := s3client.StatObject(ctx, S3BucketName, key, minio.GetObjectOptions{})
	if err != nil {
		// Unstable way
		if err.Error() == "The specified key does not exist." {
			return false, nil
		}

		return false, _NewTmpFsError("Minio", "Unknow", tool.WARN, err)
	}
	return true, nil
}

func __S3InternalPutObject(ctx context.Context, s3client *minio.Client, key string, inf *tool.NodeInfo, iscreate bool) (*minio.PutObjectOptions, *TmpFsError) {
	const daysec int64 = 60 * 60 * 24
	var usertag map[string]string
	var pobo minio.PutObjectOptions
	var ierrwithpath = func(etype string) *TmpFsError {
		ierr := _NewTmpFsError("Minio", etype, tool.WARN, errors.New(""))
		ierr.Path = key
		return ierr
	}

	found, ierr := S3InternalObjectExist(ctx, s3client, key)
	if ierr != nil {
		return nil, ierr
	}
	if iscreate == true && found == true {
		return nil, ierrwithpath("LinkExistPath")
	} else if iscreate == false && found == false {
		return nil, ierrwithpath("NotExistPath")
	}

	if inf.Expire == 0 {
		if AllowNodePermanent == false {
			return nil, _NewTmpFsError("Minio", "ExpireSetPermanent", tool.INFO, errors.New("Expire Set 0, but ExpireSetPermanent not set."))
		}
		pobo = minio.PutObjectOptions{}
	} else {
		epr := (inf.Expire / daysec)
		if (inf.Expire % daysec) != 0 {
			epr = epr + 1
		}
		usertag = map[string]string{
			"expire": strconv.FormatInt(epr, 10),
		}
		pobo = minio.PutObjectOptions{UserTags: usertag}
	}
	// Todo: Config LifeCycle

	return &pobo, nil
}

func S3InternalDirectPutObject(ctx context.Context, buff []byte, s3client *minio.Client, key string, inf *tool.NodeInfo, iscreate bool) *TmpFsError {
	var err error
	var ierr *TmpFsError
	var pobo *minio.PutObjectOptions
	var upinfo minio.UploadInfo
	var buffsize int64

	pobo, ierr = __S3InternalPutObject(ctx, s3client, key, inf, iscreate)
	if ierr != nil {
		return ierr
	}

	buffsize = int64(len(buff))
	upinfo, err = s3client.PutObject(ctx, S3BucketName, key, bytes.NewReader(buff), buffsize, *pobo)
	if err != nil {
		return _NewTmpFsError("Minio", "Unknow", tool.WARN, err)
	}
	if upinfo.Size != buffsize {
		return _NewTmpFsError("Minio", "UnknowWriteErr", tool.WARN, errors.New("Do not write enough bytes."))
	}
	return nil
}

func S3InternalPutObject(connctx context.Context, ctx context.Context, s3client *minio.Client, key string, inf *tool.NodeInfo, iscreate bool) (*S3Io, *TmpFsError) {
	var s3i S3Io
	var ierr *TmpFsError
	var pobo *minio.PutObjectOptions

	pobo, ierr = __S3InternalPutObject(ctx, s3client, key, inf, iscreate)
	if ierr != nil {
		return nil, ierr
	}

	r, w := io.Pipe()
	ctxlc, cancellc := context.WithCancel(context.Background())
	var linkfunc = func(ictx context.Context) {
		defer cancellc()
		upinfo, err := s3client.PutObject(ictx, S3BucketName, key, r, -1, *pobo)
		if err != nil {
			w.CloseWithError(err)
			tool.Error("Upload %s fail %+v.", key, err)
			return
		}
		tool.Debug("Upload %d bytes to %s.", upinfo.Size, key)
		return
	}
	go linkfunc(ctxlc)
	s3i = S3Io{
		W:        w,
		Ctx:      ctxlc,
		Cancel:   cancellc,
		S3Client: s3client,
	}
	return &s3i, nil
}

func S3Project(ctx context.Context) (*minio.Client, *TmpFsError) {
	var err error
	var terr *TmpFsError
	var found bool
	var s3client *minio.Client

	s3client, err = minio.New(S3EndPoint, &minio.Options{
		Creds:  credentials.NewStaticV4(S3SccessKey, S3SecretAccessKey, ""),
		Secure: true,
	})
	if err != nil {
		s := fmt.Sprintf("Can not Minio New %+v.", err)
		terr = _NewTmpFsError("Minio", "AccessAuthorizateErr", tool.INFO, errors.New(s))
		return nil, terr
	}
	found, err = s3client.BucketExists(ctx, S3BucketName)
	if found == false && S3AutoMakeBucket == false && err.Error() == "Access Denied." {
		terr = _NewTmpFsError("Minio", "BucketAccess", tool.INFO, errors.New("S3 Bucket Name not exist(Access Denied.)."))
		return nil, terr
	}
	if err != nil && err.Error() != "Access Denied." {
		terr = _NewTmpFsError("Minio", "BucketAccess", tool.INFO, err)
		return nil, terr
	}
	if S3AutoMakeBucket && err.Error() == "Access Denied." {
		tool.Info("Minio create %s Bucket.", S3BucketName)
	} else {
		tool.Debug("Minio Exists %s Bucket.", S3BucketName)
	}

	err = s3client.MakeBucket(ctx, S3BucketName, minio.MakeBucketOptions{Region: S3Region, ObjectLocking: false})
	if err != nil {
		terr = _NewTmpFsError("Minio", "BucketAccess", tool.INFO, err)
		return nil, terr
	}

	return s3client, nil
}

func TempS3GetLifeCycle(ctx context.Context) {
	var err error

	s3client, _ := S3Project(ctx)
	// Get bucket lifecycle from S3
	lifecycle, err := s3client.GetBucketLifecycle(ctx, S3BucketName)
	if err != nil {
		fmt.Println(err)
	}
	// Save the lifecycle document to a file
	localLifecycleFile, err := os.Create("lifecycle.xml")
	if err != nil {
		fmt.Println(err)
	}
	defer localLifecycleFile.Close()

	enc := xml.NewEncoder(localLifecycleFile)
	enc.Indent("  ", "    ")
	if err := enc.Encode(lifecycle); err != nil {
		fmt.Println(err)
	}
}

func _TempS3Read(ctxc context.Context, ctx context.Context, folder string, dryrun bool) (*S3Io, *tool.NodeInfo, *TmpFsError) {
	var ob *minio.Object
	var rinfo io.Reader
	var ninfo *tool.NodeInfo
	metakey, nodekey := StorjS3FolderChild(folder)

	s3client, terr := S3Project(ctxc)
	if terr != nil {
		return nil, ninfo, terr
	}

	// Update metadata
	ob, _, terr = S3InternalGetObject(ctx, s3client, metakey)
	if terr != nil {
		return nil, ninfo, terr
	}
	ninfo, rinfo, terr = UpdateMataJsonWhenRead(ob.Read, metakey, "Minio")
	if terr != nil {
		return nil, ninfo, terr
	}

	if !dryrun {
		buff, err := ioutil.ReadAll(rinfo)
		if err != nil {
			return nil, ninfo, _NewTmpFsError("Minio", "UnknowReadErr", tool.WARN, err)
		}
		terr = S3InternalDirectPutObject(ctx, buff, s3client, metakey, ninfo, false)
		if terr != nil {
			return nil, ninfo, terr
		}
	}

	ob, ninfo.Size, terr = S3InternalGetObject(ctxc, s3client, nodekey)
	if terr != nil {
		return nil, ninfo, terr
	}
	return NewS3IoRerader(ob, s3client), ninfo, nil
}

func _TempS3Link(ctxc context.Context, ctx context.Context, inf *tool.NodeInfo, folder string) (*S3Io, *TmpFsError) {
	var uprlt, wtrob *S3Io

	metakey, nodekey := StorjS3FolderChild(folder)
	s3client, ierr := S3Project(ctxc)
	if ierr != nil {
		return nil, ierr
	}

	wtrob, ierr = S3InternalPutObject(ctx, ctx, s3client, metakey, inf, true)
	if ierr != nil {
		return nil, ierr
	}
	defer wtrob.Close()
	ierr = CreateMataJsonWhenLink(wtrob.Write, inf, "Minio")
	if ierr != nil {
		return nil, ierr
	}

	uprlt, ierr = S3InternalPutObject(ctxc, ctx, s3client, nodekey, inf, true)
	if ierr != nil {
		return nil, ierr
	}
	return uprlt, nil
}

func _TempS3Unlink(ctx context.Context, folder string) *TmpFsError {
	mata, node := StorjS3FolderChild(folder)
	s3client, ierr := S3Project(ctx)
	if ierr != nil {
		return ierr
	}
	delfunc := func(delkey string) {
		err := s3client.RemoveObject(ctx, S3BucketName, delkey, minio.RemoveObjectOptions{})
		if err != nil {
			tool.Info("Delete %s key %+v.", delkey, err)
		}
		return
	}
	delfunc(mata)
	delfunc(node)
	return nil

}

func _TempS3Scan(ctx context.Context, rmExpire bool) (*treeset.Set, *TmpFsError) {
	var used *treeset.Set = treeset.NewWithStringComparator()
	s3client, ierr := S3Project(ctx)
	if ierr != nil {
		return used, ierr
	}
	obch := s3client.ListObjects(ctx, S3BucketName, minio.ListObjectsOptions{
		Recursive: true,
	})
	for ob := range obch {
		if ob.Err != nil {
			return used, _NewTmpFsError("Minio", "Unknow", tool.INFO, ob.Err)
		}
		ok, k := StorjS3Key2UsedPrefix(ob.Key)
		if ok {
			used.Add(k)
		}
	}
	return used, nil
}
