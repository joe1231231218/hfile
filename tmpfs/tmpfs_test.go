package tmpfs

import (
	"context"
	"hfile/tool"

	"testing"
)

/*
	"errors"
	"fmt"

	"io"
	"io/ioutil"
	"path/filepath"
	"strings"
	"time"

	"github.com/emirpasic/gods/sets/treeset"
godsutils "github.com/emirpasic/gods/utils"
*/

/*
// if you want to do test, please open this block and full config value.
func LoadTestConfig() {
	Flty = GoogleStorage
	PrefixLen, SuffixLen = 2, 3
	MaxMetadataLen = 1024
	AllowNodePermanent = false

	Dir_tmp = ""

	StorjBucketName = ""
	StorjAccessGrant = ""
	StorjRootPassphrase = ""

	S3Region = ""
	S3BucketName = ""
	S3SccessKey = ""
	S3SecretAccessKey = ""
	S3AutoMakeBucket = false

	GoogleStorageBucket = ""
	ServiceAccountCredentialJSON = ""
}
*/

func Test_Tempfile(t *testing.T) {
	var err *TmpFsError
	var fd TmpfsFd
	var ctx context.Context
	var ind *tool.NodeInfo = tool.NewNodeInfo(60*60*24*3, 3)
	var folder string

	LoadTestConfig()
	InitTmpfs()

	ctx, _ = context.WithCancel(context.Background())

	fd, folder, err = Link(ctx, ctx, ind)
	t.Log(folder, err)
	_ = fd
	fd.Write([]byte("Test_TempfileRead"))
	fd.Close()
	/*
		fd, ind, err = Read(ctx, ctx, folder, false)
		t.Log(ind, err)
		fd.Close()

		fd, ind, err = Read(ctx, ctx, folder, true)
		t.Log(ind, err)
		fd.Close()

		fd, ind, err = Read(ctx, ctx, folder, false)
		buff, _ := ioutil.ReadAll(fd)
		t.Log(ind, string(buff[:]), err)
		fd.Close()

		err = UnLink(ctx, folder)
		t.Log(err)

		used, err := Scan(ctx, false)
		t.Log(used, err)
	*/
}

/*
func Test_TempfileMulLink(t *testing.T) {
	var err *TmpFsError
	var fd TmpfsFd
	var fld string
	var ind *tool.NodeInfo
	var ctx context.Context
	var folder string
	var linkfolder []string

	ctx, _ = context.WithCancel(context.Background())

	for i := 0; i < 10; i = i + 1 {
		fd, _, err = Link(ctx, ctx, tool.NewNodeInfo(10000, 1))
		if err != nil {
			t.Log(err)
		}
		fmt.Fprintf(fd, "Test_TempfileMulLink%d", i)
		fd.Close()
	}
	used, err := Scan(ctx, false)
	t.Log(used, err)

	remaintime := 5
	wrtxstr := "Test_TempfileMulLink_wrtxstr"
	for i := 0; i < 3; i = i + 1 {
		fd, folder, err = Link(ctx, ctx, tool.NewNodeInfo(10000, remaintime))
		if err != nil {
			t.Log(err)
		}
		fmt.Fprintf(fd, "%s", wrtxstr)
		fd.Close()
		linkfolder = append(linkfolder, folder)
	}
	for _, fld = range linkfolder {
		for j := 0; j < remaintime; j = j + 1 {
			_, ind, err = Read(ctx, ctx, fld, true)
			if err != nil {
				t.Log(fld, err)
			}
			if ind.Size != int64(len([]byte(wrtxstr))) {
				t.Log("Expect Size fill in dryrun.")
			}

			fd, _, err = Read(ctx, ctx, fld, false)
			if err != nil {
				t.Log(fld, err)
			}
			buff, _ := ioutil.ReadAll(fd)
			if string(buff[:]) != wrtxstr {
				t.Log("The strings written and read do not match.")
			}
			fd.Close()
		}
		_, _, terr := Read(ctx, ctx, fld, false)
		if terr == nil {
			t.Log("Expect RemainExhaust return there.\n")
		}
		t.Log(terr)
	}

}

func Test_TempFileLarge(t *testing.T) {
	var ctx context.Context
	var testtempfulelarge bool = true
	const onemb uint64 = 1048576

	ctx, _ = context.WithCancel(context.Background())

	wrtnmbandcheck := func(n uint64, basesize uint64, sleep int) {
		var i uint64
		var rsz uint64
		var buff []byte = make([]byte, basesize)
		var basebuff = strings.Repeat("0", int(basesize))
		fd, folder, terr := Link(ctx, ctx, tool.NewNodeInfo(60*20, 3))
		if terr != nil {
			t.Logf("%#v", terr)
			return
		}
		fmt.Printf("Test_TempFileLarge Link:\n")
		for i = 0; i < n; i = i + 1 {
			fmt.Printf("-")
			if sleep > 0 {
				time.Sleep(time.Second * time.Duration(sleep))
			}
			_, err := io.Copy(fd, strings.NewReader(basebuff))
			if err != nil {
				t.Log(err)
			}
		}
		fd.Close()
		fmt.Printf("\n")

		time.Sleep(time.Second * 2)
		fmt.Printf("Test_TempFileLarge Read:\n")
		rsz = 0
		fd, _, terr = Read(ctx, ctx, folder, false)
		if terr != nil {
			t.Logf("%#v", terr)
			return
		}
		for {
			fmt.Printf("+")
			if sleep > 0 {
				time.Sleep(time.Second * time.Duration(sleep))
			}
			n, err := fd.Read(buff)
			if err != nil {
				if errors.Is(err, io.EOF) {
					break
				}
				t.Log(err)
				return
			}
			rsz = rsz + uint64(n)
		}
		fd.Close()
		fmt.Printf("\n")
		if rsz != n*basesize {
			t.Logf("UnExpect lenth, write %d bytes but read %d byte.", n*1048576, rsz)
		}
		return
	}

	if testtempfulelarge {
		wrtnmbandcheck(1, onemb, 0)
		wrtnmbandcheck(5, onemb, 0)
		wrtnmbandcheck(128, onemb, 0)
		wrtnmbandcheck(2048, onemb, 0)
	}
	wrtnmbandcheck(5, 12, 2)
	wrtnmbandcheck(40, 12, 2)
}

func Test_TempFileNotfound(t *testing.T) {
	var err error
	ctx, _ := context.WithCancel(context.Background())
	_, _, err = Read(ctx, ctx, "00000", false)
	if err == nil {
		t.Log("Expect Error return there.\n")
	}

}

func Test_PermutationsSet(t *testing.T) {
	var s *treeset.Set = treeset.NewWithStringComparator()
	addset := func(v string) {
		s.Add(v)
	}
	pmt := func(slt string, r int) {
		PermutationsSet(slt, r, addset)
		t.Log(s)
		s.Clear()
	}

	pmt("012", 3)
	pmt("012", 2)
	pmt("012", 1)
	pmt("1", 1)
	pmt("SEDFBU", 2)
}

func Test_StaticFolder(t *testing.T) {
	p, _ := filepath.Abs("../eric")
	sstf, err := StaticFolderConvertCheck(StaticFolder{AbsPath: p})
	if err != nil {
		t.Error(err)
	}
	StaticFolderConvert.Put("AH2TR", sstf)
	_, err = SttRead("AH2TR", "file")
	if err != nil {
		t.Error(err)
	}
	wf, err := SttLink("AH2TR", "test")
	if err != nil {
		t.Error(err)
	}
	wf.WriteString("test")
	wf.Close()
}
*/
/*
func Test_Lfsr(t *testing.T) {
	const st uint8 = 1
	var s *treeset.Set = treeset.NewWith(godsutils.UInt8Comparator)
	var curr uint8

	curr = LfsrFibonacci6Bit(st)
	for curr != st {
		curr = LfsrFibonacci6Bit(curr)
		if s.Contains(curr) {
			t.Errorf("LfsrFibonacci6Bit return seam value.")
			break
		}
		s.Add(curr)
	}
	t.Log(s)
}
*/
