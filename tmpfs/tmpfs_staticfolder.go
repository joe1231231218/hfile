package tmpfs

import (
	"errors"
	"fmt"
	"hfile/tool"
	"os"
	"path/filepath"
)

func StaticFolderConvertCheck(folder StaticFolder) (StaticFolder, error) {
	var errs string = ""
	fla := folder.AbsPath
	if !filepath.IsAbs(fla) {
		errs = fmt.Sprintf("StaticFolder[%s](%s) shell abs.\n", folder.AbsPath, fla)
		return folder, NewTmpFsError(0, 7, tool.WARN, errs)
	}
	info, err := os.Stat(fla)
	if err != nil {
		etype := 4
		if errors.Is(err, os.ErrNotExist) {
			etype = 0
			errs = fmt.Sprintf("StaticFolder[%s](%s) os.ErrNotExist.\n", folder.AbsPath, fla)
		} else {
			errs = err.Error()
		}
		return folder, NewTmpFsError(0, etype, tool.WARN, errs)
	}
	if !info.IsDir() {
		return folder, NewTmpFsError(0, 6, tool.WARN, "")
	}
	return folder, nil
}

func SubfoldercConcat(folder string, sub string) (string, error) {
	var emsg string
	var flagerr bool = false
	if filepath.IsAbs(sub) {
		flagerr, emsg = true, fmt.Sprintf("fd(%s) shell not abs.", sub)
	}
	sub, _ = filepath.Abs(filepath.Join(folder, sub))
	if folder != sub[0:len(folder)] {
		flagerr, emsg = true, fmt.Sprintf("fd(%s) shell not content '.' or '..' .", sub)
	}
	if flagerr {
		err := NewTmpFsError(0, 8, tool.INFO, emsg)
		err.Folder = folder
		return "", err
	}
	return sub, nil
}

func _ScgSfccSfct(folder string, fd string) (string, error) {
	_flp, _ok := StaticFolderConvert.Get(folder)
	if !_ok {
		ierr := NewTmpFsError(0, 0, tool.INFO, fmt.Sprintf("StaticFolder[%s] not exist.", folder))
		ierr.Folder = folder
		return "", ierr
	}
	flp, ok := _flp.(StaticFolder)
	if !ok {
		ierr := NewTmpFsError(0, 0, tool.WARN, fmt.Sprintf("StaticFolder[%s] not type of StaticFolder.", folder))
		ierr.Folder = folder
		return "", ierr
	}
	flp, err := StaticFolderConvertCheck(flp)
	if err != nil {
		return "", err
	}
	fla, err := SubfoldercConcat(flp.AbsPath, fd)
	if err != nil {
		return "", err
	}
	return fla, nil
}

func SttRead(folder string, fd string) (*os.File, error) {
	var sf *os.File
	sstpath, err := _ScgSfccSfct(folder, fd)
	if err != nil {
		ierr := err.(TmpFsError)
		ierr.Method = 5
		return sf, ierr
	}
	sf, err = os.OpenFile(sstpath, os.O_RDONLY, 664)
	if err != nil {
		etype := 4
		if errors.Is(err, os.ErrNotExist) {
			etype = 0
		}
		ierr := NewTmpFsError(5, etype, tool.INFO, err.Error())
		ierr.Path = sstpath
		return sf, ierr
	}
	return sf, nil
}

func SttLink(folder string, fd string) (*os.File, error) {
	var wf *os.File
	sstpath, err := _ScgSfccSfct(folder, fd)
	if err != nil {
		ierr := err.(TmpFsError)
		ierr.Method = 6
		return wf, ierr
	}
	wf, err = os.OpenFile(sstpath, os.O_WRONLY|os.O_CREATE, 664)
	if err != nil {
		ierr := NewTmpFsError(6, 0, tool.INFO, err.Error())
		ierr.Path, ierr.Folder = sstpath, folder
		return wf, ierr
	}
	return wf, nil
}
