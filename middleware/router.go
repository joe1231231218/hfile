package middleware

import (
	"archive/zip"
	"bufio"
	"bytes"
	"context"
	"errors"
	"fmt"
	"hfile/ratelimiter"
	"hfile/tmpfs"
	"hfile/tool"
	"io"
	"mime/multipart"
	"net"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"time"

	"github.com/gorilla/mux"
	qrcode "github.com/skip2/go-qrcode"
)

const OneMbByte int64 = (1 << 20)

const (
	EstimateTimeFs        uint = 0x01
	EstimateTimeAuth      uint = 0x02
	EstimateTimeFsOp      uint = 0x04
	EstimateTimeGeneral   uint = 0x08
	EstimateTimeRateLimit uint = 0x10
)

var EstimateTimeMap map[uint]time.Duration = map[uint]time.Duration{
	EstimateTimeFs:        30,
	EstimateTimeAuth:      12,
	EstimateTimeGeneral:   20,
	EstimateTimeRateLimit: 12,
}

var HeaderAlias map[string][]string = map[string][]string{
	"X-Remain":  []string{"Remain", "Max-Downloads", "Max-Download"},
	"X-Expire":  []string{"Expire", "Max-Days", "Max-Day", "Purge-Time"},
	"X-Encrypt": []string{"Encrypt"},
	"X-Decrypt": []string{"Decrypt"},
	"X-Rename":  []string{"Rename"},
	"X-Acl":     []string{"Acl"},
}

// Config
var SecretLength int = 16
var DelTokenLength int = 12
var AuthCsvMaxMaga int = 128
var AssetFolderPath string = "eric/asset"
var SendHttpErrorDetail bool = true
var GatewaySchemeHostPort string = "http://127.0.0.1:8080"
var LogHttpAccessElapseTime bool = true

var RouterContext context.Context

// Todo do not that tool.NewHttpError log there, cause by then tmpfs.TmpFsError create, which is loged
func TmpfsError2HttpError(terr *tmpfs.TmpFsError, hfi *HfileContext) {
	switch terr.StatCode {
	default:
		hfi.HttpErr = tool.NewHttpErrorWithoutLog(terr, int(terr.StatCode), int(terr.LogLevel))
	}
	return
}

// Todo remove
// Must **not** log http error there
func HttpErrLog(w http.ResponseWriter, err error) bool {
	var ok bool
	var hperr tool.HttpError
	hperr, ok = err.(tool.HttpError)
	if !ok {
		tool.Error("Can not assert err to tool.HttpError, err type is %T %+v", err, err)
		return false
	}
	switch hperr.Code {
	case 429:
		w.Header().Set("Retry-After", fmt.Sprintf("%d", hperr.RetryAfter))
	}
	http.Error(w, hperr.Error(), hperr.Code)
	tool.EtLogf(2, int16(hperr.LogLevel), "%s", hperr.Error())
	return true
}

func RemoteAddr2RemoteIp(RemoteAddr string) string {
	var remoteip string
	if strings.ContainsRune(RemoteAddr, ':') {
		remoteip, _, _ = net.SplitHostPort(RemoteAddr)
	} else {
		remoteip = RemoteAddr
	}
	return remoteip
}

func EstimateCtx(flag uint) (context.Context, context.CancelFunc) {
	var totalctx context.Context
	var totaldura time.Duration
	var totalcancel context.CancelFunc
	for flg, dura := range EstimateTimeMap {
		if (flag & flg) != 0 {
			totaldura = totaldura + dura
		}
	}
	totalctx, totalcancel = context.WithTimeout(RouterContext, totaldura*time.Second)

	return totalctx, totalcancel
}

func FindHeaderByAlias(r *http.Request, name string) (string, bool) {
	var hval string
	for _, alias := range HeaderAlias[name] {
		hval = r.Header.Get(alias)
		if len(hval) != 0 {
			return hval, true
		}
	}
	hval = r.Header.Get(name)
	if len(hval) == 0 {
		return "", false
	}
	return hval, true
}

func ContentType2Mime(r *http.Request) string {
	var contenttype string
	contenttype = r.Header.Get("Content-Type")
	if len(contenttype) == 0 {
		return ""
	}
	return strings.ToLower(strings.Split(contenttype, ";")[0])
}

func CopyBody(ctx context.Context, w http.ResponseWriter, src io.Reader, size int64) *tool.HttpError {
	return LinkReadCopyBody(w, src, size, size, 0, func(inbyte int, outbyte int, cleanup bool) error {
		select {
		case <-ctx.Done():
			return tool.ContextTimeOutErr
		default:
		}
		return nil
	})
}

func MethodGuard(r *http.Request, allow []string) *tool.HttpError {
	for _, method := range allow {
		if r.Method == method {
			return nil
		}
	}
	return tool.NewHttpErr405(r.Method, allow)
}

func InitRouter(hfilectx context.Context) {
	RouterContext, _ = context.WithCancel(hfilectx)
	GatewaySchemeHostPort = strings.Trim(strings.TrimSpace(GatewaySchemeHostPort), "/")

	InitAuth()
}

func Hi(w http.ResponseWriter, req *http.Request) {
	hfi, endfunc := HfileContextStart(w, req, 0)
	defer endfunc()

	if hfi.HttpErr = MethodGuard(req, []string{http.MethodGet}); hfi.HttpErr != nil {
		return
	}

	hfi.StatusCode = 200
	fmt.Fprintf(w, "hi")
	return
}
func Header(w http.ResponseWriter, req *http.Request) {
	hfi, endfunc := HfileContextStart(w, req, 0)
	defer endfunc()

	hfi.StatusCode = 200
	for name, hds := range req.Header {
		for _, h := range hds {
			fmt.Fprintf(w, "%s : %s \n", name, h)
		}
	}
	return
}

func FormData(req *http.Request) (*multipart.Part, error) {
	var part *multipart.Part
	reader, err := req.MultipartReader()
	if err != nil {
		return part, tool.NewHttpError(err, 422, tool.INFO)
	}
	for {
		part, err = reader.NextPart()
		if err != nil {
			if err == io.EOF {
				break
			}

			return part, tool.NewHttpError(err, 422, tool.INFO)
		}
		var fln, frn string = part.FileName(), part.FormName()
		if fln == "" {
			tool.Debug("%s %s", fln, frn)
			continue
		}
		return part, nil
	}
	err = errors.New("multipart/form-data no file part.")
	return part, tool.NewHttpError(err, 422, tool.INFO)
}

func PrepareUploadNodeInfo(r *http.Request, hfi *HfileContext) *tool.NodeInfo {
	var ok bool
	var err error
	var tmp string
	var nodeinfo *tool.NodeInfo
	var remain, keepsecond int
	var filename string

	tmp, ok = FindHeaderByAlias(r, "X-Remain")
	if !ok {
		remain = hfi.Role.DefaultRemain
	} else {
		remain, err = strconv.Atoi(tmp)
		if err != nil {
			s := fmt.Sprintf("HTTP header %s`s value is not in the correct format, %s.", "X-Remain", err.Error())
			hfi.HttpErr = tool.NewHttpError(errors.New(s), 400, tool.DEBUG)
			return nil
		}

	}
	if hfi.Role.MaxRemain < remain {
		remain = hfi.Role.MaxRemain
	}
	tmp, ok = FindHeaderByAlias(r, "X-Expire")
	if !ok {
		keepsecond = 60 * 60 * 3
	} else {
		keepsecond, err = strconv.Atoi(tmp)
		if err != nil {
			s := fmt.Sprintf("HTTP header %s`s value is not in the correct format, %s.", "X-Expire", err.Error())
			hfi.HttpErr = tool.NewHttpError(errors.New(s), 400, tool.DEBUG)
			return nil
		}
	}
	if hfi.Role.MaxExpire < int64(keepsecond) {
		keepsecond = int(hfi.Role.MaxExpire)
	}
	nodeinfo = tool.NewNodeInfo(keepsecond, remain)

	filename, ok = mux.Vars(r)["firstpt"]
	if ok && len(filename) != 0 {
		nodeinfo.FileName = filename
	} else {
		tmp, ok = FindHeaderByAlias(r, "X-Rename")
		if ok {
			nodeinfo.FileName = tmp
		}
	}

	tmp, ok = FindHeaderByAlias(r, "X-Acl")
	if ok {
		nodeinfo.Acl = tool.String2NodeInfoAcl(tmp)
	}

	nodeinfo.Role = hfi.Role.Role
	nodeinfo.LinkIp = RemoteAddr2RemoteIp(r.RemoteAddr)
	nodeinfo.DelToken = tool.RandCryptoBase64url(DelTokenLength)
	return nodeinfo
}

func UploadReaderSelect(req *http.Request) (io.ReadCloser, int64, string, string, *tool.HttpError) {
	var s string
	var err error
	var terr *tool.HttpError
	var part *multipart.Part
	var realsize int64 = -1
	var mime, filename, contenttype string
	var multipartformdata = func() (*multipart.Part, int64, *tool.HttpError) {
		reader, err := req.MultipartReader()
		if err != nil {
			return part, 0, tool.NewHttpError(err, 400, tool.INFO)
		}
		for {
			part, err = reader.NextPart()
			if err != nil {
				if errors.Is(err, io.EOF) {
					break
				}
				part.Close()
				return part, 0, tool.NewHttpError(err, 400, tool.INFO)
			}
			filename = part.FileName()
			return part, -1, nil
		}
		part.Close()
		err = errors.New("multipart/form-data do not have any part.")
		return part, 0, tool.NewHttpError(err, 400, tool.INFO)
	}
	var formurlencoded = func() (string, *tool.HttpError) {
		var terr *tool.HttpError
		var filestr string
		err = req.ParseForm()
		if err != nil {
			err = errors.New(fmt.Sprintf("Parser form urlencoded %v", err))
			return "", tool.NewHttpError(err, 400, tool.WARN)
		}
		filestr = ""
		switch len(req.PostForm) {
		case 0:
			err = errors.New("x-www-form-urlencoded do not have any item.")
			terr = tool.NewHttpError(err, 400, tool.WARN)
		case 1:
			terr = nil
			for k, v := range req.PostForm {
				filename = k
				filestr = v[0]
				break
			}
		default:
			err = errors.New("x-www-form-urlencoded have more then 1 item.")
			terr = tool.NewHttpError(err, 400, tool.WARN)
		}
		return filestr, terr
	}
	// if realsize == -1 mean client use chunked transfer encoding
	realsize = req.ContentLength

	mime = ContentType2Mime(req)
	contenttype = "application/octet-stream"
	switch mime {
	case "":
	case "application/x-www-form-urlencoded":
		s, terr = formurlencoded()
		b := []byte(s)
		return io.NopCloser(bytes.NewReader(b)), int64(len(b)), filename, contenttype, terr
	case "multipart/form-data":
		part, realsize, terr = multipartformdata()
		return part, realsize, filename, contenttype, terr
	default:
		contenttype = req.Header.Get("Content-Type")
	}
	return req.Body, realsize, filename, contenttype, nil
}

// If Predict fail, must not use ctxfs, ctxfscancel, if ctxfs create at function fail, should cancel by fuinction
func LinkReadPredictSize(ctx context.Context, hfi *HfileContext, realsize int64, wrretryafter *int64, selectlink bool) (int64, context.Context, context.CancelFunc, func(int, int, bool) error) {
	var n, allowdn int64
	var role *tool.UserInfo
	var ctxfs context.Context
	var doallow func(int, int, bool) error
	var predictsize int64
	var ctxfscancel context.CancelFunc
	var predictlinktime int64
	var riskpremaga, periodmaga, maxmaga int

	role = hfi.Role
	if selectlink {
		maxmaga = role.MaxLinkMaga
		periodmaga = role.PeriodLinkMaga
		riskpremaga = role.RiskPreLink
	} else {
		maxmaga = role.MaxReadMaga
		periodmaga = role.PeriodReadMaga
		riskpremaga = role.RiskPreRead
	}

	predictsize = tool.Maga2Byte(int64(maxmaga))
	if realsize != -1 {
		if predictsize < realsize {
			hfi.HttpErr = tool.NewHttpErr413(predictsize, realsize)
			return predictsize, ctxfs, ctxfscancel, doallow
		} else {
			predictsize = realsize
		}
	}

	predictlinktime = (tool.Byte2Maga(predictsize) + 1) * int64(periodmaga)
	ctxfs, ctxfscancel = context.WithTimeout(RouterContext, time.Duration(predictlinktime)*time.Second)
	fmt.Printf("predictsize %d realsize %d.\n", predictsize, realsize)
	n = 0
	allowdn = 0
	doallow = func(inbyte int, outbyte int, cleanup bool) error {
		var i int64
		var err error
		var risk int
		var block bool
		n += int64(outbyte)
		select {
		case <-ctxfs.Done():
			return tool.ContextTimeOutErr
		default:
		}

		if (n - allowdn) > 10*OneMbByte {
			allowdn = n
			cleanup = true
			i = tool.MoreThen0Int64(n - allowdn)
		}
		if cleanup {
			risk = int(tool.Byte2Maga(i)+1) * riskpremaga
			(*wrretryafter), block, err = ratelimiter.Allow(ctxfs, hfi.RatelimiterIdt, risk, role)
			if err != nil {
				return err
			}
			if block {
				return tool.TooManyRequestsErr
			}
			if n > tool.Maga2Byte(int64(maxmaga)) {
				return tool.NewHttpErr413(tool.Maga2Byte(int64(maxmaga)), n)
			}
		}
		return nil
	}

	return predictsize, ctxfs, ctxfscancel, doallow
}

func LinkReadCopyBody(dst io.Writer, src io.Reader, predictsize int64, realsize int64, retryafter int64, sub func(int, int, bool) error) *tool.HttpError {
	var n int64
	var err error
	var terr *tool.HttpError

	n, err = tool.CopyNFunc(dst, src, predictsize, sub)
	if err != nil {
		if errors.Is(err, tool.CopyReachedErr) {
			terr = tool.NewHttpErr413(predictsize, n)
		} else if errors.Is(err, tool.ContextTimeOutErr) {
			terr = tool.NewHttpErr408("EstimateTimeFs")
		} else if errors.Is(err, tool.TooManyRequestsErr) {
			terr = tool.NewHttpErr429(retryafter)
		} else if errors.As(err, &terr) {
			terr = err.(*tool.HttpError)
		} else {
			terr = tool.NewHttpError(err, 500, tool.WARN)
		}
		return terr
	}
	if realsize != -1 && n != realsize {
		errstr := fmt.Sprintf("Content-Length indicate body shoud have %d byte, but %d byte is send.", realsize, n)
		terr = tool.NewHttpError(errors.New(errstr), 413, tool.WARN)
		return terr
	}
	return nil
}

func PrepareDownload(w http.ResponseWriter, hfi *HfileContext, nodeinfo *tool.NodeInfo) {
	w.Header().Set("Content-Type", nodeinfo.ContentType)
	w.Header().Set("X-IsEncrypt", strconv.FormatBool(nodeinfo.IsEncrypt))

	//Todo filename encoding
	if len(nodeinfo.FileName) != 0 {
		w.Header().Set("Content-Disposition", fmt.Sprintf("attachment; filename=\"%s\"", nodeinfo.FileName))
	}

	// chaek acl
	var passacl []string = []string{nodeinfo.Role}
	if nodeinfo.Role == "any" {
		return
	}
	for _, pr := range append(passacl, nodeinfo.Acl...) {
		if pr == hfi.Role.Role {
			return
		}
	}
	hfi.HttpErr = tool.NewHttpError(errors.New(fmt.Sprintf("Only allow [%s] Role download file.", strings.Join(passacl, ","))), 401, tool.DEBUG)
	return
}

func UploadTmp(w http.ResponseWriter, req *http.Request) {
	hfi, endfunc := HfileContextStart(w, req, HFILEAUTHBLOCK)
	defer endfunc()
	if hfi.HttpErr != nil {
		return
	}
	var terr *tmpfs.TmpFsError
	var rf io.ReadCloser
	var wf io.WriteCloser
	var folder string
	var doallow func(int, int, bool) error
	var realsize int64
	var nodeinfo *tool.NodeInfo
	var ctxfsop, ctxlink context.Context
	var ctxfsopcancel, ctxlinkcancel context.CancelFunc
	var filename string
	var retryafter int64
	var predictsize int64
	var downloadurl string
	var contenttype string
	var newlinebeforedownloadurl bool

	ctxfsop, ctxfsopcancel = EstimateCtx(EstimateTimeFsOp | EstimateTimeGeneral)
	defer ctxfsopcancel()
	if hfi.HttpErr = MethodGuard(req, []string{http.MethodPost, http.MethodPut}); hfi.HttpErr != nil {
		return
	}

	nodeinfo = PrepareUploadNodeInfo(req, hfi)
	if hfi.HttpErr != nil {
		return
	}
	//Todo X-Encrypt

	if rf, realsize, filename, contenttype, hfi.HttpErr = UploadReaderSelect(req); hfi.HttpErr != nil {
		return
	}
	defer rf.Close()
	if len(filename) != 0 {
		nodeinfo.FileName = filename
	}
	nodeinfo.ContentType = contenttype
	// Todo filename length limiter and able to display.
	if predictsize, ctxlink, ctxlinkcancel, doallow = LinkReadPredictSize(ctxfsop, hfi, realsize, &retryafter, true); hfi.HttpErr != nil {
		return
	}
	defer ctxlinkcancel()
	wf, folder, terr = tmpfs.Link(ctxlink, ctxfsop, nodeinfo)
	if terr != nil {
		TmpfsError2HttpError(terr, hfi)
		return
	}
	defer wf.Close()
	w.Header().Set("X-Url-Delete", fmt.Sprintf("%s/del/%s/%s", GatewaySchemeHostPort, folder, nodeinfo.DelToken))

	if hfi.HttpErr = LinkReadCopyBody(wf, rf, predictsize, realsize, retryafter, doallow); hfi.HttpErr != nil {
		return
	}
	newlinebeforedownloadurl = false
	if strings.Contains(strings.ToLower(req.UserAgent()), "curl") {
		newlinebeforedownloadurl = true
	}
	if newlinebeforedownloadurl {
		downloadurl = "\n"
	}
	downloadurl = fmt.Sprintf("%s%s/%s\n", downloadurl, GatewaySchemeHostPort, folder)
	hfi.StatusCode = 200
	w.Write([]byte(downloadurl))
	return
}

func DownloadTmp(w http.ResponseWriter, req *http.Request) {
	hfi, endfunc := HfileContextStart(w, req, HFILEAUTHBLOCK)
	defer endfunc()
	if hfi.HttpErr != nil {
		return
	}

	var fd tmpfs.TmpfsFd
	var err error
	var terr *tmpfs.TmpFsError
	var folder string
	var writer io.Writer
	var doallow func(int, int, bool) error
	var ctxfsop, ctxread context.Context
	var ctxfsopcancel, ctxreadcancel context.CancelFunc
	var nodeinfo *tool.NodeInfo
	var predictsize, retryafter, realsize int64

	ctxfsop, ctxfsopcancel = EstimateCtx(EstimateTimeFsOp | EstimateTimeGeneral)
	defer ctxfsopcancel()
	if hfi.HttpErr = MethodGuard(req, []string{http.MethodGet}); hfi.HttpErr != nil {
		return
	}

	realsize = -1
	folder, _ = mux.Vars(req)["firstpt"]

	if predictsize, ctxread, ctxreadcancel, doallow = LinkReadPredictSize(ctxfsop, hfi, realsize, &retryafter, false); hfi.HttpErr != nil {
		return
	}
	defer ctxreadcancel()

	fd, nodeinfo, terr = tmpfs.Read(ctxread, ctxfsop, folder, false)
	if terr != nil {
		TmpfsError2HttpError(terr, hfi)
		return
	}
	defer fd.Close()
	//Todo X-Decrypt

	PrepareDownload(w, hfi, nodeinfo)
	if hfi.HttpErr != nil {
		return
	}
	tool.Info("nodeinfo %+v\n", nodeinfo)

	archize := zip.NewWriter(w)
	writer, err = archize.Create(nodeinfo.FileName)
	if err != nil {
		hfi.HttpErr = tool.NewHttpError(err, 500, tool.ERROR)
		return
	}

	hfi.StatusCode = 200
	realsize = nodeinfo.Size

	if hfi.HttpErr = LinkReadCopyBody(writer, fd, predictsize, realsize, retryafter, doallow); hfi.HttpErr != nil {
		return
	}
	archize.Close()
	return
}

func Deletetmp(w http.ResponseWriter, r *http.Request) {
	hfi, endfunc := HfileContextStart(w, r, HFILEAUTHALLOWFSOP)
	defer endfunc()
	if hfi.HttpErr != nil {
		return
	}

	var candel bool
	var terr *tmpfs.TmpFsError
	var folder, token string
	var nodeinfo *tool.NodeInfo
	ctxfsop, ctxfsopcancel := EstimateCtx(EstimateTimeFsOp | EstimateTimeGeneral)
	defer ctxfsopcancel()
	if hfi.HttpErr = MethodGuard(r, []string{http.MethodGet, http.MethodPost, http.MethodDelete}); hfi.HttpErr != nil {
		return
	}

	token, _ = mux.Vars(r)["token"]
	folder = mux.Vars(r)["folder"]

	_, nodeinfo, terr = tmpfs.Read(ctxfsop, ctxfsop, folder, true)
	if terr != nil {
		TmpfsError2HttpError(terr, hfi)
		return
	}
	candel = false
	if nodeinfo.DelToken == token {
		candel = true
	} else if hfi.Role.DeleteRight && hfi.Role.Role == nodeinfo.Role {
		candel = true
	}

	if !candel {
		hfi.StatusCode = 202
		fmt.Fprintf(w, "Can not delete %s, by illegal token.", folder)
		return
	}
	terr = tmpfs.UnLink(ctxfsop, folder)
	if terr != nil {
		TmpfsError2HttpError(terr, hfi)
		return
	}
	hfi.StatusCode = 200
	fmt.Fprintf(w, "Delete %s.", folder)
	return

}

func QrCodeTmp(w http.ResponseWriter, r *http.Request) {
	hfi, endfunc := HfileContextStart(w, r, HFILEAUTHALLOWFSOP)
	defer endfunc()
	if hfi.HttpErr != nil {
		return
	}

	var err error
	var terr *tmpfs.TmpFsError
	var qrpng []byte
	var folder string
	var downloadurl string
	ctxfsop, ctxfsopcancel := EstimateCtx(EstimateTimeGeneral)

	defer ctxfsopcancel()
	if hfi.HttpErr = MethodGuard(r, []string{http.MethodGet}); hfi.HttpErr != nil {
		return
	}

	folder = mux.Vars(r)["folder"]
	if terr = tmpfs.FolderSymbolCheckRetTmpfsError(folder, "ExternalLibrary"); terr != nil {
		TmpfsError2HttpError(terr, hfi)
		return
	}
	downloadurl = fmt.Sprintf("%s/%s", GatewaySchemeHostPort, folder)
	qrpng, err = qrcode.Encode(downloadurl, qrcode.High, 256)
	if err != nil {
		hfi.HttpErr = tool.NewHttpError(err, 500, tool.WARN)
	}
	w.Header().Set("Content-Type", "image/png ")
	w.Header().Set("Content-Length", fmt.Sprintf("%d", len(qrpng)))
	hfi.StatusCode = 200
	hfi.HttpErr = CopyBody(ctxfsop, w, bytes.NewReader(qrpng), int64(len(qrpng)))
	return
}

func AuthCsv(w http.ResponseWriter, r *http.Request) {
	hfi, endfunc := HfileContextStart(w, r, HFILEAUTHALLOWFSOP)
	defer endfunc()
	if hfi.HttpErr != nil {
		return
	}

	var n int64
	var err error
	//	var buf []byte
	var buff bytes.Buffer
	var buffwr *bufio.Writer
	var reader io.ReadCloser
	ctxfsop, ctxfsopcancel := EstimateCtx(EstimateTimeGeneral)
	defer ctxfsopcancel()

	if hfi.HttpErr = MethodGuard(r, []string{http.MethodGet, http.MethodPost, http.MethodPut}); hfi.HttpErr != nil {
		return
	}
	if !hfi.Role.Admin {
		hfi.HttpErr = tool.NewHttpErr403("Admin Require", fmt.Sprintf("%s do not set admin flag in authcsv.", hfi.Role.Role))
		return
	}
	buffwr = bufio.NewWriter(&buff)
	if r.Method == http.MethodGet {
		_RoleMapToAuthCsv(ctxfsop, buffwr)
		buffwr.Flush()
		w.Header().Set("Content-Type", "text/csv")
		w.Header().Set("Content-Length", strconv.Itoa(buff.Len()))

		hfi.StatusCode = 200
		n, err = tool.CopyN(ctxfsop, w, &buff, int64(buff.Len()))
		_ = n
		if err != nil {
			hfi.HttpErr = tool.NewHttpError(err, 500, tool.ERROR)
		}
		return
	} else {
		if reader, _, _, _, hfi.HttpErr = UploadReaderSelect(r); hfi.HttpErr != nil {
			return
		}
		n, err = tool.CopyN(ctxfsop, buffwr, reader, tool.Maga2Byte(int64(AuthCsvMaxMaga)))
		if err != nil {
			if errors.Is(err, tool.ContextTimeOutErr) {
				hfi.HttpErr = tool.NewHttpErr408("EstimateTimeGeneral")
			} else if errors.Is(err, tool.CopyReachedErr) {
				hfi.HttpErr = tool.NewHttpErr413(tool.Maga2Byte(int64(AuthCsvMaxMaga)), n)
			} else {
				hfi.HttpErr = tool.NewHttpError(err, 500, tool.ERROR)
			}
			return
		}
		buffwr.Flush()
		_, err = _RoleMapFromAuthCsv(ctxfsop, &buff)
		if err != nil {
			hfi.HttpErr = tool.NewHttpError(err, 500, tool.ERROR)
			return
		}
		RoleMapToAuthCsv(ctxfsop, AuthCsvPath)
		hfi.StatusCode = 200
		fmt.Fprintf(w, "Update Auth Csv.")
		return
	}

}

func AuthCreate(w http.ResponseWriter, r *http.Request) {
	hfi, endfunc := HfileContextStart(w, r, HFILEAUTHALLOWFSOP)
	defer endfunc()
	if hfi.HttpErr != nil {
		return
	}
	var err error
	var role *tool.UserInfo
	var group int
	var rolename, groupstr string
	if hfi.HttpErr = MethodGuard(r, []string{http.MethodGet, http.MethodPost}); hfi.HttpErr != nil {
		return
	}
	if !hfi.Role.Admin {
		hfi.HttpErr = tool.NewHttpErr403("Admin Require", fmt.Sprintf("%s do not set admin flag in authcsv.", hfi.Role.Role))
		return
	}
	ctxfsop, ctxfsopcancel := EstimateCtx(EstimateTimeGeneral)
	defer ctxfsopcancel()

	rolename = mux.Vars(r)["role"]
	groupstr = mux.Vars(r)["group"]
	group, err = strconv.Atoi(groupstr)
	if err != nil {
		group = 0
		tool.Error("AuthCreate can not convert group to int %v", err)
	}
	role = tool.NewUserInfoGroup(rolename, uint(group))
	RoleCreate(ctxfsop, role)
	hfi.StatusCode = 200
	fmt.Fprintf(w, "%s created.", role.Role)
}

func DirectUrl(w http.ResponseWriter, r *http.Request) {

}

func Login(w http.ResponseWriter, r *http.Request) {
	hfi, endfunc := HfileContextStart(w, r, HFILEAUTHALLOWFSOP)
	defer endfunc()
	if hfi.HttpErr != nil {
		return
	}
	var err error
	var lgrole *tool.UserInfo
	var cookie *http.Cookie
	var rolename, secret string

	if hfi.HttpErr = MethodGuard(r, []string{http.MethodPost}); hfi.HttpErr != nil {
		return
	}
	ctxfsop, ctxfsopcancel := EstimateCtx(EstimateTimeGeneral)
	defer ctxfsopcancel()

	err = r.ParseForm()
	if err != nil {
		hfi.HttpErr = tool.NewHttpError(err, 400, tool.INFO)
		return
	}
	for k, vlst := range r.PostForm {
		v := ""
		if len(vlst) > 0 {
			v = vlst[0]
		} else {
			tool.Error("PostForm get length 0 value on %s", k)
		}
		switch strings.ToLower(k) {
		case "role", "user":
			rolename = strings.TrimSpace(v)
		case "secret", "passwd", "password":
			secret = strings.TrimSpace(v)
		}
	}
	if len(rolename) == 0 || len(secret) == 0 {
		hfi.HttpErr = tool.NewHttpError(errors.New("Please Post application/x-www-form-urlencoded with role and secret key."), 400, tool.DEBUG)
		return
	}
	lgrole, hfi.HttpErr = DoAuthUserMethod(ctxfsop, &AuthUserMethod{
		RoleName: rolename,
		AuthType: "PostPassword",
		Enable: func(role *tool.UserInfo) bool {
			return role.PostPasswdOk && (len(role.PasswdSha256) != 0)
		},
		CheckSecret: func(role *tool.UserInfo) bool {
			if role.PasswdSha256 == tool.PasswordSha256(secret, role.Salt) {
				return true
			}
			return false
		},
	})
	if hfi.HttpErr != nil {
		ratelimiter.Allow(ctxfsop, hfi.RatelimiterIdt, 60, hfi.Role)
		return
	}

	if lgrole.CookieExpire < (time.Now().Unix()-60) || len(lgrole.Secret) == 0 {
		lgrole.CookieExpire = time.Now().Unix() + 60*60*3
		lgrole.Secret = tool.RandCryptoBase64url(SecretLength)
		RoleChang(ctxfsop, lgrole)
	}
	cookie = &http.Cookie{
		Name:   "role",
		Value:  rolename,
		Path:   "/",
		MaxAge: int(lgrole.CookieExpire - time.Now().Unix()),
	}
	http.SetCookie(w, cookie)
	cookie = &http.Cookie{
		Name:   "secret",
		Value:  lgrole.Secret,
		Path:   "/",
		MaxAge: int(lgrole.CookieExpire - time.Now().Unix()),
	}
	http.SetCookie(w, cookie)
	hfi.StatusCode = 200
	fmt.Fprintf(w, "Login success, set cookie.")
}

func RatelimiterStatus(w http.ResponseWriter, r *http.Request) {
	hfi, endfunc := HfileContextStart(w, r, HFILEAUTHALLOWFSOP)
	defer endfunc()
	if hfi.HttpErr != nil {
		return
	}

	var role *tool.UserInfo

	if hfi.HttpErr = MethodGuard(r, []string{http.MethodGet}); hfi.HttpErr != nil {
		return
	}
	ctxfsop, ctxfsopcancel := EstimateCtx(EstimateTimeGeneral)
	defer ctxfsopcancel()

	role = hfi.Role
	lv0, lv1, lv2, err := ratelimiter.Risk(ctxfsop, hfi.RatelimiterIdt, role.LeakRate)
	if err != nil {
		hfi.HttpErr = tool.NewHttpError(err, 200, tool.ERROR)
		return
	}
	hfi.StatusCode = 200
	fmt.Fprintf(w, "%s %s\n", role.Role, hfi.RatelimiterIdt)
	fmt.Fprintf(w, "LeakyBucket Water %d Leak per second %d Burst %d\n", lv0, role.LeakRate, role.Burst)
	fmt.Fprintf(w, "SlideWindow_0 Total %d Full %d\n", lv1, role.Lv1Full)
	fmt.Fprintf(w, "SlideWindow_1 Total %d Full %d\n", lv2, role.Lv2Full)
	return
}

func simpleMw(next http.Handler) http.Handler {
	var hhdr http.Handler = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var urlpath = r.URL.Path
		// Do stuff here
		fmt.Println(r.RequestURI)
		urlpath = filepath.Clean(urlpath)
		StaticFd(urlpath)(w, r)
		// Call the next handler, which can be another middleware in the chain, or the final handler.
		next.ServeHTTP(w, r)
	})

	return hhdr
}

func StaticFolder(dirname string, strip string) http.Handler {
	var httpfs http.Handler = http.FileServer(http.Dir(dirname))

	if strip != "" {
		httpfs = http.StripPrefix(strip, httpfs)
	}

	return httpfs
}

func StaticFd(filename string) func(http.ResponseWriter, *http.Request) {
	var f = func(w http.ResponseWriter, req *http.Request) {
		hfi, endfunc := HfileContextStart(w, req, 0)
		defer endfunc()
		if hfi.HttpErr = MethodGuard(req, []string{http.MethodGet}); hfi.HttpErr != nil {
			return
		}
		ctx, cancel := EstimateCtx(EstimateTimeFs)
		defer cancel()
		filename = filepath.Clean(filename)
		// Todo add os.stat for If-Not
		infd, err := os.Open(filepath.Join(AssetFolderPath, filename))
		defer infd.Close()
		if err != nil {
			hfi.HttpErr = tool.NewHttpError(err, 404, tool.ERROR)
			return
		}
		hfi.StatusCode = 200
		_, err = tool.CopyN(ctx, w, infd, OneMbByte*512)
		if err != nil {
			tool.NewHttpError(err, 500, tool.DEBUG)
			return
		}
	}
	return f
}

func uploadStt(w http.ResponseWriter, req *http.Request) {
	var folder string = mux.Vars(req)["folder"]
	var fname string = mux.Vars(req)["fd"]
	wf, err := tmpfs.SttLink(folder, fname)
	defer wf.Close()
	if err != nil {
		//		TmpfsError2HttpError(w, err.(tmpfs.TmpFsError))
	}
	//Todo
	//rf, err := PotFormat(req)
	if err != nil {
		HttpErrLog(w, err)
		return
	}
	//_, err = io.Copy(wf, rf)
	if err != nil {
		HttpErrLog(w, tool.NewHttpError(err, 500, tool.WARN))
		return
	}
	w.Write([]byte(folder + fname))
}

func downloadSst(w http.ResponseWriter, req *http.Request) {
	folder := mux.Vars(req)["folder"]
	fd := mux.Vars(req)["fd"]
	sf, err := tmpfs.SttRead(folder, fd)
	defer sf.Close()
	if err != nil {
		//		TmpfsError2HttpError(w, err.(tmpfs.TmpFsError))
		return
	}
	//tool.CopyN(w, sf, OneMbByte*4096)
}
