package middleware

import (
	"context"
	"errors"
	"fmt"
	"hfile/ratelimiter"
	"hfile/tool"
	"net/http"
	"strings"
	"time"
)

const (
	HFILEAUTHBLOCK     uint = (1 << 3)
	HFILEAUTHALLOWFSOP uint = (1 << 3) + (1 << 4)
)

type AuthUserMethod struct {
	AuthType    string
	RoleName    string
	Enable      func(role *tool.UserInfo) bool
	CheckSecret func(role *tool.UserInfo) bool
}

type HfileContext struct {
	StartFlag  uint
	StatusCode int
	// Http Access Log
	StartTp          time.Time
	HttpAccessLogFmt string

	Role *tool.UserInfo
	//Http Error
	HttpErr *tool.HttpError
	//Ratelimiter
	RatelimiterIdt string
}

func NewHfileContext() *HfileContext {
	return &HfileContext{}
}

func DoAuthUserMethod(ctx context.Context, aum *AuthUserMethod) (*tool.UserInfo, *tool.HttpError) {
	var ok bool
	var role *tool.UserInfo
	role, ok = RoleGet(ctx, aum.RoleName)
	if !ok {
		return nil, tool.NewHttpErr403(aum.AuthType, "User not exist")
	}
	if !aum.Enable(role) {
		return role, tool.NewHttpErr403(aum.AuthType, "Auth config csv not enable this method")
	}
	if !aum.CheckSecret(role) {
		return role, tool.NewHttpErr403(aum.AuthType, "Password not match")
	}

	return role, nil
}

func HfileContextStart(w http.ResponseWriter, r *http.Request, flag uint) (*HfileContext, func()) {
	var hlc *HfileContext = NewHfileContext()
	hlc.Start(w, r, flag)
	return hlc, func() {
		hlc.End(w, r)
	}
}

func (hlc *HfileContext) HttpAccessLog(w http.ResponseWriter, r *http.Request) {
	var mtd string
	var rpa string
	var remoteip string

	if LogHttpAccessElapseTime {
		hlc.StartTp = time.Now()
	}
	mtd = r.Method
	rpa = r.URL.Path
	remoteip = RemoteAddr2RemoteIp(r.RemoteAddr)

	hlc.HttpAccessLogFmt = fmt.Sprintf("%s %s %d/%d %s", remoteip, mtd, r.ProtoMajor, r.ProtoMinor, rpa)
	if !LogHttpAccessElapseTime {
		tool.HttpAccessLog(hlc.HttpAccessLogFmt)
	}

}

func (hlc *HfileContext) HttpAccessLogEnd(w http.ResponseWriter, r *http.Request) {
	if !LogHttpAccessElapseTime {
		return
	}
	var elapsed time.Duration
	elapsed = time.Now().Sub(hlc.StartTp)
	hlc.HttpAccessLogFmt = fmt.Sprintf("%s %d %dms", hlc.HttpAccessLogFmt, hlc.StatusCode, elapsed.Milliseconds())
	if hlc.HttpErr != nil {
		errstr := hlc.HttpErr.Error()
		hlc.HttpAccessLogFmt = fmt.Sprintf("%s %s", hlc.HttpAccessLogFmt, errstr)
	}
	tool.HttpAccessLog(hlc.HttpAccessLogFmt)
	return
}

func (hlc *HfileContext) AuthUser(w http.ResponseWriter, r *http.Request) {
	var err error
	var authtype string
	ctx, cancel := EstimateCtx(EstimateTimeAuth)
	defer cancel()

	role, passwd, ok := r.BasicAuth()
	if ok {
		hlc.Role, hlc.HttpErr = DoAuthUserMethod(ctx, &AuthUserMethod{
			Enable: func(role *tool.UserInfo) bool {
				return role.BasicOk && (len(role.PasswdSha256) != 0)
			},
			AuthType: "BasicAuth",
			RoleName: role,
			CheckSecret: func(role *tool.UserInfo) bool {
				if tool.PasswordSha256(passwd, role.Salt) == role.PasswdSha256 {
					return true
				}
				return false
			},
		})
		return
	}
	// jwt
	authtype = "JWT"
	_ = authtype

	var validcookie = func(name string, cookie *http.Cookie, val *string) bool {
		if strings.ToLower(cookie.Name) == name {
			err = cookie.Valid()
			if err != nil {
				return false
			}
			val = &cookie.Value
			return true
		}
		return false
	}

	ok = false
	rlok := false
	seok := false
	for _, cookie := range r.Cookies() {
		if rlok = validcookie("role", cookie, &role); rlok {
			role = strings.ToLower(role)
		}
		seok = validcookie("secret", cookie, &passwd)
		if rlok && seok {
			ok = true
			break
		}
	}
	if ok {
		hlc.Role, hlc.HttpErr = DoAuthUserMethod(ctx, &AuthUserMethod{
			Enable: func(role *tool.UserInfo) bool {
				return role.CookieOk && (len(hlc.Role.Secret) != 0)
			},
			AuthType: "Cookie",
			RoleName: role,
			CheckSecret: func(role *tool.UserInfo) bool {
				if role.CookieExpire < time.Now().Unix() {
					// Todo delete cookie
					return false
				}
				if passwd == role.Secret {
					return true
				}
				return false
			},
		})
		return
	} else {
		role = ""
	}
	if len(role) == 0 {
		role = "any"
		hlc.Role, ok = RoleGet(ctx, role)
		if !ok {
			tool.Error("Need to add any for anonymous user.")
			hlc.HttpErr = tool.NewHttpError(errors.New("Need to add any for anonymous user."), 500, tool.ERROR)
		}
		return
	}
	return
}

func (hlc *HfileContext) Ratelimiter(w http.ResponseWriter, r *http.Request) {
	ctx, cancel := EstimateCtx(EstimateTimeRateLimit)
	defer cancel()

	var ok, block bool
	var err error
	var period int64
	if hlc.Role.Role == "any" {
		hlc.RatelimiterIdt = RemoteAddr2RemoteIp(r.RemoteAddr)
	} else {
		hlc.RatelimiterIdt = hlc.Role.Role
	}

	// for debug
	if len(hlc.RatelimiterIdt) == 0 {
		tool.Error("HfileContext::RatelimiterIdt len(0) == 0")
	}
	if (hlc.StartFlag & (1 << 4)) == 0 {
		period, block, err = ratelimiter.IsBlock(ctx, hlc.RatelimiterIdt)
		ok = !block
	} else {
		period, ok, err = ratelimiter.Allow(ctx, hlc.RatelimiterIdt, 3, hlc.Role)
	}
	if err != nil {
		hlc.HttpErr = tool.NewHttpError(err, 500, tool.ERROR)
		return
	}
	if !ok {
		hlc.HttpErr = tool.NewHttpErr429(tool.MoreThen0Int64(period-time.Now().Unix()) + 1)
		return
	}
	return
}

func (hlc *HfileContext) Start(w http.ResponseWriter, r *http.Request, flag uint) {
	hlc.StartFlag = flag
	hlc.HttpAccessLog(w, r)
	if (hlc.StartFlag & (1 << 3)) != 0 {
		hlc.AuthUser(w, r)
		if hlc.HttpErr != nil {
			return
		}
		hlc.Ratelimiter(w, r)
		if hlc.HttpErr != nil {
			return
		}
	}
	return
}

// Write http status code and message, if hlc.HttpErr.StatusCode dose not 0
func (hlc *HfileContext) HttpErrHandle(w http.ResponseWriter) bool {
	tool.Error("HttpErrHandle %v", hlc.HttpErr)
	var errstr string
	if hlc.HttpErr == nil {
		tool.Error("HfileContext::End without status code or HttpErr.")
		return false
	}

	switch hlc.HttpErr.Code {
	case 429:
		w.Header().Set("Retry-After", fmt.Sprintf("%d", hlc.HttpErr.RetryAfter))
	case 405:
		w.Header().Set("Allow", strings.Join(hlc.HttpErr.Allow, ","))
	}

	if SendHttpErrorDetail {
		errstr = hlc.HttpErr.Error()
	} else {
		errstr = http.StatusText(hlc.HttpErr.Code)
	}
	http.Error(w, errstr, hlc.HttpErr.Code)
	return true
}

func (hlc *HfileContext) End(w http.ResponseWriter, r *http.Request) {
	// Presumably no response was written
	if hlc.StatusCode == 0 {
		hlc.HttpErrHandle(w)
	}
	hlc.HttpAccessLogEnd(w, r)
}
