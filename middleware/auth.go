package middleware

import (
	"context"
	"hfile/tool"
	"io"
	"os"

	"github.com/emirpasic/gods/maps/treemap"
	"github.com/gocarina/gocsv"
)

var RoleMap *treemap.Map = treemap.NewWithStringComparator()

// config
var AuthCsvPath string = "auth.csv"
var AuthCsvDefaultPath string = "default_auth.csv"

func InitAuth() {
	RoleMapInit(RouterContext)
	RoleMapToAuthCsv(RouterContext, AuthCsvDefaultPath)
	RoleMap.Clear()
	if err := RoleMapFromAuthCsv(RouterContext); err != nil {
		panic(err)
	}
}

func RoleMapInit(ctx context.Context) {
	_RolePut(ctx, tool.NewUserInfoBase("base"))
	_RolePut(ctx, tool.NewUserInfoGroup("any", tool.RoleGroupAny))
	_RolePut(ctx, tool.NewUserInfoGroup("admin", tool.RoleGroupAdmin))
}

func _RoleMapToAuthCsv(ctx context.Context, f io.Writer) {
	var err error
	var itflist []interface{}
	var keylist []string
	var rolelist []*tool.UserInfo

	keylist = make([]string, 0, 32)
	rolelist = make([]*tool.UserInfo, 0, 32)
	unlockfunc, _ := tool.AuthRoleRlock(ctx)
	itflist = RoleMap.Keys()
	unlockfunc()
	for _, itf := range itflist {
		k := itf.(string)
		keylist = append(keylist, k)
	}
	itflist = make([]interface{}, 0)
	for _, k := range keylist {
		role, ok := RoleGet(ctx, k)
		if ok {
			rolelist = append(rolelist, role)
		}
		select {
		case <-ctx.Done():
			tool.Info("RoleMapToAuthCsv ContextTimeOutErr.")
		default:
		}
	}
	err = gocsv.Marshal(&rolelist, f)
	if err != nil {
		tool.Warn("gocsv.Marshal %+v", err)
	}
}

func RoleMapToAuthCsv(ctx context.Context, filepath string) {
	var f *os.File
	var err error

	f, err = os.OpenFile(filepath, os.O_CREATE|os.O_WRONLY|os.O_TRUNC, 664)
	defer f.Close()
	if err != nil {
		tool.Warn("RoleMapToAuthCsv Open %s %+v", tool.Abs(filepath), err)
	}
	unlockfunc, _ := tool.AuthCsvLock(ctx)
	_RoleMapToAuthCsv(ctx, f)
	unlockfunc()
}

func _RoleMapFromAuthCsv(ctx context.Context, f io.Reader) (writeback bool, err error) {
	var rl *tool.UserInfo
	var rolemap *treemap.Map = treemap.NewWithStringComparator()
	var rolelist []*tool.UserInfo
	writeback = false

	err = gocsv.Unmarshal(f, &rolelist)
	if err != nil {
		tool.Warn("gocsv.Unmarshal %+v", err)
		return
	}
	for _, rl = range rolelist {
		if (rl.BasicOk || rl.PostPasswdOk) && len(rl.Salt) == 0 && len(rl.PasswdSha256) != 0 {
			writeback = true
			rl.Salt = tool.RandCryptoBase64url(SecretLength)
			rl.PasswdSha256 = tool.PasswordSha256(rl.PasswdSha256, rl.Salt)
		}
		rolemap.Put(rl.Role, rl)
		select {
		case <-ctx.Done():
			tool.Info("_RoleMapFromAuthCsv ContextTimeOutErr.")
		default:
		}
	}
	unlockfunc, _ := tool.AuthRoleWlock(ctx)
	RoleMap = rolemap
	unlockfunc()
	return
}

func RoleMapFromAuthCsv(ctx context.Context) error {
	var f *os.File
	var err error
	var writeback bool

	f, err = os.OpenFile(AuthCsvPath, os.O_RDONLY, 664)
	if err != nil {
		tool.Error("RoleMapFromAuthCsv Open %s %+v", AuthCsvPath, err)
		return err
	}
	unlockfunc, _ := tool.AuthCsvLock(ctx)
	writeback, err = _RoleMapFromAuthCsv(ctx, f)
	f.Close()
	unlockfunc()
	if err != nil {
		return err
	}
	if writeback {
		RoleMapToAuthCsv(ctx, AuthCsvPath)
	}
	return nil
}

func RoleGet(rctx context.Context, role string) (*tool.UserInfo, bool) {
	var ok bool
	var itp interface{}
	var rin *tool.UserInfo

	unlockfunc, _ := tool.AuthRoleRlock(rctx)
	itp, ok = RoleMap.Get(role)
	unlockfunc()
	if !ok {
		return rin, false
	}
	if rin, ok = itp.(*tool.UserInfo); !ok {
		tool.NewTypeAssertErr("*tool.UserInfo", itp)
		return rin, false
	}
	return rin, true
}

func _RolePut(ctx context.Context, rin *tool.UserInfo) {
	unlockfunc, _ := tool.AuthRoleWlock(ctx)
	RoleMap.Put(rin.Role, rin)
	unlockfunc()
}

func RoleCreate(ctx context.Context, rin *tool.UserInfo) {
	var ok bool
	_, ok = RoleGet(ctx, rin.Role)
	if !ok {
		_RolePut(ctx, rin)
	} else {
		tool.Warn("RoleCreate(%s) Role exist", rin.Role)
	}
}

func RoleChang(ctx context.Context, rin *tool.UserInfo) {
	var ok bool
	_, ok = RoleGet(ctx, rin.Role)
	if ok {
		_RolePut(ctx, rin)
	} else {
		tool.Warn("RoleChang(%s) Role not exist", rin.Role)
	}
}

func RoleDelete(ctx *context.Context, rin *tool.UserInfo) {

}
