package tool

import (
	"context"
	"crypto/rand"
	"crypto/sha256"
	"encoding/base64"
	"encoding/binary"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"runtime"
	"strings"
	"sync"
)

var Logger *log.Logger = log.New(os.Stdout, "", log.Ldate|log.Ltime)

const (
	ERROR       = 1
	WARN        = 2
	INFO        = 3
	DEBUG       = 4
	HTTPACCSE   = 8
	TMPFSSCANRM = 16
)

var LgLevel int16 = HTTPACCSE | DEBUG | TMPFSSCANRM

func EtLogf(calldepth int, level int16, format string, v ...interface{}) {
	var prefix string = "nil"
	if level <= DEBUG {
		if level > (LgLevel & 0x07) {
			return
		}
	} else {
		if (level & LgLevel) == 0 {
			return
		}
	}

	switch level {
	case DEBUG:
		prefix = "DEBUG"
	case INFO:
		prefix = "INFO"
	case WARN:
		prefix = "WARN"
	case ERROR:
		prefix = "ERROR"
	case HTTPACCSE:
		prefix = "HTTPACCSE"
	case TMPFSSCANRM:
		prefix = "SCANRM"
	default:
		Logger.Printf("ERROR cannot match log level.\n")
	}

	_, file, line, ok := runtime.Caller(calldepth)
	if !ok {
		file = "???"
		line = 0
	}
	format = fmt.Sprintf(" %s:%d %s %s", filepath.Base(file), line, prefix, format)
	Logger.Printf(format, v...)
}
func Logf(level int16, format string, v ...interface{}) {
	EtLogf(2, level, format, v...)
}
func Debug(format string, v ...interface{}) {
	EtLogf(2, DEBUG, format, v...)
}
func Info(format string, v ...interface{}) {
	EtLogf(2, INFO, format, v...)
}
func Warn(format string, v ...interface{}) {
	EtLogf(2, WARN, format, v...)
}
func Error(format string, v ...interface{}) {
	EtLogf(2, ERROR, format, v...)
}
func HttpAccessLog(logstr string) {
	if (LgLevel & HTTPACCSE) != HTTPACCSE {
		return
	}
	Logger.Print(logstr)
}

func ResetLoggerPath(lgpath string) bool {
	if strings.ToLower(lgpath) == "stdout" {
		Logger = log.New(os.Stdout, "", log.Ldate|log.Ltime)
		return true
	}
	f, err := os.OpenFile(lgpath, os.O_CREATE|os.O_APPEND|os.O_WRONLY, 664)
	if err != nil {
		Error("Can not open %s for logger.\n", Abs(lgpath))
		return false
	}
	Logger = log.New(f, "", log.Ldate|log.Ltime)
	return true
}

type HttpError struct {
	Err        error
	Code       int
	LogLevel   int
	RetryAfter int64
	AuthType   string
	Allow      []string
}

func (err HttpError) Error() string {
	return fmt.Sprintf("%d %s", err.Code, err.Err.Error())
}

func (err *HttpError) Log() *HttpError {
	EtLogf(3, int16(err.LogLevel), "StatusCode%d %s", err.Code, err.Err.Error())
	return err
}

func NewHttpErrorWithoutLog(err error, code int, loglevel int) *HttpError {
	return &HttpError{
		Err:      err,
		Code:     code,
		LogLevel: loglevel,
	}
}

func NewHttpError(err error, code int, loglevel int) *HttpError {
	return (&HttpError{
		Err:      err,
		Code:     code,
		LogLevel: loglevel,
	}).Log()
}

// Forbidden
func NewHttpErr403(authtype string, errstr string) *HttpError {
	errstr = fmt.Sprintf("%s %s", authtype, errstr)
	return (&HttpError{
		Err:      errors.New(errstr),
		Code:     403,
		LogLevel: DEBUG,
		AuthType: authtype,
	}).Log()
}

// Method Not Allowed
func NewHttpErr405(failmethod string, allow []string) *HttpError {
	errstr := fmt.Sprintf("%s Method Not Allowed, allow %s", failmethod, strings.Join(allow, ","))
	return (&HttpError{
		Err:      errors.New(errstr),
		Code:     405,
		LogLevel: DEBUG,
		Allow:    allow,
	}).Log()
}

// Request Timeout
func NewHttpErr408(estimatetimeflag string) *HttpError {
	errstr := fmt.Sprintf("%s Timeout.", estimatetimeflag)
	return (&HttpError{
		Err:      errors.New(errstr),
		Code:     408,
		LogLevel: DEBUG,
	}).Log()
}

// Content Too Large
func NewHttpErr413(expectlength int64, contentlength int64) *HttpError {
	errstr := fmt.Sprintf("Expect content length less then %d,but %d byte read.", expectlength, contentlength)
	return (&HttpError{
		Err:      errors.New(errstr),
		Code:     413,
		LogLevel: DEBUG,
	}).Log()
}

// Too Many Requests
func NewHttpErr429(retryafter int64) *HttpError {
	return (&HttpError{
		Err:        errors.New("Too Many Requests"),
		Code:       429,
		LogLevel:   DEBUG,
		RetryAfter: retryafter,
	}).Log()
}

// Errors
var TooManyRequestsErr error = errors.New("TooManyRequestsErr")
var ContextTimeOutErr error = errors.New("ContextTimeOutErr")
var TypeAssertErr error = errors.New("TypeAssertErr")
var CopyReachedErr error = errors.New("Copy The required write length is reached.")
var RoleNotExist error = errors.New("RoleNotExist")

func NewTypeAssertErr(expecttype string, gettype interface{}) error {
	EtLogf(3, ERROR, "Type assert to %s, but type is %T.", expecttype, gettype)
	return TypeAssertErr
}

// Utility

func Abs(path string) string {
	p, err := filepath.Abs(path)
	if err != nil {
		Error("Can not Abs %s path %s", path, err.Error())
	}
	return p
}

type ReaderStruct struct {
	Readerfunc func([]byte) (int, error)
}

func (r ReaderStruct) Read(b []byte) (int, error) {
	return r.Readerfunc(b)
}

type WriterStruct struct {
	WriterFunc func([]byte) (int, error)
}

func (w WriterStruct) Write(b []byte) (int, error) {
	return w.WriterFunc(b)
}

func ReadNByte(readerfunc func([]byte) (int, error), nbyte int) ([]byte, error) {
	var reader ReaderStruct
	reader = ReaderStruct{
		Readerfunc: readerfunc,
	}

	buff, err := ioutil.ReadAll(io.LimitReader(reader, int64(nbyte)))
	return buff, err
}

var _CopyNBufferPool = sync.Pool{
	New: func() interface{} {
		return make([]byte, 4096)
	},
}

//type CopyFunc func(int, int, bool) error

func CopyNFunc(dst io.Writer, src io.Reader, n int64, sub func(int, int, bool) error) (int64, error) {
	var t int64 = 0
	var in, on int
	var ierr, oerr, serr error

	buff := _CopyNBufferPool.Get().([]byte)
	defer _CopyNBufferPool.Put(buff)

	defer sub(in, on, true)
	for {
		in, ierr = src.Read(buff)
		if ierr != nil && ierr != io.EOF {
			return t, ierr
		}
		if in == 0 && ierr != io.EOF {
			return t, io.ErrNoProgress
		}
		on, oerr = dst.Write(buff[:in])
		if oerr != nil {
			return t, oerr
		}
		if in != on {
			return t, io.ErrShortWrite
		}
		t += int64(on)
		if t > n {
			return t, CopyReachedErr
		}
		if ierr == io.EOF {
			break
		}

		if serr = sub(in, on, false); serr != nil {
			in = 0
			on = 0
			return t, serr
		}
	}
	return t, nil
}

// Todo remove
func CopyN(ctx context.Context, dst io.Writer, src io.Reader, n int64) (int64, error) {
	return CopyNFunc(dst, src, n, func(inbyte int, outbyte int, cleanup bool) error {
		select {
		case <-ctx.Done():
			return ContextTimeOutErr
		default:
		}
		return nil
	})
}

func String2NodeInfoAcl(s string) []string {
	const comma rune = 44

	var r rune
	var previousrune rune
	n := make([]rune, 0)

	isAsciiDigitLetterDashHyphenComma := func(r rune) bool {
		if (r <= 122 && r >= 97) || (r <= 90 && r >= 65) || (r <= 57 && r >= 48) {
			return true
		}
		if r == 45 || r == comma || r == 95 {
			return true
		}
		return false
	}
	previousrune = 32
	for _, r = range s {
		if isAsciiDigitLetterDashHyphenComma(r) {
			if !(r == comma && previousrune == comma) {
				n = append(n, r)
				previousrune = r
			}
		}
	}
	s = strings.Trim(string(n), ",")
	return strings.Split(s, ",")
}

func MoreThen0Int64(n int64) int64 {
	if n < 0 {
		return 0
	}
	return n
}

func Maga2Byte(maga int64) int64 {
	return maga << 20
}

func Byte2Maga(nbyte int64) int64 {
	return nbyte >> 20
}

// Encrypt

func PasswordSha256(passwd string, salt string) string {
	s := salt + passwd
	b := sha256.Sum256([]byte(s))
	return base64.StdEncoding.EncodeToString(b[:])
}

func RandCryptoBase64url(n int) string {
	const base64url string = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_"

	var onebyte [2]byte
	var randstr []byte = make([]byte, n)

	for i := 0; i < n; i += 1 {
		rand.Read(onebyte[:])
		j := binary.BigEndian.Uint16(onebyte[:])
		j = j & 0x003f
		randstr[i] = base64url[j]
	}
	return string(randstr)
}
