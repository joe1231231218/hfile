package tool

import (
	"context"
	"errors"
	"sync"
	"time"

	"github.com/emirpasic/gods/maps/treemap"
)

var _LocalAuthCsvLock sync.Mutex
var _LocalAuthRoleRwlock sync.RWMutex
var _LocalTmpfsChangeFsLock sync.Mutex

type _RwLockStruct struct {
	RCond   uint32
	WReq    bool
	RRep    bool
	WLocked bool
	WExpire int64
}

type LocalRwLockSet struct {
	r        sync.Mutex
	m        *treemap.Map
	Interval time.Duration
}

type RwLockSet interface {
	TryRLock(ctx context.Context, k string) (bool, error)
	RLockBlock(ctx context.Context, k string) error
	RUnlock(ctx context.Context, k string) error
	WLockBlock(ctx context.Context, k string) error
	WUnlock(ctx context.Context, k string) error
}

func NewLocalRwLockSet() *LocalRwLockSet {
	return &LocalRwLockSet{
		m:        treemap.NewWithStringComparator(),
		Interval: time.Microsecond * 100,
	}
}

func (lrl *LocalRwLockSet) _NewRwLockStruct() *_RwLockStruct {
	tp := time.Now().Unix()
	return &_RwLockStruct{
		RCond:   0,
		WReq:    false,
		WLocked: false,
		WExpire: tp + 5,
	}
}

func (lrl *LocalRwLockSet) _CreateOrGet(k string, iscreate bool) (*_RwLockStruct, bool, error) {
	var rws *_RwLockStruct
	itp, ok := lrl.m.Get(k)
	if !ok {
		if iscreate {
			rws = lrl._NewRwLockStruct()
		} else {
			return rws, false, nil
		}
	} else {
		rws, ok = itp.(*_RwLockStruct)
		if !ok {
			return rws, false, NewTypeAssertErr("*_RwLockStruct", itp)
		}
	}
	return rws, true, nil
}

func (lrl *LocalRwLockSet) TryRLock(ctx context.Context, k string) (bool, error) {
	var ok bool
	var err error
	var rws *_RwLockStruct
	lrl.r.Lock()
	defer lrl.r.Unlock()
	rws, ok, err = lrl._CreateOrGet(k, true)
	if err != nil || !ok {
		return false, err
	}
	if rws.WLocked || rws.WReq {
		return false, nil
	}
	rws.RCond = rws.RCond + 1
	lrl.m.Put(k, rws)
	return true, nil
}

func (lrl *LocalRwLockSet) RLockBlock(ctx context.Context, k string) error {
	for i := 0; i < (1 << 10); i = i + 1 {
		ok, err := lrl.TryRLock(ctx, k)
		if err != nil {
			return err
		}
		if ok {
			return nil
		} else {
			select {
			case <-ctx.Done():
				return ContextTimeOutErr
			case <-time.After(lrl.Interval):

			}
		}
	}

	return errors.New("Try to get WLock too many times.")
}

func (lrl *LocalRwLockSet) RUnlock(ctx context.Context, k string) error {
	var ok bool
	var err error
	var rws *_RwLockStruct

	lrl.r.Lock()
	defer lrl.r.Unlock()
	rws, ok, err = lrl._CreateOrGet(k, false)
	if !ok {
		Debug("Release UnRecord key %s.", k)
		return nil
	}
	if err != nil {
		return err
	}

	if rws.WLocked {
		Debug("%s key Release Read lock when Write Locked.", k)
	}
	if rws.RCond > 0 {
		rws.RCond = rws.RCond - 1
	} else {
		Debug("%s key Repeat release Read lock.", k)
	}
	if rws.RCond == 0 && !rws.WLocked && !rws.WReq {
		lrl.m.Remove(k)
	} else {
		lrl.m.Put(k, rws)
	}
	return nil
}

func (lrl *LocalRwLockSet) WLockBlock(ctx context.Context, k string) error {
	const (
		wow       int = 0 // wait other write lock
		wor       int = 1 // wait other read lock
		sus       int = 2 // get write lock
		AssertErr int = 3 //Fail Type assert to _RwLockStruct
	)
	var ok bool
	var err error
	var rws *_RwLockStruct
	var tryrlt int

	var tryw = func() int {
		lrl.r.Lock()
		rws, ok, err = lrl._CreateOrGet(k, true)
		if err != nil || !ok {
			lrl.r.Unlock()
			return AssertErr
		}
		Error("%s %+v", k, rws)
		if rws.WLocked {
			lrl.r.Unlock()
			return wow
		}
		ri := sus
		if rws.RCond == 0 {
			rws.WReq = false
			rws.WLocked = true
		} else {
			rws.WReq = true
			ri = wor
		}
		lrl.m.Put(k, rws)
		lrl.r.Unlock()
		return ri
	}

	for i := 0; i < (1 << 10); i = i + 1 {
		tryrlt = tryw()
		if tryrlt == AssertErr {
			return err
		} else if tryrlt == sus {
			//fmt.Printf("WLockBlock Try Wlock %d times.\n", i)
			return nil
		} else {
			select {
			case <-ctx.Done():
				return ContextTimeOutErr
			case <-time.After(lrl.Interval):

			}
		}
	}

	return errors.New("Try to get WLock too many times.")
}

func (lrl *LocalRwLockSet) WUnlock(ctx context.Context, k string) error {
	lrl.r.Lock()
	defer lrl.r.Unlock()
	rws, ok, err := lrl._CreateOrGet(k, false)
	if !ok {
		Debug("Release UnRecord key %s.", k)
		return nil
	}
	if err != nil {
		return err
	}
	if !rws.WLocked {
		Debug("%s key Repeat release Write lock.", k)
	}
	rws.WLocked = false
	lrl.m.Remove(k)

	return nil
}

func TmpfsChangeFsLock() {
	_LocalTmpfsChangeFsLock.Lock()
}

func TmpfsChangeFsUnlock() {
	_LocalTmpfsChangeFsLock.Unlock()
}

func AuthRoleRlock(ctx context.Context) (func() error, error) {
	var runlockfunc = func() error {
		_LocalAuthRoleRwlock.RUnlock()
		return nil
	}
	_LocalAuthRoleRwlock.RLock()
	return runlockfunc, nil
}

func AuthRoleWlock(ctx context.Context) (func() error, error) {
	var wunlockfunc = func() error {
		_LocalAuthRoleRwlock.Unlock()
		return nil
	}
	_LocalAuthRoleRwlock.Lock()
	return wunlockfunc, nil
}

func AuthCsvLock(ctx context.Context) (func() error, error) {
	var unlockfunc = func() error {
		_LocalAuthCsvLock.Unlock()
		return nil
	}
	_LocalAuthCsvLock.Lock()
	return unlockfunc, nil
}
