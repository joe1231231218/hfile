package tool

import (
	"context"
	"fmt"
	"math/rand"
	"sync"
	"time"

	"testing"

	"github.com/emirpasic/gods/maps/treemap"
)

/*

 */

/*


	func Test_TmpfsChangeFsLock(t *testing.T) {
		var prtl int
		var wg sync.WaitGroup

		var runfunc = func(i int) {
			var tmp int
			defer wg.Done()
			for j := 0; j < 20; j = j + 1 {
				TmpfsChangeFsLock()
				prtl = i
				time.Sleep(time.Microsecond * 20)
				tmp = prtl
				TmpfsChangeFsUnlock()
				if tmp != i {
					t.Logf("prtl is not protected, i %d prtl %d", i, prtl)
				}
			}
		}

		gtn := 10
		for j := 0; j < gtn; j = j + 1 {
			wg.Add(1)
			go runfunc(j)
		}
		wg.Wait()
	}
*/

func Test_AuthRoleRwlock(t *testing.T) {
	ctx, _ := context.WithCancel(context.Background())

	un1, _ := AuthRoleRlock(ctx)
	un2, _ := AuthRoleRlock(ctx)
	un1()
	un2()
	un3, _ := AuthRoleWlock(ctx)
	un3()

}

func Test_LocalRwLockSet(t *testing.T) {
	const (
		kut string = "Unkey"
		kg1 string = "G1key"
		kg2 string = "G2key"
		kg3 string = "G3key"
		kg4 string = "G4key"
	)
	var err error
	var l *LocalRwLockSet = NewLocalRwLockSet()
	var ctx context.Context

	ctx, _ = context.WithCancel(context.Background())
	err = l.RLockBlock(ctx, kut)
	if err != nil {
		t.Log(err)
	}
	err = l.RUnlock(ctx, kut)
	if err != nil {
		t.Log(err)
	}
	err = l.WLockBlock(ctx, kut)
	if err != nil {
		t.Log(err)
	}
	err = l.WUnlock(ctx, kut)
	if err != nil {
		t.Log(err)
	}

	err = l.WLockBlock(ctx, kut)
	if err != nil {
		t.Log(err)
	}
	err = l.WUnlock(ctx, kut)
	if err != nil {
		t.Log(err)
	}
	err = l.RLockBlock(ctx, kut)
	if err != nil {
		t.Log(err)
	}
	err = l.RLockBlock(ctx, kut)
	if err != nil {
		t.Log(err)
	}
	err = l.RUnlock(ctx, kut)
	if err != nil {
		t.Log(err)
	}
	err = l.RUnlock(ctx, kut)
	if err != nil {
		t.Log(err)
	}

	var wg sync.WaitGroup
	var grlst []string = []string{kg1, kg2, kg3, kg4}
	var rloop = func(sleep uint, loop uint, key string) {
		wg.Add(1)
		defer wg.Done()

		var i uint
		for i = 0; i < loop; i = i + 1 {
			if (i+1)%5 == 0 {
				fmt.Printf("-")
			}
			err = l.RLockBlock(ctx, kut)
			if err != nil {
				t.Log(err)
			}
			time.Sleep(time.Duration(sleep) * time.Microsecond)
			err = l.RUnlock(ctx, kut)
			if err != nil {
				t.Log(err)
			}
		}
	}

	var wloop = func(sleep uint, loop uint, key string) {
		wg.Add(1)
		defer wg.Done()

		var i uint
		for i = 0; i < loop; i = i + 1 {
			if (i+1)%5 == 0 {
				fmt.Printf("+")
			}
			err = l.WLockBlock(ctx, kut)
			if err != nil {
				t.Log(err)
			}
			time.Sleep(time.Duration(sleep) * time.Microsecond)
			err = l.WUnlock(ctx, kut)
			if err != nil {
				t.Log(err)
			}
		}
	}
	for _, gr := range grlst {
		go rloop(1, 10, gr)
		go wloop(1, 10, gr)
	}
	wg.Wait()
	fmt.Printf("\n")
	for i := 0; i < 25; i = i + 1 {
		go rloop(1, 100, grlst[i%len(grlst)])
		if i < len(grlst)*3 {
			go wloop(1, 100, grlst[i%len(grlst)])
		}
	}
	wg.Wait()
	for i := 0; i < 100; i = i + 1 {
		rloop(uint(rand.Int31n(120)), 50, grlst[i%len(grlst)])
	}
	wg.Wait()

}

func Test_Utility(t *testing.T) {
	t.Log(RandCryptoBase64url(12))
	t.Log(String2NodeInfoAcl("any,admin,Eric,Eric2"))
	t.Log(String2NodeInfoAcl(" , 12,_ , ,, ,-aFd3; , ,, "))
}

type GodsCopy struct {
	i   int
	ptr *int
}

func NewGodsCopy() *GodsCopy {
	var i = 54
	return &GodsCopy{
		i:   87,
		ptr: &i,
	}
}

func Test_Gods(t *testing.T) {
	//var ii int = 66
	var m *treemap.Map = treemap.NewWithStringComparator()
	var gdc, ggd *GodsCopy

	gdc = NewGodsCopy()
	m.Put("copy", gdc)
	fmt.Printf("gdc %p %+v.\n", gdc, gdc)
	gdc.i = 55
	*gdc.ptr = 66
	it, _ := m.Get("copy")
	ggd = it.(*GodsCopy)
	fmt.Printf("ggd %p %+v.\n", ggd, ggd)

	var si int = 54
	var sgd, sgg GodsCopy
	sgd = GodsCopy{
		i:   87,
		ptr: &si,
	}
	m.Put("scop", sgd)
	fmt.Printf("sgd %p  %+v.\n", &sgd, sgd)
	gdc.i = 55
	*gdc.ptr = 66
	it, _ = m.Get("scop")
	sgg = it.(GodsCopy)
	fmt.Printf("sgg %p  %+v.\n", &sgg, sgg)
}
