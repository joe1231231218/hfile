package tool

import (
	"time"
)

const (
	RoleGroupBase = iota
	RoleGroupAny
	RoleGroupAdmin
	RoleGroupBig
	RoleGroupSmall
	RoleGroupSlow
	RoleGroupHighRisk
)

// The expiration field represents the birth plus the number of seconds for expiration.
// If the expiration is zero, it means it will never expire (setting permission is required)
// Size must write by Tmpfs::Read
type NodeInfo struct {
	Acl         []string
	Size        int64
	Role        string
	Birth       int64
	Expire      int64
	Remain      int
	LinkIp      string
	Folder      string //Todo
	DelToken    string
	FileName    string
	IsEncrypt   bool
	ContentType string
}

func NewNodeInfo(keepsecond int, remain int) *NodeInfo {
	var inf *NodeInfo
	tp := time.Now().Unix()

	inf = &NodeInfo{
		Acl:       make([]string, 0),
		Size:      -1,
		Role:      "any",
		Birth:     tp,
		Expire:    int64(keepsecond),
		Remain:    remain,
		DelToken:  "",
		IsEncrypt: false,
	}
	return inf
}

// if user name is any, we will pass all authenticate
type UserInfo struct {
	Role       string `csv:Role`
	RoleExpire int64
	//rate limit
	Burst int `csv:Burst`
	// pre minute
	LeakRate int   `csv:LeakRate`
	Lv1Full  int64 `csv:Lv1Full`
	Lv2Full  int64 `csv:Lv2Full`
	// pre Maga
	RiskPreLink int
	RiskPreRead int
	// authentication
	BasicOk      bool `csv:BasicOk`
	PasswdSha256 string
	Salt         string
	CookieOk     bool
	Secret       string
	CookieExpire int64
	JwtOk        bool `csv:JwtOk`
	PostPasswdOk bool
	// right
	// add and list user
	Admin bool `csv:Admin`
	// Delete the files created by yourself
	DeleteRight bool `csv:DeleteRight`
	// ignore ACL
	IgnoreAcl bool `csv:IgnoreAcl`

	MaxRemain int `csv:MaxRemain`
	//MaxExpire in second
	MaxExpire     int64 `csv:MaxExpire`
	MaxLinkMaga   int   `csv:MaxLinkMaga`
	MaxReadMaga   int   `csv:MaxReadMaga`
	DefaultAcl    []string
	DefaultRemain int
	DefaultExpire int
	// Estimate period pre mage in second
	PeriodReadMaga int
	PeriodLinkMaga int
}

func NewUserInfoGroup(role string, group uint) *UserInfo {
	var r, base *UserInfo
	base = &UserInfo{
		Role:           role,
		Burst:          1200,
		LeakRate:       1,
		Lv1Full:        2000,
		Lv2Full:        2000,
		RiskPreLink:    5,
		RiskPreRead:    1,
		BasicOk:        true,
		PasswdSha256:   "0000",
		CookieOk:       false,
		JwtOk:          false,
		PostPasswdOk:   false,
		Admin:          false,
		DeleteRight:    false,
		IgnoreAcl:      false,
		MaxRemain:      100,
		MaxExpire:      60 * 60 * 24,
		MaxLinkMaga:    1024,
		MaxReadMaga:    4096,
		DefaultAcl:     make([]string, 0),
		DefaultRemain:  3,
		DefaultExpire:  60 * 60 * 3,
		PeriodReadMaga: 12,
		PeriodLinkMaga: 21,
	}
	r = base
	switch group {
	case RoleGroupBase:
	case RoleGroupAny:
		r.BasicOk = false
		r.PasswdSha256 = ""
	case RoleGroupAdmin:
		r.Admin = true
		r.PostPasswdOk = true
	case RoleGroupBig:
		r.Burst = 35000
		r.Lv1Full = 45000
		r.Lv2Full = 45000
		r.MaxReadMaga = (1 << 30) * 25
		r.MaxLinkMaga = (1 << 30) * 25
	case RoleGroupSmall:
		r.MaxReadMaga = (1 << 10)
		r.MaxLinkMaga = (1 << 10)
	case RoleGroupSlow:
		r.PeriodReadMaga = 200
		r.PeriodLinkMaga = 250
	case RoleGroupHighRisk:
		r.Burst = 12
		r.Lv1Full = 15
		r.Lv2Full = 15
	}
	return r
}

func NewUserInfoBase(role string) *UserInfo {
	var rany *UserInfo
	rany = NewUserInfoGroup(role, RoleGroupBase)
	return rany
}
