// config.go
package main

import (
	"hfile/middleware"
	"hfile/ratelimiter"
	"hfile/tmpfs"
	"hfile/tool"
	"path/filepath"

	"gopkg.in/ini.v1"
)

var ConfigFileOptionName []string = []string{"Config.ini", "config.ini", "cfg.ini", "Hfile.config", "hfile.config"}
var ConfigFileOptionFolder []string = []string{".", "/etc/hfile"}

var ConfigFilePath string
var ConfigAsDefault bool = false

// config
var ConfigDefaultFilePath string = "DefaultConfig.ini"

type ConfigCurrentStruct struct {
	Cfg     *ini.File
	DfCfg   *ini.File
	Sec     *ini.Section
	DfSec   *ini.Section
	SecName string
	Key     *ini.Key
	KeyName string
	Errk    error //Error about get or set key
	ErrS    error //Error about get or set section
	ErrC    error //Error about convert key value string to type
}

var ConfigCurrent ConfigCurrentStruct

func _FindConfigFile(cfgpath string) (*ini.File, bool) {
	var cfgp string
	var ok bool
	var cfg *ini.File
	loadfunc := func(p string) (*ini.File, bool) {
		cfg, err := ini.InsensitiveLoad(p)
		if err != nil {
			tool.Debug("Try to load %s %s\n", tool.Abs(p), err.Error())
			return nil, false
		}
		return cfg, true
	}
	ok = false
	if len(cfgpath) != 0 {
		cfgp = cfgpath
		cfg, ok = loadfunc(cfgp)
	}
	if !ok {
	ENDLOOP:
		for _, p := range ConfigFileOptionFolder {
			for _, n := range ConfigFileOptionName {
				cfgp = filepath.Join(p, n)
				cfg, ok = loadfunc(cfgp)
				if ok {
					break ENDLOOP
				}
			}
		}
	}
	if ok {
		ConfigFilePath = cfgp
		tool.Info("Load Config File at %s.\n", tool.Abs(ConfigFilePath))
	}
	return cfg, ok
}

func _ConfigKey(key string, defaultval string) *ini.Key {
	var err error
	var cc *ConfigCurrentStruct = &ConfigCurrent
	cc.KeyName = key

	_ConfigConvertCheck()
	_, err = cc.DfSec.NewKey(key, defaultval)
	if err != nil {
		tool.Error("Unexpect Error at Config %s Section %s Key %v.\n", cc.SecName, key, err)
		panic(err)
	}
	cc.Key, cc.Errk = cc.Sec.GetKey(key)
	if cc.Errk != nil {
		tool.Error("Error at Config %s Section %s Key %v.\n", cc.SecName, key, cc.Errk)
		panic(cc.Errk)
	}
	return cc.Key
}

func _ConfigConvertCheck() {
	var cc *ConfigCurrentStruct = &ConfigCurrent
	if cc.ErrC != nil {
		tool.Error("Error at Config %s Section %s Key convert %v.\n", cc.SecName, cc.KeyName, cc.ErrC)
		panic(cc.ErrC)
	}
}

func _ConfigSec(secname string) {
	var cc *ConfigCurrentStruct = &ConfigCurrent
	cc.SecName = secname

	cc.DfSec, _ = cc.DfCfg.NewSection(secname)
	cc.Sec, cc.ErrS = cc.Cfg.GetSection(secname)
	if cc.ErrS != nil {
		tool.Error("Error at Config %s Section %v.\n", cc.SecName, cc.ErrS)
		panic(cc.ErrS)
	}
	return
}

func ConfigServer() {
	var cc *ConfigCurrentStruct = &ConfigCurrent
	_ConfigSec("server")

	//[server]
	//ip = 127.0.0.1
	//port = 8080
	ServerIp = _ConfigKey("ip", "127.0.0.1").String()
	ServerPort, cc.ErrC = _ConfigKey("port", "8080").Uint()
	ServerTimeout, cc.ErrC = _ConfigKey("timeout", "15").Uint()
	_ConfigConvertCheck()
}

func ConfigLog() {
	var t uint
	var p string
	var cc *ConfigCurrentStruct = &ConfigCurrent
	_ConfigSec("log")

	p = _ConfigKey("logpath", "stdout").String()
	t, cc.ErrC = _ConfigKey("loglevel", "27").Uint()
	ConfigDefaultFilePath = _ConfigKey("DefaultConfigFilePath", "DefaultConfig.ini").String()
	_ConfigConvertCheck()

	tool.LgLevel = int16(t)
	tool.ResetLoggerPath(p)
}

func ConfigTmpfs() {
	var cc *ConfigCurrentStruct = &ConfigCurrent
	_ConfigSec("tmpfs")

	tmpfs.Flty, cc.ErrC = _ConfigKey("flty", "1").Int()
	tmpfs.PrefixLen, cc.ErrC = _ConfigKey("prefixlen", "2").Int()
	tmpfs.SuffixLen, cc.ErrC = _ConfigKey("suffixlen", "3").Int()
	tmpfs.MaxMetadataLen, cc.ErrC = _ConfigKey("MaxMetadataLen", "1024").Int()
	tmpfs.AllowNodePermanent, cc.ErrC = _ConfigKey("AllowNodePermanent", "false").Bool()
	_ConfigConvertCheck()
}

func ConfigTmpfsStorj() {
	_ConfigSec("tmpfs-storj")

	tmpfs.StorjBucketName = _ConfigKey("bucketname", "").String()
	tmpfs.StorjAccessGrant = _ConfigKey("accessgrant", "").String()
	tmpfs.StorjRootPassphrase = _ConfigKey("rootpassphrase", "").String()
	_ConfigConvertCheck()
}

func ConfigTmpfsTempfile() {
	_ConfigSec("tmpfs-tempfile")

	tmpfs.Dir_tmp = _ConfigKey("tempdir", "tmp/").String()
	_ConfigConvertCheck()
}

func ConfigTmpfsSftp() {
	var cc *ConfigCurrentStruct = &ConfigCurrent
	_ConfigSec("tmpfs-sftp")

	tmpfs.SftpUser = _ConfigKey("SftpUser", "user").String()
	tmpfs.SftpAddr = _ConfigKey("SftpAddr", "127.0.0.1").String()
	tmpfs.SftpPort, cc.ErrC = _ConfigKey("SftpPort", "22").Uint()
	tmpfs.SftpTimeout, cc.ErrC = _ConfigKey("SftpTimeout", "12").Uint()
	tmpfs.SftpPassword = _ConfigKey("SftpPassword", "").String()
	tmpfs.SftpStoreDirectory = _ConfigKey("SftpStoreDirectory", "/tmp/hfile").String()
	_ConfigConvertCheck()
}

func ConfigRouter() {
	var cc *ConfigCurrentStruct = &ConfigCurrent
	_ConfigSec("router")

	middleware.SecretLength, cc.ErrC = _ConfigKey("SecretLength", "16").Int()
	middleware.DelTokenLength, cc.ErrC = _ConfigKey("DelTokenLength", "12").Int()
	middleware.AuthCsvMaxMaga, cc.ErrC = _ConfigKey("AuthCsvMaxMaga", "128").Int()
	middleware.AssetFolderPath = _ConfigKey("AssetFolderPath", "eric/asset").String()
	middleware.SendHttpErrorDetail, cc.ErrC = _ConfigKey("SendHttpErrorDetail", "true").Bool()
	middleware.GatewaySchemeHostPort = _ConfigKey("GatewaySchemeHostPort", "http://127.0.0.1:8080").String()
	middleware.LogHttpAccessElapseTime, cc.ErrC = _ConfigKey("LogHttpAccessElapseTime", "true").Bool()
	_ConfigConvertCheck()
}

func ConfigAuth() {
	_ConfigSec("Auth")

	middleware.AuthCsvPath = _ConfigKey("AuthCsvPath", "auth.csv").String()
	middleware.AuthCsvDefaultPath = _ConfigKey("AuthCsvDefaultPath", "default_auth.csv").String()
	_ConfigConvertCheck()
}

func ConfigTmpfsS3() {
	var cc *ConfigCurrentStruct = &ConfigCurrent
	_ConfigSec("tmpfs-s3")

	tmpfs.S3Region = _ConfigKey("S3Region", "us-east-1").String()
	tmpfs.S3SccessKey = _ConfigKey("S3SccessKey", "").String()
	tmpfs.S3BucketName = _ConfigKey("S3BucketName", "").String()
	tmpfs.S3SecretAccessKey = _ConfigKey("S3SecretAccessKey", "").String()
	tmpfs.S3AutoMakeBucket, cc.ErrC = _ConfigKey("S3AutoMakeBucket", "false").Bool()
	_ConfigConvertCheck()
}

func ConfigTmpfsGoogle() {
	_ConfigSec("tmpfs-google")

	tmpfs.GoogleStorageBucket = _ConfigKey("GoogleStorageBucket", "").String()
	tmpfs.ServiceAccountCredentialJSON = _ConfigKey("ServiceAccountCredentialJSON", "").String()
	_ConfigConvertCheck()
}

func ConfigLock() {
	_ConfigSec("ratelimiter")
}

func ConfigRateLimiter() {
	var cc *ConfigCurrentStruct = &ConfigCurrent
	_ConfigSec("ratelimiter")

	ratelimiter.Rlty, cc.ErrC = _ConfigKey("Rlty", "0").Int()
	ratelimiter.DefaultBlockExpire, cc.ErrC = _ConfigKey("DefaultBlockExpire", "300").Int()
	ratelimiter.DefaultSlideWindowSize, cc.ErrC = _ConfigKey("DefaultSlideWindowSize", "4").Int()
	ratelimiter.DefaultLeakyBucketExpire, cc.ErrC = _ConfigKey("DefaultLeakyBucketExpire", "18000").Int()
	ratelimiter.RateLimiterRedisDb, cc.ErrC = _ConfigKey("RateLimiterRedisDb", "0").Int()
	ratelimiter.RateLimiterRedisPoolSize, cc.ErrC = _ConfigKey("RateLimiterRedisPoolSize", "32").Int()
	ratelimiter.RateLimiterRedisAddrPort = _ConfigKey("RateLimiterRedisAddrPort", "").String()
	ratelimiter.RateLimiterRedisPassword = _ConfigKey("RateLimiterRedisPassword", "").String()
	_ConfigConvertCheck()
}

// Set before run
func LoadConfig(cfgpath string) (recoverRet bool) {
	var ok bool = false

	if !ConfigAsDefault {
		ConfigCurrent.Cfg, ok = _FindConfigFile(cfgpath)
	}
	if !ok {
		tool.Error("Can not load Config File.\n")
		ConfigAsDefault = true
	}
	if ConfigAsDefault {
		ConfigCurrent.Cfg = ini.Empty()
		ConfigCurrent.DfCfg = ConfigCurrent.Cfg
	} else {
		ConfigCurrent.DfCfg = ini.Empty()
	}

	defer func() {
		if r := recover(); r != nil {
			recoverRet = false
		}
	}()

	ConfigLog()
	ConfigServer()

	ConfigTmpfs()
	ConfigTmpfsS3()
	ConfigTmpfsSftp()
	ConfigTmpfsStorj()
	ConfigTmpfsGoogle()
	ConfigTmpfsTempfile()

	ConfigLock()
	ConfigAuth()
	ConfigRouter()
	ConfigRateLimiter()

	err := ConfigCurrent.DfCfg.SaveTo(ConfigDefaultFilePath)
	if err != nil {
		tool.Error("Save DefaultConfigFile to %s fail with %s.\n", tool.Abs(ConfigDefaultFilePath), err.Error())
		return false
	}
	tool.Info("Save Default Config File to %s.\n", tool.Abs(ConfigDefaultFilePath))
	recoverRet = !ConfigAsDefault
	return
}
